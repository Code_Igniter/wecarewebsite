﻿using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.ServerModel;

namespace DAL.Repositories
{
   public class DanphecareRepository : Repository<Danphecare>, IDanphecareRepository
    {
        public DanphecareRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
