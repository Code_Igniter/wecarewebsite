﻿
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.ServerModel;

namespace DAL.Repositories
{
    public class DanphecareReachUsQuicklyRepository : Repository<DanphecareReachUsQuickly>, IDanphecareReachUsQuickly
    {
        public DanphecareReachUsQuicklyRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
