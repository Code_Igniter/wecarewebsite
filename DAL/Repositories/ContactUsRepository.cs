﻿using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.ServerModel;

namespace DAL.Repositories
{
   public class ContactUsRepository : Repository<ContactUs>, IContactUsRepository
    {
        public ContactUsRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
