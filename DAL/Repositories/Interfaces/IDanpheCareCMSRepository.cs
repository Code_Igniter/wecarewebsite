﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.ServerModel;



namespace DAL.Repositories.Interfaces
{
    public interface IAboutUsRepository : IRepository<AboutUsModel>
    {
    }    
    public interface IOurServicesRepository : IRepository<OurServicesModel>
    {
    }
    public interface IDepartmentModelRepository : IRepository<DepartmentModel>
    {
    }
    public interface IDepartmentConsultationModelRepository : IRepository<DepartmentConsultationModel>
    {
    }
    public interface IDanpheCareDoctorModelRepository : IRepository<DanpheCareDoctorModel>
    {
    }
    public interface IResourcefulArticlesModelRepository : IRepository<ResourcefulArticlesModel>
    {
    }
    public interface INewsModelRepository : IRepository<NewsModel>
    {
    }
    public interface IBlogsModelRepository : IRepository<BlogsModel>
    {
    }
    public interface ICommentsModelRepository : IRepository<CommentsModel>
    {
    }

    public interface IMetaTagRepository : IRepository<MetaTagModel>
    {
    }


    public interface IOurTeamMemberRepository : IRepository<OurTeamMemberModel>
    {
    }

    public interface IOurMediaCoverageRepository : IRepository<OurMediaCoverageModel>
    {
    }

    public interface ITestimonialMainRepository : IRepository<TestimonialMainModel>
    {
    }

    public interface ITestimonialRepository : IRepository<TestimonialModel>
    {
    }
    public interface IDanpheCareContactRepository : IRepository<DanpheCareContactModel>
    {
    }

    public interface IExpatModelRepository : IRepository<ExpatModel>
    {
    }

    public interface IWebinarModelRepository : IRepository<WebinarModel>
    {
    }
    public interface IDepartmentTabRepository : IRepository<DepartmentTab>
    {
    }

    public interface IDepartmentTabContentRepository : IRepository<DepartmentTabContent>
    {
    }
    public interface IDepartmentSubHeadingRepository : IRepository<DepartmentSubHeading>
    {
    }

    public interface IPackageNameModelRepository : IRepository<PackageNameModel>
    {
    }

    public interface IPackageDetailsRepository : IRepository<PackageDetails>
    {
    }

    public interface ISubServicesRepository : IRepository<SubServices>
    {
    }

    public interface ISubServicesDetailsRepository : IRepository<SubServicesDetails>
    {
    }
}
