﻿
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.ServerModel;

namespace DAL.Repositories
{
    public class AboutUsRepository : Repository<AboutUsModel>, IAboutUsRepository
    {
        public AboutUsRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class OurServicesRepository : Repository<OurServicesModel>, IOurServicesRepository
    {
        public OurServicesRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class DepartmentModelRepository : Repository<DepartmentModel>, IDepartmentModelRepository
    {
        public DepartmentModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class DepartmentConsultationModelRepository : Repository<DepartmentConsultationModel>, IDepartmentConsultationModelRepository
    {
        public DepartmentConsultationModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class DanpheCareDoctorModelRepository : Repository<DanpheCareDoctorModel>, IDanpheCareDoctorModelRepository
    {
        public DanpheCareDoctorModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class ResourcefulArticlesModelRepository : Repository<ResourcefulArticlesModel>, IResourcefulArticlesModelRepository
    {
        public ResourcefulArticlesModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class NewsModelRepository : Repository<NewsModel>, INewsModelRepository
    {
        public NewsModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class BlogsModelRepository : Repository<BlogsModel>, IBlogsModelRepository
    {
        public BlogsModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class CommentsModelRepository : Repository<CommentsModel>, ICommentsModelRepository
    {
        public CommentsModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class MetaTagRepository : Repository<MetaTagModel>, IMetaTagRepository
    {
        public MetaTagRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class OurTeamMemberRepository : Repository<OurTeamMemberModel>, IOurTeamMemberRepository
    {
        public OurTeamMemberRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
    public class OurMediaCoverageRepository : Repository<OurMediaCoverageModel>, IOurMediaCoverageRepository
    {
        public OurMediaCoverageRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
    public class TestimonialMainRepository : Repository<TestimonialMainModel>, ITestimonialMainRepository
    {
        public TestimonialMainRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
    public class TestimonialRepository : Repository<TestimonialModel>, ITestimonialRepository
    {
        public TestimonialRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class DanpheCareContactRepository : Repository<DanpheCareContactModel>, IDanpheCareContactRepository
    {
        public DanpheCareContactRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
    public class ExpatModelRepository : Repository<ExpatModel>, IExpatModelRepository
    {
        public ExpatModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
    public class WebinarModelRepository : Repository<WebinarModel>, IWebinarModelRepository
    {
        public WebinarModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class DepartmentTabRepository : Repository<DepartmentTab>, IDepartmentTabRepository
    {
        public DepartmentTabRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
    public class DepartmentTabContentRepository : Repository<DepartmentTabContent>, IDepartmentTabContentRepository
    {
        public DepartmentTabContentRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class DepartmentSubHeadingRepository : Repository<DepartmentSubHeading>, IDepartmentSubHeadingRepository
    {
        public DepartmentSubHeadingRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class PackageNameModelRepository : Repository<PackageNameModel>, IPackageNameModelRepository
    {
        public PackageNameModelRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class PackageDetailsRepository : Repository<PackageDetails>, IPackageDetailsRepository
    {
        public PackageDetailsRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class SubServicesRepository : Repository<SubServices>, ISubServicesRepository
    {
        public SubServicesRepository(ApplicationDbContext context) : base(context)
        {

        }
    }

    public class SubServicesDetailsRepository : Repository<SubServicesDetails>, ISubServicesDetailsRepository
    {
        public SubServicesDetailsRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
