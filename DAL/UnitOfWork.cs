﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Repositories;
using DAL.Repositories.Interfaces;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

        ICustomerRepository _customers;
        IProductRepository _products;
        IOrdersRepository _orders;
		IDanphecareRepository _danphecare;
        IDanphecareReachUsQuickly _danphecareruq;
        IContactUsRepository _contactUs;
        IAboutUsRepository _aboutUs;
        IOurServicesRepository _ourServices;
        IDepartmentModelRepository _departmentModel;
        IDepartmentConsultationModelRepository _departmentConsultationModel;
        IDanpheCareDoctorModelRepository _danpheCareModel;
        IResourcefulArticlesModelRepository _resourcefulArticlesModel;
        INewsModelRepository _newsModel;
        IBlogsModelRepository _blogsModel;
        ICommentsModelRepository _commentsModel;
        IMetaTagRepository _metaTagsModel;
        IOurTeamMemberRepository _ourTeamMembers;
        IOurMediaCoverageRepository _ourMediaCoverage;
        ITestimonialMainRepository _testimonialMain;
        ITestimonialRepository _testimonials;
        IDanpheCareContactRepository _danpheCareContact;
        IExpatModelRepository _expat;
        IWebinarModelRepository _webinar;
        IDepartmentTabRepository _depTabs;
        IDepartmentTabContentRepository _depTabContent;
        IDepartmentSubHeadingRepository _depSubHeading;
        IPackageNameModelRepository _packageNameModel;
        IPackageDetailsRepository _packageDetails;
        ISubServicesRepository _subServicesModel;
        ISubServicesDetailsRepository _subServicesDetails;


        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }



        public ICustomerRepository Customers
        {
            get
            {
                if (_customers == null)
                    _customers = new CustomerRepository(_context);

                return _customers;
            }
        }



        public IProductRepository Products
        {
            get
            {
                if (_products == null)
                    _products = new ProductRepository(_context);

                return _products;
            }
        }



        public IOrdersRepository Orders
        {
            get
            {
                if (_orders == null)
                    _orders = new OrdersRepository(_context);

                return _orders;
            }
        }
         
		 public IDanphecareRepository Danphecare
        {
            get
            {
                if (_danphecare == null)
                    _danphecare = new DanphecareRepository(_context);

                return _danphecare;
            }
        }
        public IDanphecareReachUsQuickly DanphecareReachUsQuickly
        {
            get
            {
                if (_danphecareruq == null)
                    _danphecareruq = new DanphecareReachUsQuicklyRepository(_context);

                return _danphecareruq;
            }
        }
        public IContactUsRepository ContacUs
        {
            get
            {
                if (_contactUs == null)
                    _contactUs = new ContactUsRepository(_context);

                return _contactUs;
            }
        }
        public IAboutUsRepository AboutUs
        {
            get
            {
                if (_aboutUs == null)
                    _aboutUs = new AboutUsRepository(_context);

                return _aboutUs;
            }
        }
        public IOurServicesRepository OurServices
        {
            get
            {
                if (_ourServices == null)
                    _ourServices = new OurServicesRepository(_context);

                return _ourServices;
            }

        }

        public IDepartmentModelRepository DepartmentModels
        {
            get
            {
                if (_departmentModel == null)
                    _departmentModel = new DepartmentModelRepository(_context);

                return _departmentModel;
            }

        }

        public IDepartmentConsultationModelRepository DepartmentConsultationModels
        {
            get
            {
                if (_departmentConsultationModel == null)
                    _departmentConsultationModel = new DepartmentConsultationModelRepository(_context);

                return _departmentConsultationModel;
            }

        }

        public IDanpheCareDoctorModelRepository DanpheCareDoctorModels
        {
            get
            {
                if (_danpheCareModel == null)
                    _danpheCareModel = new DanpheCareDoctorModelRepository(_context);

                return _danpheCareModel;
            }

        }

        public IResourcefulArticlesModelRepository ResourcefulArticlesModels
        {
            get
            {
                if (_resourcefulArticlesModel == null)
                    _resourcefulArticlesModel = new ResourcefulArticlesModelRepository(_context);

                return _resourcefulArticlesModel;
            }

        }

        public INewsModelRepository NewsModels
        {
            get
            {
                if (_newsModel == null)
                    _newsModel = new NewsModelRepository(_context);

                return _newsModel;
            }

        }

        public IBlogsModelRepository BlogsModels
        {
            get
            {
                if (_blogsModel == null)
                    _blogsModel = new BlogsModelRepository(_context);

                return _blogsModel;
            }

        }

        public ICommentsModelRepository CommentsModels
        {
            get
            {
                if (_commentsModel == null)
                    _commentsModel = new CommentsModelRepository(_context);

                return _commentsModel;
            }

        }

        public IMetaTagRepository MetaTagModels
        {
            get
            {
                if (_metaTagsModel==null)
                    _metaTagsModel = new MetaTagRepository(_context);

                return _metaTagsModel;
            }

        }

        public IOurTeamMemberRepository OurTeamMembers
        {
            get
            {
                if (_ourTeamMembers == null)
                    _ourTeamMembers = new OurTeamMemberRepository(_context);

                return _ourTeamMembers;
            }

        }

        public IOurMediaCoverageRepository OurMediaCoverage
        {
            get
            {
                if (_ourMediaCoverage == null)
                    _ourMediaCoverage = new OurMediaCoverageRepository(_context);

                return _ourMediaCoverage;
            }

        }

        public ITestimonialMainRepository TestimonialMain
        {
            get
            {
                if (_testimonialMain == null)
                    _testimonialMain = new TestimonialMainRepository(_context);

                return _testimonialMain;
            }

        }

        public ITestimonialRepository Testimonials
        {
            get
            {
                if (_testimonials == null)
                    _testimonials = new TestimonialRepository(_context);

                return _testimonials;
            }

        }

        public IDanpheCareContactRepository DanpheCareContactModels
        {
            get
            {
                if (_danpheCareContact == null)
                    _danpheCareContact = new DanpheCareContactRepository(_context);

                return _danpheCareContact;
            }

        }

        public IExpatModelRepository ExpatModels
        {
            get
            {
                if (_expat == null)
                    _expat = new ExpatModelRepository(_context);

                return _expat;
            }

        }

        public IWebinarModelRepository WebinarModels
        {
            get
            {
                if (_webinar == null)
                    _webinar = new WebinarModelRepository(_context);

                return _webinar;
            }

        }

        public IDepartmentTabRepository DepartmentTabs
        {
            get
            {
                if (_depTabs == null)
                    _depTabs = new DepartmentTabRepository(_context);

                return _depTabs;
            }

        }

        public IDepartmentTabContentRepository DepartmentTabContents
        {
            get
            {
                if (_depTabContent == null)
                    _depTabContent = new DepartmentTabContentRepository(_context);

                return _depTabContent;
            }

        }

        public IDepartmentSubHeadingRepository DepartmentSubHeadings
        {
            get
            {
                if (_depSubHeading == null)
                    _depSubHeading = new DepartmentSubHeadingRepository(_context);

                return _depSubHeading;
            }

        }

        public IPackageNameModelRepository PackageNameModels
        {
            get
            {
                if (_packageNameModel == null)
                    _packageNameModel = new PackageNameModelRepository(_context);

                return _packageNameModel;
            }

        }

        public IPackageDetailsRepository PackageDetails
        {
            get
            {
                if (_packageDetails == null)
                    _packageDetails = new PackageDetailsRepository(_context);

                return _packageDetails;
            }

        }

        public ISubServicesRepository SubServices
        {
            get
            {
                if (_subServicesModel == null)
                    _subServicesModel = new SubServicesRepository(_context);

                return _subServicesModel;
            }

        }

        public ISubServicesDetailsRepository SubServicesDetails
        {
            get
            {
                if (_subServicesDetails == null)
                    _subServicesDetails = new SubServicesDetailsRepository(_context);

                return _subServicesDetails;
            }

        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
