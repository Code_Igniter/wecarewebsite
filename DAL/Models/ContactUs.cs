﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace Telemedicine.ServerModel
{
   public class ContactUs
    {
        [Key]
        public int UserContactId { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public string Massage { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
