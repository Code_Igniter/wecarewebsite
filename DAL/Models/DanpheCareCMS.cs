﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Telemedicine.ServerModel
{

    public class AboutUsModel : AuditableEntity
    {
        [Key]
        public int AboutUsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ShortDescription { get; set; }
        public string ImagePath { get; set; }
    }

    public class OurServicesModel : AuditableEntity
    {
        [Key]
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string Content { get; set; }
        public string IconPath { get; set; }
        public string CoverImage { get; set; }
        public string ImagePath { get; set; }
        public string SubscriptionPlan { get; set; }
        public string SubscriptionContent { get; set; }
        public string SubscriptionImage { get; set; }
        public int? DepartmentId { get; set; }
        public string ShortIntroduction { get; set; }
        public string Color { get; set; }
        public string PermaLink { get; set; }
        public virtual DepartmentModel Department { get; set; }
    }

   public class DepartmentModel :  AuditableEntity
    {
        [Key]
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string IconPath { get; set; }
        public string Title { get; set; }
        public string Introduction { get; set; }
        public string ImagePath { get; set; }
        public string PermaLink { get; set; }
    }
    public class DanpheCareDoctorModel: AuditableEntity
    {
        [Key]
        public int DoctorId { get; set; }
        public string FullName { get; set; }
        public string Designation { get; set; }
        public string Experience { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public string CoverPhoto { get; set; }
        public int Sorting { get; set; }
        public bool DisableSorting { get; set; }
        public int? DepartmentId { get; set; }
        public string PermaLink { get; set; }
        public virtual DepartmentModel Department { get; set; }

    }
    public class DepartmentConsultationModel : AuditableEntity
    {
        [Key]
        public int DepartmentConsultationId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string IconPath { get; set; }
        public int DepartmentId { get; set; }
        public virtual DepartmentModel Department { get; set; }
    }
    public class ResourcefulArticlesModel : AuditableEntity
    {
        [Key]
        public int ResourcefulArticlesId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
    }
    public class NewsModel : AuditableEntity
    {
        [Key]
        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public string PostedBy { get; set; }
    }
    public class BlogsModel : AuditableEntity
    {
        [Key]
        public int BlogId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public string PostedBy { get; set; }
        public string PermaLink { get; set; }
    }
    public class CommentsModel: AuditableEntity
    {
        [Key]
        public int CommentId { get; set; }
        public string Comment { get; set; }
        public int BlogId { get; set; }
        public virtual BlogsModel Blog { get; set; }

    }
    public class MetaTagModel : AuditableEntity
    {
        [Key]
        public int MetaTagId { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Page { get; set; }
        public int BlogId { get; set; }
        public int DoctorId { get; set; }
        public int DepartmentId { get; set; }
        public int ServiceId { get; set; }


    }
    public class OurTeamMemberModel : AuditableEntity
    {
        [Key]
        public int TeamMemberId { get; set; }
        public string ImagePath { get; set; }
        public string Designation { get; set; }
        public string FullName { get; set; }
        public string Department { get; set; }
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public int Sorting { get; set; }
        public bool DisableSorting { get; set; }
        public string CoverPhoto { get; set; }
        public bool IsCoreTeam { get; set; }
    }

    public class  OurMediaCoverageModel : AuditableEntity
    {
        [Key]
        public int MediaId { get; set; }
        public string ImagePath { get; set; }
        public string MediaURL { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
    }

    public class TestimonialMainModel : AuditableEntity
    {
        [Key]
        public int TestimonialMainId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string CustomerCount { get; set; }
    }
    public class TestimonialModel : AuditableEntity
    {
        [Key]
        public int TestimonialId { get; set; }
        public int Star { get; set; }
        public string Message { get; set; }
        public string WriterName { get; set; }
        public string WriterDesignation { get; set; }
        public string ImagePath { get; set; }

    }

    public class DanpheCareContactModel : AuditableEntity
    {
        [Key]
        public int DanpheCareContactId { get; set; }
        public string Location { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string GoogleMap { get; set; }
        public string OpeningHours { get; set; }

    }
    public class ExpatModel : AuditableEntity
    {
        [Key]
        public int ExpatId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }        
    }
    public class WebinarModel : AuditableEntity
    {
        [Key]
        public int WebinarId { get; set; }
        public string Title { get; set; }
        public string WebinarVideo { get; set; }
    }

    public class DepartmentTab: AuditableEntity
    {
        [Key]
        public int DepartmentTabId { get; set; }
        public string TabName { get; set; }

       
    }

    public class DepartmentTabContent: AuditableEntity
    {
        [Key]
        public int DepartmentContentId { get; set; }
        public int DepartmentTabId { get; set; }
        public DepartmentTab DepartmentTab { get; set; }
        public int DepartmentId { get; set; }
        public DepartmentModel Department { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
    public class DepartmentSubHeading : AuditableEntity
    {
        [Key]
        public int DepartmentSubHeadingId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string IconPath { get; set; }
        public int DepartmentId { get; set; }
        public DepartmentModel Department { get; set; }

    }
    public class PackageNameModel : AuditableEntity
    {
        [Key]
        public int PackageNameModelId { get; set; }
        public string PackageName { get; set; }
    }

    public class PackageDetails : AuditableEntity
    {
        [Key]
        public int PackageDetailId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int PackageNameModelId { get; set; }
        public PackageNameModel PackageNameModel { get; set; }
    }


    public class SubServices : AuditableEntity
    {
        [Key]
        public int SubServicesId { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public int OurServicesId { get; set; }
        public OurServicesModel OurServicesModel { get; set; }
    }

    public class SubServicesDetails : AuditableEntity
    {
        [Key]
        public int SubServicesDetailsId { get; set; }
        public string SubTitle { get; set; }
        public string Introduction { get; set; }
        public string ImagePath { get; set; }
        public string Content { get; set; }
        public int SubServicesId { get; set; }
        public SubServices SubServices { get; set; }
    }
}
