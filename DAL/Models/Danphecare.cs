﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace Telemedicine.ServerModel
{
   public class Danphecare
    {       
        [Key]
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public int ServiceId { get; set; }
        public string Service { get; set; }

        [Column(TypeName = "decimal(16,2)")]        
        public decimal Price { get; set; }
        public string PaymentProvider { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
