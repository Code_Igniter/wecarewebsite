﻿

 --------------------------------- Start Danphe Care CMS ---------------------------------------------

 -- Date:15th Dec 2020
GO

CREATE TABLE [AboutUs] (
    [AboutUsId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Title] nvarchar(max) NULL,
    [Content] nvarchar(max) NULL,
    [ShortDescription] nvarchar(max) NULL,
    [ImagePath] nvarchar(max) NULL,
    CONSTRAINT [PK_AboutUs] PRIMARY KEY ([AboutUsId])
);

GO

CREATE TABLE [BlogsModels] (
    [BlogId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Title] nvarchar(max) NULL,
    [Content] nvarchar(max) NULL,
    [ImagePath] nvarchar(max) NULL,
    [PostedBy] nvarchar(max) NULL,
    CONSTRAINT [PK_BlogsModels] PRIMARY KEY ([BlogId])
);

GO

CREATE TABLE [DanpheCareContactModels] (
    [DanpheCareContactId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Location] nvarchar(max) NULL,
    [Phone] nvarchar(max) NULL,
    [Fax] nvarchar(max) NULL,
    [Website] nvarchar(max) NULL,
    [GoogleMap] nvarchar(max) NULL,
    [OpeningHours] nvarchar(max) NULL,
    CONSTRAINT [PK_DanpheCareContactModels] PRIMARY KEY ([DanpheCareContactId])
);

GO

CREATE TABLE [DepartmentModels] (
    [DepartmentId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [DepartmentName] nvarchar(max) NULL,
    [IconPath] nvarchar(max) NULL,
    CONSTRAINT [PK_DepartmentModels] PRIMARY KEY ([DepartmentId])
);

GO

CREATE TABLE [ExpatModels] (
    [ExpatId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Name] nvarchar(max) NULL,
    [ImagePath] nvarchar(max) NULL,
    CONSTRAINT [PK_ExpatModels] PRIMARY KEY ([ExpatId])
);

GO

CREATE TABLE [NewsModels] (
    [NewsId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Title] nvarchar(max) NULL,
    [Content] nvarchar(max) NULL,
    [ImagePath] nvarchar(max) NULL,
    [PostedBy] nvarchar(max) NULL,
    CONSTRAINT [PK_NewsModels] PRIMARY KEY ([NewsId])
);

GO

CREATE TABLE [OurMediaCoverage] (
    [MediaId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [ImagePath] nvarchar(max) NULL,
    [MediaURL] nvarchar(max) NULL,
    [Name] nvarchar(max) NULL,
    [ShortDescription] nvarchar(max) NULL,
    CONSTRAINT [PK_OurMediaCoverage] PRIMARY KEY ([MediaId])
);

GO

CREATE TABLE [OurServices] (
    [ServiceId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [ServiceName] nvarchar(max) NULL,
    [Content] nvarchar(max) NULL,
    [IconPath] nvarchar(max) NULL,
    [ImagePath] nvarchar(max) NULL,
    [SubscriptionPlan] nvarchar(max) NULL,
    [SubscriptionContent] nvarchar(max) NULL,
    [SubscriptionImage] nvarchar(max) NULL,
    CONSTRAINT [PK_OurServices] PRIMARY KEY ([ServiceId])
);

GO

CREATE TABLE [OurTeamMembers] (
    [TeamMemberId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [ImagePath] nvarchar(max) NULL,
    [Designation] nvarchar(max) NULL,
    [FullName] nvarchar(max) NULL,
    [Department] nvarchar(max) NULL,
    [ShortDescription] nvarchar(max) NULL,
    [IsCoreTeam] bit NOT NULL,
    CONSTRAINT [PK_OurTeamMembers] PRIMARY KEY ([TeamMemberId])
);

GO

CREATE TABLE [TestimonialMain] (
    [TestimonialMainId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Title] nvarchar(max) NULL,
    [Content] nvarchar(max) NULL,
    [CustomerCount] nvarchar(max) NULL,
    CONSTRAINT [PK_TestimonialMain] PRIMARY KEY ([TestimonialMainId])
);

GO

CREATE TABLE [Testimonials] (
    [TestimonialId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Star] int NOT NULL,
    [Message] nvarchar(max) NULL,
    [WriterName] nvarchar(max) NULL,
    [WriterDesignation] nvarchar(max) NULL,
    [ImagePath] nvarchar(max) NULL,
    CONSTRAINT [PK_Testimonials] PRIMARY KEY ([TestimonialId])
);

GO

CREATE TABLE [WebinarModels] (
    [WebinarId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Title] nvarchar(max) NULL,
    [WebinarVideo] nvarchar(max) NULL,
    CONSTRAINT [PK_WebinarModels] PRIMARY KEY ([WebinarId])
);

GO

CREATE TABLE [DanpheCareDoctorModels] (
    [DoctorId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [FullName] nvarchar(max) NULL,
    [Designation] nvarchar(max) NULL,
    [Experience] nvarchar(max) NULL,
    [ImagePath] nvarchar(max) NULL,
    [CoverPhoto] nvarchar(max) NULL,
    [DepartmentId] int NOT NULL,
    CONSTRAINT [PK_DanpheCareDoctorModels] PRIMARY KEY ([DoctorId]),
    CONSTRAINT [FK_DanpheCareDoctorModels_DepartmentModels_DepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [DepartmentModels] ([DepartmentId]) ON DELETE CASCADE
);

GO

CREATE TABLE [DepartmentConsultationModels] (
    [DepartmentConsultationId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Title] nvarchar(max) NULL,
    [Content] nvarchar(max) NULL,
    [IconPath] nvarchar(max) NULL,
    [DepartmentId] int NOT NULL,
    CONSTRAINT [PK_DepartmentConsultationModels] PRIMARY KEY ([DepartmentConsultationId]),
    CONSTRAINT [FK_DepartmentConsultationModels_DepartmentModels_DepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [DepartmentModels] ([DepartmentId]) ON DELETE CASCADE
);




CREATE TABLE [DepartmentTabs] (
    [DepartmentTabId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [TabName] nvarchar(max) NULL,
    CONSTRAINT [PK_DepartmentTabs] PRIMARY KEY ([DepartmentTabId])
);



GO

INSERT INTO DepartmentTabs (TabName,CreatedDate,UpdatedDate)
VALUES ('FAQs', GETDATE(),GETDATE()),('Components',GETDATE(),GETDATE()),('Shock Setup',GETDATE(),GETDATE()),('Warranty and Registration',GETDATE(),GETDATE());

GO

CREATE TABLE [DepartmentTabContents] (
    [DepartmentContentId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [DepartmentTabId] int NOT NULL,
    [DepartmentId] int NOT NULL,
    [Title] nvarchar(max) NULL,
    [Content] nvarchar(max) NULL,
    CONSTRAINT [PK_DepartmentTabContents] PRIMARY KEY ([DepartmentContentId]),
    CONSTRAINT [FK_DepartmentTabContents_DepartmentModels_DepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [DepartmentModels] ([DepartmentId]) ON DELETE CASCADE,
    CONSTRAINT [FK_DepartmentTabContents_DepartmentTabs_DepartmentTabId] FOREIGN KEY ([DepartmentTabId]) REFERENCES [DepartmentTabs] ([DepartmentTabId]) ON DELETE CASCADE
);

GO


ALTER TABLE [OurServices] ADD [DepartmentId] int NULL;

GO

CREATE INDEX [IX_OurServices_DepartmentId] ON [OurServices] ([DepartmentId]);

GO

ALTER TABLE [OurServices] ADD CONSTRAINT [FK_OurServices_DepartmentModels_DepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [DepartmentModels] ([DepartmentId]) ON DELETE NO ACTION;

GO
CREATE TABLE [MetaTags] (
    [MetaTagId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NULL,
    [CreatedDate] datetime2 NULL,
    [Content] nvarchar(max) NULL,
    CONSTRAINT [PK_MetaTags] PRIMARY KEY ([MetaTagId])
);

GO
ALTER TABLE [DanpheCareContactModels]
ADD Email nvarchr(256) Null;
GO

ALTER TABLE [OurTeamMembers] ADD [Content] nvarchar(max) NULL;

GO

ALTER TABLE [DanpheCareDoctorModels] ADD [Content] nvarchar(max) NULL;

GO

-- 30th Dec

ALTER TABLE [OurTeamMembers] ADD [CoverPhoto] nvarchar(max) NULL;

GO

ALTER TABLE [OurTeamMembers] ADD [DisableSorting] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [OurTeamMembers] ADD [Sorting] int NOT NULL DEFAULT 0;

GO

ALTER TABLE [DanpheCareDoctorModels] ADD [DisableSorting] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [DanpheCareDoctorModels] ADD [Sorting] int NOT NULL DEFAULT 0;

GO

 --Date: 11th Jan 2021
GO

CREATE TABLE [ResourcefulArticlesModels] (
    [ResourcefulArticlesId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [Title] nvarchar(max) NULL,
    [Content] nvarchar(max) NULL,
    [ImagePath] nvarchar(max) NULL,
    CONSTRAINT [PK_ResourcefulArticlesModels] PRIMARY KEY ([ResourcefulArticlesId])
);

GO

CREATE TABLE [DepartmentSubHeadings] (
    [DepartmentSubHeadingId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [Title] nvarchar(max) NULL,
    [ShortDescription] nvarchar(max) NULL,
    [IconPath] nvarchar(max) NULL,
    [DepartmentId] int NOT NULL,
    CONSTRAINT [PK_DepartmentSubHeadings] PRIMARY KEY ([DepartmentSubHeadingId]),
    CONSTRAINT [FK_DepartmentSubHeadings_DepartmentModels_DepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [DepartmentModels] ([DepartmentId]) ON DELETE CASCADE
);

GO
  --13 Jan 2021
ALTER TABLE OurServices 
ADD CoverImage VARCHAR (255)  NULL;

GO

ALTER TABLE DepartmentModels 
ADD Title VARCHAR (max)  NULL;

Go
ALTER TABLE DepartmentModels 
ADD Introduction VARCHAR (max)  NULL;

Go
ALTER TABLE DepartmentModels 
ADD ImagePath VARCHAR (225)  NULL;

 --Date: 22nd Jan,2021
GO

CREATE TABLE [MetaTags] (
    [MetaTagId] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(256) NULL,
    [UpdatedBy] nvarchar(256) NULL,
    [UpdatedDate] datetime2 NOT NULL,
    [CreatedDate] datetime2 NOT NULL,
    [Content] nvarchar(max) NULL,
    [Title] nvarchar(max) NULL,
    [Keywords] nvarchar(max) NULL,
    [Page] nvarchar(max) NULL,
    CONSTRAINT [PK_MetaTags] PRIMARY KEY ([MetaTagId])
);

GO

-- Date 2nd Feb 2021

GO

ALTER TABLE [OurServices] ADD [Color] nvarchar(max) NULL;

GO

ALTER TABLE [OurServices] ADD [ShortIntroduction] nvarchar(max) NULL;

GO


--- Start Date 25th Feb 2021

GO

ALTER TABLE [OurServices] ADD [PermaLink] nvarchar(max) NULL;

GO

ALTER TABLE [DepartmentModels] ADD [PermaLink] nvarchar(max) NULL;

GO

ALTER TABLE [DanpheCareDoctorModels] ADD [PermaLink] nvarchar(max) NULL;

GO

ALTER TABLE [BlogsModels] ADD [PermaLink] nvarchar(max) NULL;

GO
----- End Date 25th Feb 2021

-------------------------------- End Danphe Care CMS -----------------------------------
