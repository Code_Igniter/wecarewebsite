﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork
    {
        ICustomerRepository Customers { get; }
        IProductRepository Products { get; }
        IOrdersRepository Orders { get; }
		IDanphecareRepository Danphecare { get; }
        IDanphecareReachUsQuickly DanphecareReachUsQuickly { get; }
        IContactUsRepository ContacUs { get; }
        IAboutUsRepository AboutUs { get; }
        IOurServicesRepository OurServices { get; }
        IDepartmentModelRepository DepartmentModels { get; }
        IDepartmentConsultationModelRepository DepartmentConsultationModels { get; }
        IDanpheCareDoctorModelRepository DanpheCareDoctorModels { get; }
        IResourcefulArticlesModelRepository ResourcefulArticlesModels { get; }
        INewsModelRepository NewsModels { get; }
        IBlogsModelRepository BlogsModels { get; }
        ICommentsModelRepository CommentsModels { get; }
        IMetaTagRepository MetaTagModels { get; }
        IOurTeamMemberRepository OurTeamMembers { get; }
        IOurMediaCoverageRepository OurMediaCoverage { get; }
        ITestimonialMainRepository TestimonialMain { get; }
        ITestimonialRepository Testimonials { get; }
        IDanpheCareContactRepository DanpheCareContactModels { get; }
        IExpatModelRepository ExpatModels { get; }
        IWebinarModelRepository WebinarModels { get; }
        IDepartmentTabRepository DepartmentTabs { get; }
        IDepartmentTabContentRepository DepartmentTabContents { get; }
        IDepartmentSubHeadingRepository DepartmentSubHeadings { get; }

        IPackageNameModelRepository PackageNameModels { get; }
        IPackageDetailsRepository PackageDetails { get; }

        ISubServicesRepository SubServices { get; }
        ISubServicesDetailsRepository SubServicesDetails { get; }


        int SaveChanges();
    }
}
