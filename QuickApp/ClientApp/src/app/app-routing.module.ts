// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule, DefaultUrlSerializer, UrlSerializer, UrlTree } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
/*import { HomeComponent } from './components/home/home.component';*/
import { CustomersComponent } from './components/customers/customers.component';
import { ProductsComponent } from './components/products/products.component';
import { OrdersComponent } from './components/orders/orders.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AboutComponent } from './components/about/about.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { Utilities } from './services/utilities';
import { HomePageComponent } from './website/home/homepage.component';
import { AboutUsPageComponent } from './website/aboutus/aboutus.component';
import { BlogPageComponent } from './website/blog/blog.component';
import { BlogDetailsPageComponent } from './website/blog/blogdetails.component';
import { ContactPageComponent } from './website/contact/contact.component';
import { ExpatPageComponent } from './website/expat/expat.component';
import { NewsPageComponent } from './website/news/news.component';
import { NewsDetailsPageComponent } from './website/news/newsdetails.component';
import { ProfilePageComponent } from './website/profile/profile.component';
import { ServicesPageComponent } from './website/services/services.component';
import { ServiceDetailsPageComponent } from './website/services/servicedetails.component';
import { SpecialistPageComponent } from './website/profile/specialist.component';
import { TeamPageComponent } from './website/team/team.component';
import { CoreTeamProfileComponent } from './website/team/core-team-profile.component';
import { DepartmentDetailsPageComponent } from './website/department/departmentdetails-page.component';
import { ArticlesPageComponent } from './website/profile/articles-page.component';
import { DepartmentDetailsListComponent } from './website/department/departmentdetails-list.component';
import { HamroPatroComponent } from './website/hamropatra/hamropatra.component';
import { CMSPackageComponent } from './cms/components/package/package.component';
import { CMSPackageDetailsComponent } from './cms/components/package/package-details.component';
import { CMSPackageDetailsAddComponent } from './cms/components/package/package-details-add.component';
import { CMSPackageDetailsEditComponent } from './cms/components/package/package-details-update.component';



@Injectable()
export class LowerCaseUrlSerializer extends DefaultUrlSerializer {
    parse(url: string): UrlTree {
        const possibleSeparators = /[?;#]/;
        const indexOfSeparator = url.search(possibleSeparators);
        let processedUrl: string;

        if (indexOfSeparator > -1) {
            const separator = url.charAt(indexOfSeparator);
            const urlParts = Utilities.splitInTwo(url, separator);
            urlParts.firstPart = urlParts.firstPart.toLowerCase();

            processedUrl = urlParts.firstPart + separator + urlParts.secondPart;
        } else {
            processedUrl = url.toLowerCase();
        }

        return super.parse(processedUrl);
    }
}


const routes: Routes = [
   {
    path: '', component: LoginComponent,
        
      },
      { path: 'aboutpage', component: AboutUsPageComponent },
      { path: 'homepage', component: HomePageComponent },
      { path: 'blogpage', component: BlogPageComponent },
      { path: 'blog/:permalink', component: BlogDetailsPageComponent },
      { path: 'contactpage', component: ContactPageComponent },
      { path: 'expatpage', component: ExpatPageComponent },
      { path: 'newspage', component: NewsPageComponent },
      { path: 'newsdetails', component: NewsDetailsPageComponent },
      { path: 'profilepage/:permalink', component: ProfilePageComponent },
      { path: 'servicepage', component: ServicesPageComponent },
      { path: 'service/:permalink', component: ServiceDetailsPageComponent },
      { path: 'specialist', component: SpecialistPageComponent },
      { path: 'teampage', component: TeamPageComponent },
      { path: 'teamprofile', component: CoreTeamProfileComponent },
      { path: 'department/:permalink', component: DepartmentDetailsPageComponent },
      { path: 'articlespage', component: ArticlesPageComponent },
      { path: 'coreteamdetails', component: CoreTeamProfileComponent },
      { path: 'departmentdetailslist', component: DepartmentDetailsListComponent },
    //{ path: '', loadChildren: './website/website.module#WebsiteModule' },
      { path: 'cms', loadChildren: './cms/cms.module#CMSModule', canActivate: [AuthGuard] },
    //{ path: '', component: HomeComponent, canActivate: [AuthGuard], data: { title: 'Home' } },
    { path: 'login', component: LoginComponent, data: { title: 'Login' } },
    { path: 'customers', component: CustomersComponent, canActivate: [AuthGuard], data: { title: 'Customers' } },
    { path: 'products', component: ProductsComponent, canActivate: [AuthGuard], data: { title: 'Products' } },
    { path: 'orders', component: OrdersComponent, canActivate: [AuthGuard], data: { title: 'Orders' } },
    { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard], data: { title: 'Settings' } },
  { path: 'about', component: AboutComponent, data: { title: 'About Us' } },
  { path: 'hamropatro', component: HamroPatroComponent, data: { title: 'Hamro Patro' } },

    { path: 'home', redirectTo: '/', pathMatch: 'full' },
    { path: '**', component: NotFoundComponent, data: { title: 'Page Not Found' } }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
    exports: [RouterModule],
    providers: [
        AuthService,
        AuthGuard,
        { provide: UrlSerializer, useClass: LowerCaseUrlSerializer }]
})
export class AppRoutingModule { }
