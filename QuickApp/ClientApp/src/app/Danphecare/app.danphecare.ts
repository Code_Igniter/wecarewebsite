import { Component, Inject, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DanphecareModel, DanpheCareReachUsQuicklyModel } from '../models/danphecare/danphecare.model';
import { DanphecareService } from '../services/danphecare/danphe.service';
import { AlertService } from '../services/alert.service';
import { NotificationService } from '../services/notification.service';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
//import * as jwt_decode from 'jwt-decode';
import { User } from '../models/user/user.model';
import { AuthenticationService } from '../services/authentication.service';
import { MatSelectChange, MatDialogModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxCaptchaModule, ReCaptcha2Component } from 'ngx-captcha';
import { ConfigService } from '../../common/common.appconfig';
import { Global } from '../app.global';

@Component({
  templateUrl: './app.danphecare.html'
})
export class DanphecareComponent implements OnInit {
  public Dhcare: DanphecareModel = new DanphecareModel();

  public DhcareRuq: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  homeIsolationApplyForm: FormGroup;
  reachUsQuicklyForm: FormGroup;
  // public token: any;
  jwtToken: any;
  grecaptcha: any;
  loginVal: any;
  dataFromAppsettings: any;
  siteKey: string;
  siteKey_ruq: string;
  captchaRes: any[];
  loading = false;
  loading_ruq = false;
  public showPaymentMethod: boolean = false;
  public showMoreContent: boolean = false;
  public submitted: boolean = false;
  public submitted_ruq: boolean = false;
  public reCAPTCHAstatus: boolean = false;
  public Usrlist: Array<any> = new Array<any>();
 // @ViewChild('captchaElem', { static: true }) captchaElem: ReCaptcha2Component;
  //@ViewChild('captchaElem1', { static: true }) captchaElem1: ReCaptcha2Component;
  constructor(public routing: Router, @Inject(MAT_DIALOG_DATA) public data: any, public http: HttpClient, private formBuilder: FormBuilder, public danphecareservice: DanphecareService, private notifyService: NotificationService, private alertService: AlertService,
    private authenticationService: AuthenticationService, private configService: ConfigService, public global: Global ) {
    //this.GetAppliedUsrlist();
   // this.dataFromAppsettings = this.configService.loadAppConfig();
    this.siteKey = this.global.config.SiteKey;
    console.log('siteKey');
    console.log(this.siteKey);
  //  this.siteKey_ruq = "6LdhT_MZAAAAAEicw0yG6h0tcvGRyPK9l562a5X7";
    // this.token = this.authenticationService.currentUserValue;
    //this.getPatientName();
  }
  ngOnInit() {
    this.CaptchaCallback();
    // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  get f() {
    return this.homeIsolationApplyForm.controls;
  }
  get g() {
    return this.reachUsQuicklyForm.controls;
  }

  GetAppliedUsrlist() {
    this.danphecareservice.GetAppliedUsrlist()
      .subscribe(res => this.SuccessGetAppliedUsrlist(res),
        res => this.GetAppliedUsrlistError(res));

  }

  SuccessGetAppliedUsrlist(res) {
    this.Usrlist = res;
    for (var i = 0; i < this.Usrlist.length; i++) {
      this.Usrlist[i].CreatedOn = moment(this.Usrlist[i].CreatedOn).format("YYYY-MM-DD");
    }
    this.notifyService.showSuccess('Success', ' User list Load');
  }
  GetAppliedUsrlistError(res) {
    this.notifyService.showError("Error", " field to load User list")
  }

  onSubmit() {
   // this.CaptchaCallback();
    this.submitted = true;
    this.loading = true;
    if (this.homeIsolationApplyForm.invalid) {
      //this.submitted = false;
      this.loading = false;
      this.reCAPTCHAstatus == false;
      this.notifyService.showError("Error", " Please fill up the form properly")
    }
    if (this.homeIsolationApplyForm.valid) {
      if (this.reCAPTCHAstatus == true) {
        if (this.Dhcare != null) {
          this.danphecareservice.applytoIsolation(this.Dhcare)
            .subscribe(res => this.SuccessPostapplyIso(res),
              res => this.Error(res));
        }
      } else {
        this.notifyService.showError("Error", "Human verification failed.");
        this.loading = false;
      }
    }
    //if (this.homeIsolationApplyForm.value.name.invalid) {
    //  this.notifyService.showError("", "Name is required");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.phonenumber.invalid) {
    //  this.notifyService.showError("", "Mobile number is required");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.phonenumber.invalid) {
    //  this.notifyService.showError("", "Mobile number must be 10 digits");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.email.invalid) {
    //  this.notifyService.showError("", "Improper Email Format");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.email.invalid) {
    //  this.notifyService.showError("", "Email is required");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.message.invalid) {
    //  this.notifyService.showError("", "Message is required");
    //  this.loading = false;
    //}
  }

  SuccessPostapplyIso(res) {
    this.submitted = false;
    this.homeIsolationApplyForm.reset();
    // this.GetAppliedUsrlist();
    this.notifyService.showSuccess('Success', 'Apply Successfully ');
    this.reCAPTCHAstatus = false;
    this.loading = false;
    this.CaptchaCallback();
  }
  SuccessPostReachUsQuickly(res) {
    this.submitted_ruq = false;
    this.reachUsQuicklyForm.reset();
    // this.GetAppliedUsrlist();
    this.notifyService.showSuccess('Success', 'Apply Successfully ');
  //  this.reCAPTCHAstatusRUQ = false;
    this.loading_ruq = false;
    this.CaptchaCallback();
  }

  Error(res) {
    var response = res;
    this.submitted = false;
    this.submitted_ruq = false;
    this.loading = false;
    this.loading_ruq = false;
    this.notifyService.showError("Error", " Please fill up the required field")
  }

  onSubmitReachUsQuickly() {
    // this.CaptchaCallback();
    this.submitted_ruq = true;
    this.loading_ruq = true;
    if (this.reachUsQuicklyForm.invalid) {
      this.loading_ruq = false;
      this.notifyService.showError("Error", " Please fill up the form properly.")
    }
    if (this.reachUsQuicklyForm.valid) {
      // reCAPTCHAstatusRUQ
      if (this.DhcareRuq != null) {
        this.danphecareservice.reachUsQuickly(this.DhcareRuq)
          .subscribe(res => this.SuccessPostReachUsQuickly(res),
            res => this.Error(res));
      }
    }
  }

  PaymentMethod() {
    this.showPaymentMethod = true;
  }
  hide() {
    this.showPaymentMethod = false;
    this.showMoreContent = false;
  }
  knowmore() {
    this.showMoreContent = true;
  }
  handleSuccess(recaptchaRes: any[]) {
    this.captchaRes = recaptchaRes;
    if (this.captchaRes.length > 1) {
      this.reCAPTCHAstatus = true;
    }
    console.log("recaptchaResponse");
    console.log(this.captchaRes);
  }
  //handleRUQSuccess(recaptchaRes: any[]) {
  //  this.captchaRes = recaptchaRes;
  //  if (this.captchaRes.length > 1) {
  //    //this.reCAPTCHAstatusRUQ = true;
  //  }
  //  console.log("recaptchaResponse");
  //  console.log(this.captchaRes);
  //}
  GotoTelemedicine() {
    this.routing.navigate(['/login']);
  }
  public CaptchaCallback() {
    this.homeIsolationApplyForm = this.formBuilder.group({
      name: ["", [Validators.required],
      ],
      phonenumber: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      //password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]]
      email: ["", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ],
      message: ["", [Validators.required],
      ],
      homeIsoRecaptcha: [""],

      termsandcondition:[ false, Validators.requiredTrue]
    });
    this.reachUsQuicklyForm = this.formBuilder.group({
      ruq_name: ["", [Validators.required],
      ],
      ruq_phonenumber: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      //password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]]
      ruq_email: [
        "", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ],
      ruq_message: ["", [Validators.required],
      ],
      ruq_company: ["", [Validators.required],
      ]
    });
  }
}



