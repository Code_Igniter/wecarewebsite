(function ($) {
    "use strict";

    document.documentElement.className = "js";


    //Preloader

    $(window).on('load', function (e) { // makes sure the whole site is loaded
        $(".loader__figure").fadeOut(); // will first fade out the loading animation
        $(".loader").delay(500).fadeOut("slow"); // will fade out the white DIV that covers the website.
    })


    //Parallax & fade on scroll

    function scrollBanner() {
        $(document).on('scroll', function () {
            var scrollPos = $(this).scrollTop();
            if ($(window).width() > 1200) {
                $('.parallax-top').css({
                    'top': (scrollPos / 2.5) + 'px'
                });
                $('.parallax-fade-top').css({
                    'top': (scrollPos / 2) + 'px',
                    'opacity': 1 - (scrollPos / 750)
                });
                $('.fade-top').css({
                    'opacity': 1 - (scrollPos / 350)
                });
            }
        });
    }
    scrollBanner();


    /* Scroll Animation */

    window.scrollReveal = new scrollReveal();


    /* Parallax effect */

    if ($(window).width() > 991) {
        $().enllax();
    }


    // $(document).ready(function () {


        //Scroll back to top

        var offset = 300;
        var duration = 400;
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > offset) {
                $('.scroll-to-top').fadeIn(duration);
            } else {
                $('.scroll-to-top').fadeOut(duration);
            }
        });

        $('.scroll-to-top').on('click', function (event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, duration);
            return false;
        })


        //Navigation

        "use strict";


        //Nice Select

        $('select').niceSelect();


    // });
    // Select all links with hashes
    // Testimonials carousel (uses the Owl Carousel library)
    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            900: {
                items: 2
            }
        }
    });
    $(".service-carousel").owlCarousel({
        autoplay: true,
        dots: false,
        nav: true,
        loop: false,

        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            900: {
                items: 2
            }
        }
    });
    $(".specialities-carousel").owlCarousel({
        autoplay: true,
        dots: false,
        nav: true,
        loop: true,

        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 3
            },
            900: {
                items: 5
            }
        }
    });
    //$(".center").slick({
    //        dots: true,
    //	    prevArrow: false,
    //        nextArrow: false,
    //        infinite: true,
    //        centerMode: true,
    //        slidesToShow: 5,
    //        slidesToScroll: 3
    //      });
    $('.center').slick({
        centerMode: true,
        dots: false,
        centerPadding: '0',
        slidesToShow: 5,
        prevArrow: '<span class="prev_arrow"><i class="fa fa-angle-left"></i></span>',
        nextArrow: '<span class="next_arrow"><i class="fa fa-angle-right"></i></span>',
        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            }
        ]
    });
    $('.webinars').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
		prevArrow: '<span class="prev_arrow"><i class="fa fa-angle-left"></i></span>',
        nextArrow: '<span class="next_arrow"><i class="fa fa-angle-right"></i></span>',
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
	$('.consults').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
		prevArrow: '<span class="prev_arrow"><i class="fa fa-angle-left"></i></span>',
        nextArrow: '<span class="next_arrow"><i class="fa fa-angle-right"></i></span>',
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

})($);
