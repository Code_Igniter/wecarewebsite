// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

import { NgModule, ErrorHandler, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { OAuthModule } from 'angular-oauth2-oidc';
import { ToastaModule } from 'ngx-toasta';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppErrorHandler } from './app-error.handler';
import { AppTitleService } from './services/app-title.service';
import { AppTranslationService, TranslateLanguageLoader } from './services/app-translation.service';
import { ConfigurationService } from './services/configuration.service';
import { AlertService } from './services/alert.service';
import { ThemeManager } from './services/theme-manager';
import { LocalStoreManager } from './services/local-store-manager.service';
import { OidcHelperService } from './services/oidc-helper.service';
import { NotificationService } from './services/notification.service';
import { NotificationEndpoint } from './services/notification-endpoint.service';
import { AccountService } from './services/account.service';
import { AccountEndpoint } from './services/account-endpoint.service';

import { EqualValidator } from './directives/equal-validator.directive';
import { LastElementDirective } from './directives/last-element.directive';
import { AutofocusDirective } from './directives/autofocus.directive';
import { BootstrapTabDirective } from './directives/bootstrap-tab.directive';
import { BootstrapToggleDirective } from './directives/bootstrap-toggle.directive';
import { GroupByPipe } from './pipes/group-by.pipe';

import { AppComponent } from './components/app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CustomersComponent } from './components/customers/customers.component';
import { ProductsComponent } from './components/products/products.component';
import { OrdersComponent } from './components/orders/orders.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AboutComponent } from './components/about/about.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

import { BannerDemoComponent } from './components/controls/banner-demo.component';
import { TodoDemoComponent } from './components/controls/todo-demo.component';
import { StatisticsDemoComponent } from './components/controls/statistics-demo.component';
import { NotificationsViewerComponent } from './components/controls/notifications-viewer.component';
import { SearchBoxComponent } from './components/controls/search-box.component';
import { UserInfoComponent } from './components/controls/user-info.component';
import { UserPreferencesComponent } from './components/controls/user-preferences.component';
import { UsersManagementComponent } from './components/controls/users-management.component';
import { RolesManagementComponent } from './components/controls/roles-management.component';
import { RoleEditorComponent } from './components/controls/role-editor.component';
import { CMSModule } from './cms/cms.module';
//import { WebsiteModule } from './website/website.module';
import { SharedModule } from './shared/shared.module';
import { ContactUsService } from './services/contactUs/contactUs.service';
import { ContactUsEndpoint } from './services/contactUs/contactUs-endpoint.service';
import { DanphecareService } from './services/danphecare/danphe.service';
import { DanphecareEndpoint } from './services/danphecare/danphecare-endpoint.service';
import { ConfigService } from '../common/common.appconfig';
import { Global } from './app.global';
import { HeaderComponent } from './website/others/header.component';
import { DanpheFooterComponent } from './website/others/footer.component';
import { HomePageComponent } from './website/home/homepage.component';
import { AboutUsPageComponent } from './website/aboutus/aboutus.component';
import { BlogPageComponent } from './website/blog/blog.component';
import { BlogDetailsPageComponent } from './website/blog/blogdetails.component';
import { ServiceDetailsPageComponent } from './website/services/servicedetails.component';
import { ServicesPageComponent } from './website/services/services.component';
import { ProfilePageComponent } from './website/profile/profile.component';
import { NewsPageComponent } from './website/news/news.component';
import { SpecialistPageComponent } from './website/profile/specialist.component';
import { DepartmentDetailsPageComponent } from './website/department/departmentdetails-page.component';
import { ArticlesPageComponent } from './website/profile/articles-page.component';
import { ExpatPageComponent } from './website/expat/expat.component';
import { ContactPageComponent } from './website/contact/contact.component';
import { NewsDetailsPageComponent } from './website/news/newsdetails.component';
import { TeamPageComponent } from './website/team/team.component';
import { CoreTeamProfileComponent } from './website/team/core-team-profile.component';
import { WebsiteService } from './website/websiteservice/website.service';
import { WebsiteEndpoint } from './website/websiteservice/website-endpoint.service';
import { ScriptService } from './website/scriptservice/script.service';
import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { DepartmentDetailsListComponent } from './website/department/departmentdetails-list.component';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { MetaModule } from '@ngx-meta/core';
import { HamroPatroComponent } from './website/hamropatra/hamropatra.component';
import { CMSPackageComponent } from './cms/components/package/package.component';
import { CMSPackageDetailsComponent } from './cms/components/package/package-details.component';
import { CMSPackageDetailsAddComponent } from './cms/components/package/package-details-add.component';
import { CMSPackageDetailsEditComponent } from './cms/components/package/package-details-update.component';




const initializerConfigFn = (config: ConfigService) => {
  return () => {
    var ret: any = config.loadAppConfig();
    return ret;
  };
};
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: TranslateLanguageLoader
      }
    }),
    NgxDatatableModule,
    OAuthModule.forRoot(),
    ToastaModule.forRoot(),
    NgSelectModule,
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    ChartsModule,
    CMSModule,
    SharedModule,
    ShareButtonsModule.withConfig({
      debug: true
    }),
    ShareIconsModule,
     MetaModule.forRoot()
    
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CustomersComponent,
    ProductsComponent,
    OrdersComponent,
    SettingsComponent,
    UsersManagementComponent, UserInfoComponent, UserPreferencesComponent,
    RolesManagementComponent, RoleEditorComponent,
    AboutComponent,
    NotFoundComponent,
    NotificationsViewerComponent,
    SearchBoxComponent,
    StatisticsDemoComponent, TodoDemoComponent, BannerDemoComponent,
    EqualValidator,
    LastElementDirective,
    AutofocusDirective,
    BootstrapTabDirective,
    BootstrapToggleDirective,
    GroupByPipe,
    HeaderComponent, DanpheFooterComponent, HomePageComponent, AboutUsPageComponent, BlogPageComponent, BlogDetailsPageComponent,
    ContactPageComponent, ExpatPageComponent, NewsPageComponent, ProfilePageComponent, ServicesPageComponent, ServiceDetailsPageComponent,
    TeamPageComponent, NewsDetailsPageComponent, SpecialistPageComponent, ProfilePageComponent, DepartmentDetailsPageComponent, ArticlesPageComponent,
    CoreTeamProfileComponent, DepartmentDetailsListComponent, HamroPatroComponent
  ],
  providers: [
    { provide: ErrorHandler, useClass: AppErrorHandler },
    {
      provide: APP_INITIALIZER,
      useFactory: initializerConfigFn,
      multi: true,
      deps: [ConfigService],
    },
    { provide: LocationStrategy, useClass: PathLocationStrategy } ,
    AlertService,
    ThemeManager,
    ConfigurationService,
    AppTitleService,
    AppTranslationService,
    NotificationService,
    NotificationEndpoint,
    AccountService,
    AccountEndpoint,
    LocalStoreManager,
    OidcHelperService,
    ContactUsService,
    ContactUsEndpoint,
    DanphecareService,
    DanphecareEndpoint,
    Global,
    WebsiteService, WebsiteEndpoint, ScriptService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
