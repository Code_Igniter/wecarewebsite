
import { DomSanitizer } from '@angular/platform-browser';

export class Global {
    
    IsDoctor: boolean = false;
    DoctorId: string = null;
    HospitalId: string = null;
    DepartmentId: string = null;
    BookingTime: string = null;
    VisitDate: Date = null;
    ApiUrl: string = "Home/";
    config: any = null;
    TimerValue: number = 4000;
    apiLogout: boolean = false;
    constructor() {
    }


}
