
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EndpointBase } from '../endpoint-base.service';
import { ConfigurationService } from '../configuration.service';
import { AuthService } from '../auth.service';

@Injectable()
export class DanphecareEndpoint extends EndpointBase {
  
  private readonly _GetAppliedUsrlistUrl: string = "/api/danphecare/GetAppliedUsrlist";
  private readonly _applytoIsolationUrl: string = "/api/danphecare/ApplyToIsolation";  
  private readonly _reachUsQuicklyUrl: string = "/api/danphecare/ReachUsQuickly";  

  constructor(private configurations: ConfigurationService, http: HttpClient, authService: AuthService) {
    super(http, authService);
  }

  GetAppliedUsrlist<T>(): Observable<T>{
   const endpointUrl = `${this._GetAppliedUsrlistUrl}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetAppliedUsrlist()));
      }));
  }

  applytoIsolation<T>(userObject: any): Observable<T> {

    return this.http.post<T>(this._applytoIsolationUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.applytoIsolation(userObject)));
      }));
  }
  reachUsQuickly<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._reachUsQuicklyUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.reachUsQuickly(userObject)));
      }));
  }
}
