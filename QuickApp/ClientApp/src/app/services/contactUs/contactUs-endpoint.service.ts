
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EndpointBase } from '../endpoint-base.service';
import { ConfigurationService } from '../configuration.service';
import { AuthService } from '../auth.service';

@Injectable()
export class ContactUsEndpoint extends EndpointBase {

  private readonly _GetlistofContactedUsrUrl: string = "/api/contactus/GetlistOfContactedUsr";
  private readonly _PostContactformdataUrl: string = "/api/contactus/PostContactformdata";
  
  


  constructor(private configurations: ConfigurationService, http: HttpClient, authService: AuthService) {
    super(http, authService);
  }

  GetlistOfContactedUsr<T>(): Observable<T>{
   const endpointUrl = `${this._GetlistofContactedUsrUrl}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetlistOfContactedUsr()));
      }));
  }

  PostContactformdata<T>(userObject: any): Observable<T> {

    return this.http.post<T>(this._PostContactformdataUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.PostContactformdata(userObject)));
      }));
  }
}
