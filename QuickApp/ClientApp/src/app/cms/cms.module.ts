import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CMSRoutingModule } from "./cms.routing";
import { AboutUsComponent } from "./components/about-us/about-us.component";
import { OurServicesComponent } from "./components/our-services/our-services.component";
import { SideMenuComponent } from "./components/sidemenu/side-menu.component";
import { CMSEndpoint } from "./services/cms-endpoint.service";
import { CMSService } from "./services/cms.service";
import { CKEditorModule } from 'ngx-ckeditor';
import { HeaderCMSComponent } from "./components/others/header.component";
import { FooterCMSComponent } from "./components/others/footer.component";
import { CMSBlogsComponent } from "./components/blogs/cms-blogs.component";
import { CMSContactComponent } from "./components/contact/cms-contact.component";
import { CMSDepartmentComponent } from "./components/department/cms-department.component";
import { CMSDepartmentConsultationComponent } from "./components/departmentconsultation/cms-dep-consultation.compoent";
import { CMSDoctorComponent } from "./components/doctor/cms-doctor.component";
import { CMSExpatComponent } from "./components/expat/cms-expat.component";
import { CMSMetaTagComponent } from "./components/metatag/cms-metatag.component";
import { CMSNewsComponent } from "./components/news/cms-news.component";
import { CMSArticlesComponent } from "./components/resourcefularticles/cms-articles.component";
import { CMSWebinarComponent } from "./components/webinar/cms-webinar.component";

import { OurTeamMemberComponent } from "./components/our-team/cms-our-team.component";
import { OurMediaCoverageComponent } from "./components/our-media-coverage/cms-our-media-coverage.component";
import { TestimonialComponent } from "./components/testimonial/cms-our-testimonial.component";
import { CMSDepartmentTabContentComponent } from "./components/department/department-tab.component";

import { NgxCkeditorModule } from 'ngx-ckeditor4';
import { CMSAddBlogsComponent } from './components/blogs/cms-blogs-add.component';
import { CMSDepartmentTabAddContentComponent } from './components/department/department-tab-add.component';
import { CMSDepartmentAddConsultationComponent } from './components/departmentconsultation/cms-dep-consultation-add.component';
import { CMSAddExpatComponent } from './components/expat/cms-expat-add.component';
import { CMSAddNewsComponent } from './components/news/cms-news-add.component';
import { CMSArticlesAddComponent } from './components/resourcefularticles/cms-articles-add.component';
import { OurServicesAddComponent } from './components/our-services/our-services-add.component';
import { TestimonialAddComponent } from './components/testimonial/cms-testimonial-add.component';
import { NgxPaginationModule } from "ngx-pagination";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { ToastrModule } from "ngx-toastr";
import { DepartmentSubHeadingComponent } from './components/department/department-sub-heading.component';
import { CMSDoctorAddComponent } from './components/doctor/cms-doctor-add.component';
import { CMSDoctorEditComponent } from './components/doctor/cms-doctor-edit.component';
import { CMSDepartmentEditComponent } from './components/department/cms-department-edit.component';
import { OurServicesEditComponent } from './components/our-services/our-services-edit.component';
import { CMSMetaTagBlogComponent } from "./components/metatag/cms-metatag-blog.component";
import { CMSMetaTagDepartmentComponent } from "./components/metatag/cms-metatag-department.component";
import { CMSMetaTagServiceComponent } from "./components/metatag/cms-metatag-service.component";
import { CMSMetaTagDoctorComponent } from "./components/metatag/cms-metatag-doctor.component";
import { CMSPackageComponent } from "./components/package/package.component";
import { CMSPackageDetailsComponent } from "./components/package/package-details.component";
import { CMSPackageDetailsAddComponent } from "./components/package/package-details-add.component";
import { CMSPackageDetailsEditComponent } from "./components/package/package-details-update.component";
import { CMSSubServicesComponent } from "./components/sub-services/cms-sub-services.component";
import { CMSSubServicesAddComponent } from "./components/sub-services/cms-sub-services-add.component";
import { CMSSubServicesDetailsAddComponent } from "./components/sub-services/cms-sub-services-details-add.component";
import { CMSSubServicesEditComponent } from "./components/sub-services/cms-sub-services-edit.component";
import { CMSSubServicesDetailsComponent } from "./components/sub-services/cms-sub-services-details.component";
import { CMSSubServicesDetailsEditComponent } from "./components/sub-services/cms-sub-services-details-edit.component";



@NgModule({
  declarations: [
    SideMenuComponent,
    AboutUsComponent,
    OurServicesComponent,
    HeaderCMSComponent,
    FooterCMSComponent,
    CMSBlogsComponent,
    CMSContactComponent,
    CMSDepartmentComponent,
    CMSDepartmentConsultationComponent,
    CMSDoctorComponent,
    CMSExpatComponent,
    CMSMetaTagComponent,
    CMSNewsComponent,
    CMSArticlesComponent,
    CMSWebinarComponent,
    OurTeamMemberComponent,
    OurMediaCoverageComponent,
    TestimonialComponent,
    CMSDepartmentTabContentComponent,
    CMSAddBlogsComponent,
    CMSDepartmentTabAddContentComponent,
    CMSDepartmentAddConsultationComponent,
    CMSAddExpatComponent,
    CMSAddNewsComponent,
    CMSArticlesAddComponent,
    OurServicesAddComponent,
    TestimonialAddComponent,
    DepartmentSubHeadingComponent,
    CMSDoctorAddComponent,
    CMSDoctorEditComponent,
    CMSDepartmentEditComponent,
    OurServicesEditComponent, CMSMetaTagBlogComponent, CMSMetaTagDepartmentComponent,
    CMSMetaTagDoctorComponent, CMSMetaTagServiceComponent, CMSPackageComponent, CMSPackageDetailsComponent, CMSPackageDetailsAddComponent,
    CMSPackageDetailsEditComponent, CMSSubServicesComponent, CMSSubServicesAddComponent, CMSSubServicesEditComponent, CMSSubServicesDetailsComponent, CMSSubServicesDetailsAddComponent, CMSSubServicesDetailsEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CMSRoutingModule,
    CKEditorModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    ToastrModule.forRoot(),
    NgxCkeditorModule.forRoot({
      url: 'https://cdn.bootcss.com/ckeditor/4.11.3/ckeditor.js',
      config: {
        filebrowserUploadMethod: 'xhr',
        filebrowserUploadUrl: 'http://127.0.0.1:8000/index/index/uploads',
      },
    }),

  ],
  providers: [
   
    CMSService,
    CMSEndpoint,
  ]
})
export class CMSModule {

}
