
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { Expat } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cms-expat.component.html',
  styleUrls:["./cms-expat.component.css"]
})


export class CMSExpatComponent implements OnInit {

  public selectedCMSExpat: Expat = new Expat();
  public expatList: Array<Expat> = new Array<Expat>();
  progress: number;
  message: string;
  file: any;
  previewImgURL: any;
  EditMode: boolean;
  ImagePath: string;
  p: number = 1;
  public searchText: any;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetExpat();

  }
  AddCMSExpat(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.cmsService.AddExpat(this.selectedCMSExpat).subscribe(res => {
        // this.showPopupModel=false;
        this.GetExpat();
        this.notifyService.showSuccess('Success', 'Expat Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateExpat();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    let mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.selectedCMSExpat.isImagePathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {
      let mimeType = this.file.type;
      if (mimeType.match(/image\/*/) === null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      let reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
    } else {
      let reader = new FileReader();
    }
  }

  GetExpat() {
    this.cmsService.GetExpat().subscribe(res => {
      if (res) {
        this.expatList = [];
        this.expatList = Object.assign(this.expatList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Expat Found!");
      });
  }

  EditExpat(id) {
    this.selectedCMSExpat = this.expatList.find(a => a.expatId == id);
    this.previewImgURL = this.selectedCMSExpat.imagePath;
    this.ImagePath = "";
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateExpat() {
    if (this.ImagePath !== "") {
      this.selectedCMSExpat.imagePath = "";
      this.selectedCMSExpat.imagePath = this.ImagePath;
      this.selectedCMSExpat.isImagePathUploaded = true;
    }
    else {
      this.selectedCMSExpat.imagePath = this.selectedCMSExpat.imagePath;
    }
    //this.selectedCMSExpat.imagePath = this.ImagePath;
    this.cmsService.UpdateExpat(this.selectedCMSExpat).subscribe(res => {
      // this.showPopupModel=false;
      this.GetExpat();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteExpat(id) {
    if (confirm("Do you Want to delete this Expat?")) {
      this.cmsService.DeleteExpat(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetExpat();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSExpat = new Expat();
    this.previewImgURL = null;
    this.EditMode = false;
    this.routing.navigate(['cms/addexpat']);
    // this.showPopupModel = true;   
  }
}
