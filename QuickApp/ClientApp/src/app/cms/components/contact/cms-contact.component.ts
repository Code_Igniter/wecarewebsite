
import { Component, OnInit } from '@angular/core';
import { AboutUsModel } from '../../models/aboutUs.model';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { DanpheCareContact } from '../../models/danphecare.cms.model';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  templateUrl: './cms-contact.component.html'
})


export class CMSContactComponent implements OnInit {

  public contact: DanpheCareContact = new DanpheCareContact();
  progress: number;
  message: string;
  file: any;
  ShowPreview: boolean;
  previewImgURL: any;
  EditMode: boolean = false;
  // public classicCKeditor = ClassicEditor;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService) {
    ////
  }
  ngOnInit() {

    this.GetContact();

  }
  AddContact(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }
    if (!this.EditMode) {
      this.cmsService.AddContact(this.contact).subscribe(res => {
        this.GetContact();
        this.notifyService.showSuccess('', 'Contact Us Added Successfully!');
      },
        res => this.Error(res));
    } else {
      this.cmsService.UpdateContact(this.contact).subscribe(res => {
        this.GetContact();
        this.notifyService.showSuccess('', 'Contact Us Updated Successfully!');
      },
        res => this.Error(res));
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

 

  GetContact() {
    this.cmsService.GetContact().subscribe(res => {
      if (res && res.length > 0) {
        this.contact = Object.assign(this.contact, res[0]);
        this.ShowPreview = true;
        this.EditMode = true;
      }
    },
      res => {
        this.EditMode = false;
        this.notifyService.showError("Error", "Internal Error")
      });
  }

}
