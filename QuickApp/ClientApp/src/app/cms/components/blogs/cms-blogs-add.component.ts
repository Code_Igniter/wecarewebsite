
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { Blogs } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cms-blogs-add.component.html',
  styleUrls: ["./cms-blogs.component.css"]
})


export class CMSAddBlogsComponent implements OnInit {


  public selectedCMSBlogs: Blogs = new Blogs();
  public blogsList: Array<Blogs> = new Array<Blogs>();
  progress: number;
  message: string;
  file: any;
  previewImgURL: any;
  EditMode: boolean;


  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetBlogs();

  }
  AddCMSBlogs(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

      this.cmsService.AddBlogs(this.selectedCMSBlogs).subscribe(res => {
        // this.showPopupModel=false;
        this.routing.navigate(['cms/cmsblogs']);
        //this.GetBlogs();
        this.notifyService.showSuccess('Success', 'Blogs Added Successfully!');
        //document.getElementById("close1").click();
      },
        res => this.Error(res));
   

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    let mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.selectedCMSBlogs.isImagePathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {
      let mimeType = this.file.type;
      if (mimeType.match(/image\/*/) === null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      let reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
    } else {
      let reader = new FileReader();
    }
  }

  GetBlogs() {
    this.cmsService.GetBlogs().subscribe(res => {
      if (res) {
        this.blogsList = [];
        this.blogsList = Object.assign(this.blogsList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Blogs Found!");
      });
  }

  EditBlogs(id) {
    this.selectedCMSBlogs = this.blogsList.find(a => a.blogId === id);
    this.previewImgURL = this.selectedCMSBlogs.imagePath;
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateBlogs() {
    this.cmsService.UpdateBlogs(this.selectedCMSBlogs).subscribe(res => {
      // this.showPopupModel=false;
      this.GetBlogs();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteBlogs(id) {
    if (confirm("Do you Want to delete this Blogs?")) {
      this.cmsService.DeleteBlog(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetBlogs();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSBlogs = new Blogs();
    this.previewImgURL = null;
    this.routing.navigate(['/addblogs']);
    //this.EditMode = false;
    // this.showPopupModel = true;   
  }
  Back() {
    this.selectedCMSBlogs = new Blogs();
    this.previewImgURL = null;
    this.routing.navigate(['cms/cmsblogs']);
  }
}
