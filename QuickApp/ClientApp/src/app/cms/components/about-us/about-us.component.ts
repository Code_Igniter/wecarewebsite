
import { Component, OnInit } from '@angular/core';
import { AboutUsModel } from '../../models/aboutUs.model';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  templateUrl: './about-us.component.html',
  styleUrls: ["./about-us.component.css"]
})


export class AboutUsComponent implements OnInit {

  public aboutUs: AboutUsModel = new AboutUsModel();
  progress: number;
  message: string;
  file: any;
  ShowPreview: boolean;
  previewImgURL: any;
  EditMode: boolean = false;
  ImagePath: string;
  // public classicCKeditor = ClassicEditor;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService) {
    ////
  }
  ngOnInit() {
    //$('#sidebarCollapse').on('click', function () {
    //  $('#sidebar').toggleClass('active');
    //});
    this.GetAboutUs();

  }
  AddAboutUs(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }
    if (!this.EditMode) {
      this.aboutUs.imagePath = this.ImagePath;
      this.cmsService.AddAboutUs(this.aboutUs).subscribe(res => {
        this.GetAboutUs();
        this.notifyService.showSuccess('', 'About Us Added Successfully!');
      },
        res => this.Error(res));
    } else {
      if (this.ImagePath !== "") {
        this.aboutUs.imagePath = this.ImagePath;
        this.aboutUs.isImagePathUploaded = true;
      }
      else {
        this.aboutUs.imagePath = this.aboutUs.imagePath;
      }
      this.aboutUs.imagePath = this.ImagePath;
      this.cmsService.UpdateAboutUs(this.aboutUs).subscribe(res => {
        this.GetAboutUs();
        this.notifyService.showSuccess('', 'About Us Updated Successfully!');
      },
        res => this.Error(res));
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    var mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.aboutUs.isImagePathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {
      var mimeType = this.file.type;
      if (mimeType.match(/image\/*/) == null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
      this.ShowPreview = true;
    } else {
      this.ShowPreview = false;
      var reader = new FileReader();
    }
  }

  GetAboutUs() {
    this.cmsService.GetAboutUs().subscribe(res => {
      if (res && res.length > 0) {
        this.aboutUs = Object.assign(this.aboutUs, res[0]);
        this.previewImgURL = this.aboutUs.imagePath;
        this.ShowPreview = true;
        this.EditMode = true;
      }
    },
      res => {
        this.EditMode = false;
        this.notifyService.showError("Error", "Internal Error")
      });
  }

  // onCKeditorChange(data) {
  //   this.aboutUs.ShortDescription = data;
  // }
}
