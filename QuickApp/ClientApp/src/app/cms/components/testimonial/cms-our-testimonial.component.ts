
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { Testimonial, TestimonialMain } from '../../models/danphecare.cms.model';
import { Router } from '@angular/router';
import { ToastaService } from 'ngx-toasta';
import { AlertService, MessageSeverity } from '../../../services/alert.service';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  templateUrl: './cms-our-testimonial.component.html',
  styleUrls: ["./cms-our-testimonial.component.css"]
})


export class TestimonialComponent implements OnInit {
  public selectedObject: Testimonial = new Testimonial();
  public objectList: Array<Testimonial> = new Array<Testimonial>();
  public mainObject: TestimonialMain = new TestimonialMain();
  progress: number;
  message: string;
  file: any;
  previewImgURL: any;
  EditMode: boolean = false;
  mainEdit: boolean = false;
  ImagePath: string;
  p: number = 1;
  public searchText: any;
  // showPopupModel: boolean = false;
  // public classicCKeditor = ClassicEditor;

  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router, private toastaService: ToastaService, private alertService: AlertService,) {
    ////
  }
  ngOnInit() {
    //$('#sidebarCollapse').on('click', function () {
    //  $('#sidebar').toggleClass('active');
    //});
    this.GetTestimonialMain();
    this.GetTestimonials();

  }
  AddTestimonial(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.cmsService.AddTestimonial(this.selectedObject).subscribe(res => {
        // this.showPopupModel=false;
        this.GetTestimonials();
        this.notifyService.showSuccess('Success', 'Testimonial Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateTestimonial();
    }

  }
  AddTestimonialMain(form: NgForm)
  {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.mainEdit) {
      this.mainObject.customerCount = this.mainObject.customerCount.toString();
      this.cmsService.AddTestimonialMain(this.mainObject).subscribe(res => {
        // this.showPopupModel=false;
        this.GetTestimonialMain();
        this.notifyService.showSuccess('Success', 'Testimonial Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateTestimonialMain();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    var mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.selectedObject.isImagePathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {
      var reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
    } else {
      var reader = new FileReader();
    }
  }

  GetTestimonials() {
    this.cmsService.GetTestimonials().subscribe(res => {
      if (res) {
        this.objectList = [];
        this.objectList = Object.assign(this.objectList, res);
      }
    },
      res => {
        console.log("Error: " + res);
      });
  }

  GetTestimonialMain() {
    this.cmsService.GetTestimonialMain().subscribe(res => {
      if (res && res.length > 0) {
        this.mainObject = new TestimonialMain();
        this.mainObject = Object.assign(this.mainObject, res[0]);
        this.mainEdit = true;
      }
    },
      res => {
        console.log("Error: " + res);
      });
  }

  EditData(id) {
    this.selectedObject = this.objectList.find(a => a.testimonialId == id);
    this.previewImgURL = this.selectedObject.imagePath;
    this.ImagePath = "";
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateTestimonial() {
    if (this.ImagePath !== "") {
      this.selectedObject.imagePath = "";
      this.selectedObject.imagePath = this.ImagePath;
      this.selectedObject.isImagePathUploaded = true;
    }
    else {
      this.selectedObject.imagePath = this.selectedObject.imagePath;
    }
    this.cmsService.UpdateTestimonial(this.selectedObject).subscribe(res => {
      // this.showPopupModel=false;
      this.GetTestimonials();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }

  UpdateTestimonialMain() {
    this.mainObject.customerCount = this.mainObject.customerCount.toString();
    this.cmsService.UpdateTestimonialMain(this.mainObject).subscribe(res => {
      // this.showPopupModel=false;
      this.alertService.showMessage('Testimonial Updated Successfully!', '', MessageSeverity.success);
      this.notifyService.showSuccess('Success', 'Testimonial Updated Successfully!');
      this.GetTestimonialMain();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }

  DeleteData(id) {
    if (confirm("Do you Want to delete this Testimonials?")) {
      this.cmsService.DeleteTestimonial(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetTestimonials();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedObject = new Testimonial();
    this.previewImgURL = null;
    this.EditMode = false;
    this.routing.navigate(['cms/addtestimonial']);
    // this.showPopupModel = true;   
  }
}

