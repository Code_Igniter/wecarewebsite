
import { Component, OnInit } from '@angular/core';
import { OurServiceModel } from '../../models/OurServices.model';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { Router } from '@angular/router';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  templateUrl: './our-services-add.component.html',
  styleUrls: ["./our-services.component.css"]
})


export class OurServicesAddComponent implements OnInit {

  public selectedOurServices: OurServiceModel = new OurServiceModel();
  public ourServicesList: Array<OurServiceModel> = new Array<OurServiceModel>();
  progress: number;
  progress1: number;
  progress2: number;
  progress4: number;
  message: string;
  // file: any;
  previewImgURL: any;
  EditMode: boolean;
  previewImgURL1: any;
  previewImgURL2: any;
  previewImgURL4: any;
  public filteredDepartmentList: Array<any> = new Array<any>();
  public selDepartment: any;
  // showPopupModel: boolean = false;
  // public classicCKeditor = ClassicEditor;

  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    //$('#sidebarCollapse').on('click', function () {
    //  $('#sidebar').toggleClass('active');
    //});
    this.GetOurServices();
  /*  this.GetDepartment();*/

  }
  GetDepartment() {
    this.cmsService.GetDepartment()
      .subscribe(res => this.SuccessDepartment(res),
        res => this.Error(res));
  }
  SuccessDepartment(res) {
    this.filteredDepartmentList = res;
  /*  this.selDepartment = res[0].departmentId;*/
  }
  AddOurService(form: NgForm) {
   /* this.GetDepartment();*/
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

   /* this.selectedOurServices.departmentId = this.selDepartment;*/
      this.cmsService.AddService(this.selectedOurServices).subscribe(res => {
        // this.showPopupModel=false;
        this.routing.navigate(['cms/services']);
        this.GetOurServices();
        this.notifyService.showSuccess('Success', 'Service Added Successfully!');
        //document.getElementById("close1").click();
      },
        res => this.Error(res));
   

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  // UploadSubmitt(id:number,files){
  //   this.upload(files)
  // }

  upload(files, id: number) {

    if (files.length === 0)
      return;

    let file = files[0];
    let mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files) {
      formData.append(file.name, file);

      if (id == 0) { // Icon Image Upload
        this.selectedOurServices.imagePath = file.name;
      }
      //} else if (id == 1) { // Details Image Upload
      //  this.selectedOurServices.imagePath = file.name;
      //} else if (id == 2) { // Subscription Image upload
      //  this.selectedOurServices.subscriptionImage = file.name;
      //}
      //else if (id == 4) { // Cover Image upload
      //  this.selectedOurServices.coverImage = file.name;
      //}
    }


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {

        if (id == 0) { // Icon Image Upload
          this.progress = Math.round(100 * event.loaded / event.total);
          this.selectedOurServices.isIconPathUploaded = true;

        } else if (id == 1) { // Details Image Upload
          this.progress1 = Math.round(100 * event.loaded / event.total);
          this.selectedOurServices.isImagePathUploaded = true;

        } else if (id == 2) { // Subscription Image upload
          this.progress2 = Math.round(100 * event.loaded / event.total);
          this.selectedOurServices.isSubscriptionImageUploaded = true;
        }
        else if (id == 4) { // Cover Image upload
          this.progress2 = Math.round(100 * event.loaded / event.total);
          this.selectedOurServices.isCoverImagePathUploaded = true;
        }

      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();

    });
    this.ShowPreviewImage(file, id);

  }

  ShowPreviewImage(file, id: number) {

    if (file) {
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        if (id == 0) { // Icon Image Preview
          this.previewImgURL = reader.result;
        } else if (id == 1) { // Details Image Preview

          this.previewImgURL1 = reader.result;

        } else if (id == 2) { // Subscription Image Preview
          this.previewImgURL2 = reader.result;
        }
        else if (id == 4) { // Subscription Image Preview
          this.previewImgURL4 = reader.result;
        }
      }
    } else {
      var reader = new FileReader();
    }
  }

  GetOurServices() {
    this.cmsService.GetOurServices().subscribe(res => {
      if (res) {
        this.ourServicesList = [];
        this.ourServicesList = Object.assign(this.ourServicesList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Services Found!");
      });
  }

  EditService(id) {
    this.selectedOurServices = new OurServiceModel;
    this.selectedOurServices = this.ourServicesList.find(a => a.serviceId == id);
    this.previewImgURL = this.selectedOurServices.iconPath;
    this.previewImgURL1 = this.selectedOurServices.imagePath;
    this.previewImgURL2 = this.selectedOurServices.subscriptionImage;
    this.previewImgURL4 = this.selectedOurServices.coverImage;
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateService() {
  /*  this.selectedOurServices.departmentId = this.selDepartment;*/
    this.cmsService.UpdateService(this.selectedOurServices).subscribe(res => {
      // this.showPopupModel=false;
      this.GetOurServices();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteService(id) {
    if (confirm("Do you Want to delete this service?")) {
      this.cmsService.DeleteService(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetOurServices();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedOurServices = new OurServiceModel();
    this.previewImgURL = null;
    this.previewImgURL1 = null;
    this.previewImgURL2 = null;
    this.EditMode = false;
    this.routing.navigate(['cms/addservices']);
    // this.showPopupModel = true;   
  }
  Back() {
    this.EditMode = false;
    this.routing.navigate(['cms/services']);
  }
}
