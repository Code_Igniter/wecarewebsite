
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { ResourcefulArticles } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cms-articles-add.component.html',
  styleUrls: ['./cms-articles.component.css']
})


export class CMSArticlesAddComponent implements OnInit {

  public selectedCMSArticles: ResourcefulArticles = new ResourcefulArticles();
  public articlesList: Array<ResourcefulArticles> = new Array<ResourcefulArticles>();
  progress: number;
  message: string;
  file: any;
  previewImgURL: any;
  EditMode: boolean;
  public filteredDoctorList: Array<any> = new Array<any>();
  public selDoctor: any;
  ImagePath: string;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetArticles();
    this.GetDoctor();

  }

  GetDoctor() {
    this.cmsService.GetDoctor()
      .subscribe(res => this.SuccessDoctor(res),
        res => this.Error(res));
  }
  SuccessDoctor(res) {
    this.filteredDoctorList = res;
  }

  AddArticles(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }


    //this.selectedCMSArticles.doctorId = this.selDoctor.doctorId;
    //this.selectedCMSArticles.imagePath = this.ImagePath;
    this.cmsService.AddArticles(this.selectedCMSArticles).subscribe(res => {
      this.routing.navigate(['cms/cmsarticles']);
        this.GetArticles();
        this.notifyService.showSuccess('Success', 'Articles Added Successfully!');
        //document.getElementById("close1").click();
      },
        res => this.Error(res));
   

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    let mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.selectedCMSArticles.isImagePathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {
      let mimeType = this.file.type;
      if (mimeType.match(/image\/*/) === null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      let reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
    } else {
      let reader = new FileReader();
    }
  }

  GetArticles() {
    this.cmsService.GetArticles().subscribe(res => {
      if (res) {
        this.articlesList = [];
        this.articlesList = Object.assign(this.articlesList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Articles Found!");
      });
  }

  EditArticles(id) {
    this.selectedCMSArticles = this.articlesList.find(a => a.resourcefulArticlesId == id);
    this.previewImgURL = this.selectedCMSArticles.imagePath;
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
  }

  UpdateArticles() {
    this.selectedCMSArticles.doctorId = this.selDoctor.doctorId;
    this.cmsService.UpdateArticles(this.selectedCMSArticles).subscribe(res => {
      this.GetArticles();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteArticles(id) {
    if (confirm("Do you Want to delete this Articles?")) {
      this.cmsService.DeleteArticles(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetArticles();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSArticles = new ResourcefulArticles();
    this.previewImgURL = null;
    this.EditMode = false;
    this.routing.navigate(['cms/addarticles']);
  }
  Back() {
    this.EditMode = false;
    this.routing.navigate(['cms/cmsarticles']);
  }
}
