
import { Component, OnInit } from '@angular/core';
import { TeamMember } from '../../models/danphecare.cms.model';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { isNullOrUndefined } from '@swimlane/ngx-datatable';
import { isUndefined } from 'util';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  templateUrl: './cms-our-team.component.html',
  styleUrls: ["./cms-our-team.component.css"]
})


export class OurTeamMemberComponent implements OnInit {

  public selectedTeamMember: TeamMember = new TeamMember();
  public ourTeamMemberList: Array<TeamMember> = new Array<TeamMember>();
  progress: number;
  progress1: number;
  message: string;
  file: any;
  previewImgURL: any;
  previewImgURL1: any;
  EditMode: boolean;
  ImagePath: string;
  CoverPhoto: string;
  p: number = 1;
  public searchText: any;
  public filteredSortingList: Array<any> = new Array<any>();
  public filteredList: Array<any> = new Array<any>();
  public selSorting: any;
  public sortingNo: number;
  public sortingList: Array<any> = new Array<any>();
  // showPopupModel: boolean = false;
  // public classicCKeditor = ClassicEditor;

  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService) {
    ////
  }
  ngOnInit() {
    //$('#sidebarCollapse').on('click', function () {
    //  $('#sidebar').toggleClass('active');
    //});
    this.GetOurTeamMember();
    //this.GetSorting();

  }
  GetSorting() {
    this.cmsService.GetSorting()
      .subscribe(res => this.SuccessSorting(res),
        res => this.Error(res));
  }
  SuccessSorting(res) {
    this.filteredSortingList = res;
    this.sortingList = new Array<any>();
    for (let i = 1; i < this.filteredSortingList.length + 1; i++) {
      this.sortingList.push({ id: i, value: i });
    }
    this.filteredList = this.sortingList.filter(x => !this.filteredSortingList.some(y => x.value === y.sorting));


  }
  AddOurTeamMember(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.selectedTeamMember.imagePath = this.ImagePath;
      this.selectedTeamMember.coverPhoto = this.CoverPhoto;
      this.selectedTeamMember.sorting = this.selSorting;
      this.filteredList = new Array<any>();
      this.cmsService.AddOurTeamMember(this.selectedTeamMember).subscribe(res => {
        // this.showPopupModel=false;
        this.GetOurTeamMember();
        this.selSorting = 0;
        this.GetSorting();
        this.notifyService.showSuccess('Success', 'Team Member Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateOurTeamMember();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files, id: number) {

    if (files.length === 0)
      return;

    let file = files[0];
    let mimeType = file.type;
    if (mimeType.match(/image\/*/) === null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files) {
      formData.append(file.name, file);

      if (id == 0) { //  Image Upload
        this.selectedTeamMember.imagePath = file.name;

      }
      else if (id == 1) { // cover Image Upload
        this.selectedTeamMember.coverPhoto = file.name;
      }
    }


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {

        if (id == 0) { // Icon Image Upload
          this.progress = Math.round(100 * event.loaded / event.total);
          this.selectedTeamMember.isImagePathUploaded = true;

        }
        else if (id == 1) { // Details Image Upload
          this.progress1 = Math.round(100 * event.loaded / event.total);
          this.selectedTeamMember.isCoverPhotoUploaded = true;

        }

      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();

    });
    this.ShowPreviewImage(file, id);

  }

  ShowPreviewImage(file, id: number) {

    if (file) {
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        if (id == 0) { // profile Image Preview
          this.previewImgURL = reader.result;
        }
        else if (id == 1) { // cover Image Preview

          this.previewImgURL1 = reader.result;

        }
      }
    } else {
      var reader = new FileReader();
    }
  }

  GetOurTeamMember() {
    this.cmsService.GetOurTeamMember().subscribe(res => {
      if (res) {
        this.ourTeamMemberList = [];
        this.ourTeamMemberList = Object.assign(this.ourTeamMemberList, res);
      }
    },
      res => {
        console.log("Error Message: " + res);
        // this.notifyService.showError("Info", "No Services Found!");
      });
  }

  EditData(id) {
    this.selectedTeamMember = this.ourTeamMemberList.find(a => a.teamMemberId === id);

    
    this.previewImgURL = null;
    this.previewImgURL1 = null;
    this.previewImgURL = this.selectedTeamMember.imagePath;
    this.previewImgURL1 = this.selectedTeamMember.coverPhoto;
    this.ImagePath = "";
    this.CoverPhoto = "";
    this.EditMode = true;
    this.filteredList = new Array<any>();
    this.GetSorting();
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateOurTeamMember() {
    if (this.ImagePath !== "") {
      this.selectedTeamMember.imagePath = "";
      this.selectedTeamMember.imagePath = this.ImagePath;
      this.selectedTeamMember.isImagePathUploaded = true;
    }
    else {
      this.selectedTeamMember.imagePath = this.selectedTeamMember.imagePath;
    }
    if (this.CoverPhoto !== "") {
      this.selectedTeamMember.coverPhoto = "";
      this.selectedTeamMember.coverPhoto = this.CoverPhoto;
      this.selectedTeamMember.isCoverPhotoUploaded = true;
    }
    else {
      this.selectedTeamMember.coverPhoto = this.selectedTeamMember.coverPhoto;
    }
    if (this.selSorting !== 0) {
      this.selectedTeamMember.sorting = this.selSorting;
    }

    this.cmsService.UpdateOurTeamMember(this.selectedTeamMember).subscribe(res => {
      // this.showPopupModel=false;
      this.GetOurTeamMember();
      this.selSorting = 0;
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteOurTeamMember(id) {
    if (confirm("Do you Want to delete this service?")) {
      this.cmsService.DeleteOurTeamMember(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetOurTeamMember();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedTeamMember = new TeamMember();
    this.previewImgURL = null;
    this.EditMode = false;
    // this.showPopupModel = true;   
  }
}
