
import { Component, OnInit } from '@angular/core';
import { OurServiceModel } from '../../models/OurServices.model';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { MediaCoverage } from '../../models/danphecare.cms.model';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  templateUrl: './cms-our-media-coverage.component.html',
  styleUrls: ["./cms-our-media-coverage.component.css"]
})


export class OurMediaCoverageComponent implements OnInit {

  public selectedObject: MediaCoverage = new MediaCoverage();
  public objectList: Array<MediaCoverage> = new Array<MediaCoverage>();
  progress: number;
  message: string;
  file: any;
  previewImgURL: any;
  EditMode: boolean;
  ImagePath: string;
  public searchText: any;
  p: number = 1;
  // showPopupModel: boolean = false;
  // public classicCKeditor = ClassicEditor;
  
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService) {
    ////
  }
  ngOnInit() {
    //$('#sidebarCollapse').on('click', function () {
    //  $('#sidebar').toggleClass('active');
    //});
    this.GetOurMediaCoverage();

  }
  AddOurMediaCoverage(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }
    
    if (!this.EditMode) {
      this.selectedObject.imagePath = this.ImagePath;
      this.cmsService.AddOurMediaCoverage(this.selectedObject).subscribe(res => {
        // this.showPopupModel=false;
        this.GetOurMediaCoverage();
        this.notifyService.showSuccess('Success', 'Media Coverage Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    }else{
        this.UpdateOurMediaCoverage();
    }
    
  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    var mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.selectedObject.isImagePathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {      
      var reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
    } else {
      var reader = new FileReader();
    }
  }

  GetOurMediaCoverage() {
    this.cmsService.GetOurMediaCoverage().subscribe(res => {
      if (res) {
        this.objectList = [];
        this.objectList = Object.assign(this.objectList, res);        
      }
    },
      res => {
        console.log("Error: "+res);
      });
  }

  EditData(id) {
    this.selectedObject = this.objectList.find(a => a.mediaId == id);
    this.previewImgURL = this.selectedObject.imagePath;
    this.ImagePath = "";
    this.EditMode = true;    
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateOurMediaCoverage() {
    if (this.ImagePath !== "") {
      this.selectedObject.imagePath = "";
      this.selectedObject.imagePath = this.ImagePath;
      this.selectedObject.isImagePathUploaded = true;
    }
    else {
      this.selectedObject.imagePath = this.selectedObject.imagePath;
    }
    this.cmsService.UpdateOurMediaCoverage(this.selectedObject).subscribe(res => {
      // this.showPopupModel=false;
      this.GetOurMediaCoverage();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteData(id) {
    if(confirm("Do you Want to delete this Media Coverage?")){
      this.cmsService.DeleteOurMediaCoverage(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetOurMediaCoverage();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }    
  }
  AddBtnClick(){
    this.selectedObject = new MediaCoverage();
    this.previewImgURL = null;
    this.EditMode = false;
    // this.showPopupModel = true;   
  }
}
