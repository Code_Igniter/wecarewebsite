
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { Webinar } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';


@Component({
  templateUrl: './cms-webinar.component.html',
  styleUrls:["./cms-webinar.component.css"]
})


export class CMSWebinarComponent implements OnInit {

  public selectedCMSWebinar: Webinar = new Webinar();
  public webinarList: Array<Webinar> = new Array<Webinar>();

  EditMode: boolean;
  p: number = 1;
  public searchText: any;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService) {
    ////
  }
  ngOnInit() {
    this.GetWebinar();

  }
  AddCMSWebinar(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.cmsService.AddWebinar(this.selectedCMSWebinar).subscribe(res => {
        // this.showPopupModel=false;
        this.GetWebinar();
        this.notifyService.showSuccess('Success', 'Webinar Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateWebinar();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  
  GetWebinar() {
    this.cmsService.GetWebinar().subscribe(res => {
      if (res) {
        this.webinarList = [];
        this.webinarList = Object.assign(this.webinarList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Webinar Found!");
      });
  }

  EditWebinar(id) {
    this.selectedCMSWebinar = this.webinarList.find(a => a.webinarId == id);
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateWebinar() {
    this.cmsService.UpdateWebinar(this.selectedCMSWebinar).subscribe(res => {
      // this.showPopupModel=false;
      this.GetWebinar();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteWebinar(id) {
    if (confirm("Do you Want to delete this Webinar?")) {
      this.cmsService.DeleteWebinar(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetWebinar();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSWebinar = new Webinar();
    this.EditMode = false;
    // this.showPopupModel = true;   
  }
}
