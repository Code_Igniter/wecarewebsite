
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { DanpheCareDoctor } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cms-doctor-add.component.html'
})


export class CMSDoctorAddComponent implements OnInit {

  public selectedCMSDoctor: DanpheCareDoctor = new DanpheCareDoctor();
  public doctorList: Array<DanpheCareDoctor> = new Array<DanpheCareDoctor>();
  progress: number;
  progress1: number;
  progress2: number;
  message: string;
  file: any;
  previewImgURL1: any;
  previewImgURL2: any;
  EditMode: boolean;
  ImagePath: string;
  CoverPhoto: string;
  public filteredDepartmentList: Array<any> = new Array<any>();
  public selDepartment: any;
  p: number = 1;
  public searchText: any;
  public filteredSortingList: Array<any> = new Array<any>();
  public filteredList: Array<any> = new Array<any>();
  public selSorting: any;
  public sortingNo: number;
  public sortingList: Array<any> = new Array<any>();
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetDoctor();
  /*  this.GetDepartment();*/
    //this.GetSorting();

  }
  GetSorting() {
    this.cmsService.GetDoctorSorting()
      .subscribe(res => this.SuccessSorting(res),
        res => this.Error(res));
  }
  SuccessSorting(res) {
    this.filteredSortingList = res;
    this.sortingList = new Array<any>();
    for (let i = 1; i < this.filteredSortingList.length + 1; i++) {
      this.sortingList.push({ id: i, value: i });
    }
    this.filteredList = this.sortingList.filter(x => !this.filteredSortingList.some(y => x.value === y.sorting));

  }

  GetDepartment() {
    this.cmsService.GetDepartment()
      .subscribe(res => this.SuccessDepartment(res),
        res => this.Error(res));
  }
  SuccessDepartment(res) {
    this.filteredDepartmentList = res;

  }

  AddCMSDoctors(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      /*this.selectedCMSDoctor.departmentId = this.selDepartment.departmentId;*/
      this.selectedCMSDoctor.imagePath = this.ImagePath;
      this.selectedCMSDoctor.coverPhoto = this.CoverPhoto;
      this.selectedCMSDoctor.sorting = this.selSorting;
      this.filteredList = new Array<any>();
      this.cmsService.AddDoctor(this.selectedCMSDoctor).subscribe(res => {
        this.GetDoctor();
        this.selSorting = 0;
        this.GetSorting();
        this.notifyService.showSuccess('Success', 'Doctor Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateDoctor();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files, id: number) {

    if (files.length === 0)
      return;

    let file = files[0];
    let mimeType = file.type;
    if (mimeType.match(/image\/*/) === null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files) {
      formData.append(file.name, file);

      if (id == 0) { //  Image Upload
        this.selectedCMSDoctor.imagePath = file.name;

      }
      else if (id == 1) { // cover Image Upload
        this.selectedCMSDoctor.coverPhoto = file.name;
      }
    }


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {

        if (id == 0) { // Icon Image Upload
          this.progress = Math.round(100 * event.loaded / event.total);
          this.selectedCMSDoctor.isImagePathUploaded = true;

        }
        else if (id == 1) { // Details Image Upload
          this.progress1 = Math.round(100 * event.loaded / event.total);
          this.selectedCMSDoctor.isCoverPhotoUploaded = true;

        }

      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();

    });
    this.ShowPreviewImage(file, id);

  }

  ShowPreviewImage(file, id: number) {

    if (file) {
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        if (id == 0) { // Icon Image Preview
          this.previewImgURL1 = reader.result;
        }
        else if (id == 1) { // Details Image Preview

          this.previewImgURL2 = reader.result;

        }
      }
    } else {
      var reader = new FileReader();
    }
  }

  GetDoctor() {
    this.cmsService.GetDoctor().subscribe(res => {
      if (res) {
        this.doctorList = [];
        this.doctorList = Object.assign(this.doctorList, res);
        this.sortingNo = this.doctorList.length;
      }
    },
      res => {
        this.notifyService.showError("Info", "No Doctor Found!");
      });
  }

  EditDoctor(id) {
    this.selectedCMSDoctor = this.doctorList.find(a => a.doctorId == id);
   /* this.selDepartment = this.filteredDepartmentList.find(a => a.departmentId == this.selectedCMSDoctor.departmentId);*/

    this.previewImgURL1 = null;
    this.previewImgURL2 = null;
    this.previewImgURL1 = this.selectedCMSDoctor.imagePath;
    this.previewImgURL2 = this.selectedCMSDoctor.coverPhoto;
    this.ImagePath = "";
    this.CoverPhoto = "";
    this.filteredList = new Array<any>();
    this.GetSorting();
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
  }

  UpdateDoctor() {
    if (this.ImagePath !== "") {
      this.selectedCMSDoctor.imagePath = "";
      this.selectedCMSDoctor.imagePath = this.ImagePath;
      this.selectedCMSDoctor.isImagePathUploaded = true;
    }
    else {
      this.selectedCMSDoctor.imagePath = this.selectedCMSDoctor.imagePath;
    }
    if (this.CoverPhoto !== "") {
      this.selectedCMSDoctor.coverPhoto = "";
      this.selectedCMSDoctor.coverPhoto = this.CoverPhoto;
      this.selectedCMSDoctor.isCoverPhotoUploaded = true;
    }
    else {
      this.selectedCMSDoctor.coverPhoto = this.selectedCMSDoctor.coverPhoto;
    }
    //this.selectedCMSDoctor.imagePath = this.ImagePath;
    //this.selectedCMSDoctor.coverPhoto = this.CoverPhoto;
    //this.selectedCMSDoctor.isImagePathUploaded = true;
    //this.selectedCMSDoctor.isCoverPhotoUploaded = true;
    this.selectedCMSDoctor.departmentId = 0;
    if (this.selSorting !== 0) {
      this.selectedCMSDoctor.sorting = this.selSorting;
    }

    this.cmsService.UpdateDoctor(this.selectedCMSDoctor).subscribe(res => {
      this.GetDoctor();
      this.selSorting = 0;
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteDoctor(id) {
    if (confirm("Do you Want to delete this Doctor?")) {
      this.cmsService.DeleteDoctor(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetDoctor();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSDoctor = new DanpheCareDoctor();
    this.previewImgURL1 = null;
    this.EditMode = false;
  }
  Back() {
    this.EditMode = false;
    this.routing.navigate(['cms/cmsdoctor']);
  }
}
