
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import {  DanpheCareDepartment } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cms-department.component.html'
})


export class CMSDepartmentComponent implements OnInit {

  public selectedDepartment: DanpheCareDepartment = new DanpheCareDepartment();
  public departmentList: Array<DanpheCareDepartment> = new Array<DanpheCareDepartment>();
  progress: number;
  progress1: number;
  message: string;
  file: any;
  previewImgURL: any;
  previewImgURL1: any;
  EditMode: boolean;
  IconPath: string;
  ImagePath: string;
  p: number = 1;
  public searchText: any;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetDepartment();

  }
  AddCMSDepartment(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.selectedDepartment.iconPath = this.IconPath;
      this.selectedDepartment.imagePath = this.ImagePath;

      this.cmsService.AddDepartment(this.selectedDepartment).subscribe(res => {
        // this.showPopupModel=false;
        this.GetDepartment();
        this.notifyService.showSuccess('Success', 'Department Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateDepartment();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }
  upload(files, id: number) {

    if (files.length === 0)
      return;

    let file = files[0];
    let mimeType = file.type;
    if (mimeType.match(/image\/*/) === null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files) {
      formData.append(file.name, file);

      if (id === 0) { //  Image Upload
        this.selectedDepartment.iconPath = file.name;

      }
      else if (id == 1) { // cover Image Upload
        this.selectedDepartment.imagePath = file.name;
      }
    }


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {

        if (id == 0) { // Icon Image Upload
          this.progress = Math.round(100 * event.loaded / event.total);
          this.selectedDepartment.isIconPathUploaded = true;

        }
        else if (id == 1) { // Details Image Upload
          this.progress1 = Math.round(100 * event.loaded / event.total);
          this.selectedDepartment.isImagePathUploaded = true;

        }

      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();

    });
    this.ShowPreviewImage(file, id);

  }

  ShowPreviewImage(file, id: number) {

    if (file) {
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        if (id == 0) { // Icon Image Preview
          this.previewImgURL = reader.result;
        }
        else if (id == 1) { // Details Image Preview

          this.previewImgURL1 = reader.result;

        }
      }
    } else {
      var reader = new FileReader();
    }
  }

  GetDepartment() {
    this.cmsService.GetDepartment().subscribe(res => {
      if (res) {
        this.departmentList = [];
        this.departmentList = Object.assign(this.departmentList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Department Found!");
      });
  }

  EditDepartment(id) {
    this.selectedDepartment = new DanpheCareDepartment();
    this.previewImgURL1 = null;
    this.EditMode = false;
    this.routing.navigate(['cms/cmsdepartmentedit'], { queryParams: { id: id } });
    //this.selectedDepartment = this.departmentList.find(a => a.departmentId == id);
    //this.previewImgURL = this.selectedDepartment.iconPath;
    //this.previewImgURL1 = this.selectedDepartment.imagePath;
    //this.IconPath = "";
    //this.ImagePath = "";
    //this.EditMode = true;
    //document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateDepartment() {
    if (this.IconPath !== "") {
      this.selectedDepartment.iconPath = "";
      this.selectedDepartment.iconPath = this.IconPath;
      this.selectedDepartment.isIconPathUploaded = true;
    }
    else {
      this.selectedDepartment.iconPath = this.selectedDepartment.iconPath;
    }
    if (this.ImagePath !== "") {
      this.selectedDepartment.imagePath = "";
      this.selectedDepartment.imagePath = this.ImagePath;
      this.selectedDepartment.isImagePathUploaded = true;
    }
    else {
      this.selectedDepartment.imagePath = this.selectedDepartment.imagePath;
    }
    this.cmsService.UpdateDepartment(this.selectedDepartment).subscribe(res => {
      // this.showPopupModel=false;
      this.GetDepartment();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteDepartment(id) {
    if (confirm("Do you Want to delete this Department?")) {
      this.cmsService.DeleteDepartment(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetDepartment();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedDepartment = new DanpheCareDepartment();
    this.previewImgURL = null;
    this.EditMode = false;
    // this.showPopupModel = true;   
  }
}
