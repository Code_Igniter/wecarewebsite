
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { DanpheCareDepartment, DepartmentSubHeading } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';


@Component({
  templateUrl: './department-sub-heading.component.html'
})


export class DepartmentSubHeadingComponent implements OnInit {

  public selectedDepartment: DepartmentSubHeading = new DepartmentSubHeading();
  public departmentList: Array<DepartmentSubHeading> = new Array<DepartmentSubHeading>();
  progress: number;
  message: string;
  file: any;
  previewImgURL: any;
  EditMode: boolean;
  IconPath: string;
  p: number = 1;
  public searchText: any;
  public filteredDepartmentList: Array<any> = new Array<any>();
  public selDepartment: any;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService) {
    ////
  }
  ngOnInit() {
    this.GetDepartment();
    this.GetDepartmentList();

  }
  AddCMSDepartment(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.selectedDepartment.iconPath = this.IconPath;
      this.selectedDepartment.departmentId = this.selDepartment.departmentId;
      this.cmsService.AddDepartmentSubHeading(this.selectedDepartment).subscribe(res => {
        // this.showPopupModel=false;
        this.GetDepartment();
        this.notifyService.showSuccess('Success', 'Department SubHeading Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateDepartment();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }
  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    let mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.selectedDepartment.isIconPathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }

  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {
      let mimeType = this.file.type;
      if (mimeType.match(/image\/*/) === null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      let reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
    } else {
      let reader = new FileReader();
    }
  }

  GetDepartment() {
    this.cmsService.GetDepartmentSubHeading().subscribe(res => {
      if (res) {
        this.departmentList = [];
        this.departmentList = Object.assign(this.departmentList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Department SubHeading Found!");
      });
  }

  GetDepartmentList() {
    this.cmsService.GetDepartment()
      .subscribe(res => this.SuccessDepartment(res),
        res => this.Error(res));
  }
  SuccessDepartment(res) {
    this.filteredDepartmentList = res;

  }

  EditDepartment(id) {
    this.selectedDepartment = this.departmentList.find(a => a.departmentSubHeadingId == id);
    this.selDepartment = this.filteredDepartmentList.find(a => a.departmentId == this.selectedDepartment.departmentId);
    this.previewImgURL = this.selectedDepartment.iconPath;
    this.IconPath = "";
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateDepartment() {
    if (this.IconPath !== "") {
      this.selectedDepartment.iconPath = "";
      this.selectedDepartment.iconPath = this.IconPath;
      this.selectedDepartment.isIconPathUploaded = true;
    }
    else {
      this.selectedDepartment.iconPath = this.selectedDepartment.iconPath;
    }
    this.selectedDepartment.departmentId = this.selDepartment.departmentId;
    this.cmsService.UpdateDepartmentSubHeading(this.selectedDepartment).subscribe(res => {
      // this.showPopupModel=false;
      this.GetDepartment();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteDepartment(id) {
    if (confirm("Do you Want to delete this Department?")) {
      this.cmsService.DeleteDepartmentSubHeading(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetDepartment();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedDepartment = new DepartmentSubHeading();
    this.previewImgURL = null;
    this.EditMode = false;
    // this.showPopupModel = true;   
  }
}
