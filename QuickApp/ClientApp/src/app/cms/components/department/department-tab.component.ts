
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';
import { DepartmentTabcontent } from '../../models/danphecare.cms.model';


@Component({
  templateUrl: './department-tab.component.html'
})


export class CMSDepartmentTabContentComponent implements OnInit {

  public selectedCMSDepTabContent: DepartmentTabcontent = new DepartmentTabcontent();
  public tabContentList: Array<DepartmentTabcontent> = new Array<DepartmentTabcontent>();
  EditMode: boolean;
  public filteredDepartmentList: Array<any> = new Array<any>();
  public selDepartment: any;
  public filteredTabList: Array<any> = new Array<any>();
  public selTab: any;
  p: number = 1;
  public searchText: any;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetDepartmentTabContent();
    this.GetDepartment();
    this.GetTab();

  }

  GetDepartment() {
    this.cmsService.GetDepartment()
      .subscribe(res => this.SuccessDepartment(res),
        res => this.Error(res));
  }
  SuccessDepartment(res) {
    this.filteredDepartmentList = res;
  }
  GetTab() {
    this.cmsService.GetDepTab()
      .subscribe(res => this.SuccessTab(res),
        res => this.Error(res));
  }
  SuccessTab(res) {
    this.filteredTabList = res;
  }

  AddTabContent() {
    //if (form.invalid) {
    //  this.notifyService.showError("", "Please fill up the required field");
    //  return;
    //}

    if (!this.EditMode) {
      this.selectedCMSDepTabContent.departmentId = this.selDepartment.DepartmentId;
      this.selectedCMSDepTabContent.departmentTabId = this.selTab.DepartmentTabId;
      this.cmsService.AddDepTabContent(this.selectedCMSDepTabContent).subscribe(res => {
        this.GetDepartmentTabContent();
        this.notifyService.showSuccess('Success', 'Tab Content Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateDepartmentTabContent();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }


  GetDepartmentTabContent() {
    this.cmsService.GetDepTabContent().subscribe(res => {
      if (res) {
        this.tabContentList = [];
        this.tabContentList = Object.assign(this.tabContentList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Tab Content Found!");
      });
  }

  EditTabContent(id) {
    this.selectedCMSDepTabContent = this.tabContentList.find(a => a.departmentContentId === id);
    this.selTab = this.filteredTabList.find(a => a.departmentTabId === this.selectedCMSDepTabContent.departmentTabId);
    this.selDepartment = this.filteredDepartmentList.find(a => a.departmentId === this.selectedCMSDepTabContent.departmentId);
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
  }

  UpdateDepartmentTabContent() {
    this.selectedCMSDepTabContent.departmentId = this.selDepartment.departmentId;
    this.selectedCMSDepTabContent.departmentTabId = this.selTab.departmentTabId;
    this.cmsService.UpdateDepTabContent(this.selectedCMSDepTabContent).subscribe(res => {
      this.GetDepartmentTabContent();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteTabContent(id) {
    if (confirm("Do you Want to delete this Tab Content?")) {
      this.cmsService.DeleteDepTabContent(id).subscribe(res => {
        this.GetDepartmentTabContent();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {

    //this.selectedCMSDepTabContent = new DepartmentTabContent();
    //this.EditMode = false;
    this.routing.navigate(['cms/adddeptabcontent']);
  }
}
