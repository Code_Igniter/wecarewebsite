
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { DepartmentConsultation } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cms-dep-consultation-add.component.html'
})


export class CMSDepartmentAddConsultationComponent implements OnInit {

  public selectedCMSConsultation: DepartmentConsultation = new DepartmentConsultation();
  public consultationList: Array<DepartmentConsultation> = new Array<DepartmentConsultation>();
  progress: number;
  message: string;
  file: any;
  previewImgURL: any;
  EditMode: boolean;
  public filteredDepartmentList: Array<any> = new Array<any>();
  public selDepartment: any;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetConsultation();
    this.GetDepartment();

  }

  GetDepartment() {
    this.cmsService.GetDepartment()
      .subscribe(res => this.SuccessDepartment(res),
        res => this.Error(res));
  }
  SuccessDepartment(res) {
    this.filteredDepartmentList = res;
  }

  AddConsultation() {
    //if (form.invalid) {
    //  this.notifyService.showError("", "Please fill up the required field");
    //  return;
    //}

    this.selectedCMSConsultation.departmentId = this.selDepartment.departmentId;
    this.cmsService.AddDepConsultation(this.selectedCMSConsultation).subscribe(res => {
      this.routing.navigate(['cms/cmsconsultation']);
        this.GetConsultation();
        this.notifyService.showSuccess('Success', 'Consultation Added Successfully!');
        //document.getElementById("close1").click();
      },
        res => this.Error(res));
   

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    let mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.selectedCMSConsultation.isIconPathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {
      let mimeType = this.file.type;
      if (mimeType.match(/image\/*/) === null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      let reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
    } else {
      let reader = new FileReader();
    }
  }

  GetConsultation() {
    this.cmsService.GetDepConsultation().subscribe(res => {
      if (res) {
        this.consultationList = [];
        this.consultationList = Object.assign(this.consultationList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Doctor Found!");
      });
  }

  EditConsultation(id) {
    this.selectedCMSConsultation = this.consultationList.find(a => a.departmentConsultationId === id);
    this.previewImgURL = this.selectedCMSConsultation.iconPath;
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
  }

  UpdateConsultation() {
    this.selectedCMSConsultation.departmentId = this.selDepartment.departmentId;
    this.cmsService.UpdateDepConsultation(this.selectedCMSConsultation).subscribe(res => {
      this.GetConsultation();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteConsultation(id) {
    if (confirm("Do you Want to delete this Consultation?")) {
      this.cmsService.DeleteDepConsultation(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetConsultation();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSConsultation = new DepartmentConsultation();
    this.previewImgURL = null;
    this.EditMode = false;
  }
  Back() {
    this.EditMode = false;
    this.routing.navigate(['cms/cmsconsultation']);
  }
}
