
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { News } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cms-news.component.html',
  styleUrls:["./cms-news.component.css"]
})


export class CMSNewsComponent implements OnInit {

  public selectedCMSNews: News = new News();
  public newsList: Array<News> = new Array<News>();
  progress: number;
  message: string;
  file: any;
  previewImgURL: any;
  EditMode: boolean;
  ImagePath: string;
  p: number = 1;
  public searchText: any;
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetNews();

  }
  AddCMSNews(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.cmsService.AddNews(this.selectedCMSNews).subscribe(res => {
        // this.showPopupModel=false;
        this.GetNews();
        this.notifyService.showSuccess('Success', 'News Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateNews();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  upload(files) {

    if (files.length === 0)
      return;

    this.file = files[0];
    let mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.selectedCMSNews.isImagePathUploaded = true;
        this.progress = Math.round(100 * event.loaded / event.total);
        this.ShowPreviewImage();
      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  ShowPreviewImage() {
    this.previewImgURL = null;

    if (this.file) {
      let mimeType = this.file.type;
      if (mimeType.match(/image\/*/) === null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      let reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (_event) => {
        this.previewImgURL = reader.result;
      }
    } else {
      let reader = new FileReader();
    }
  }

  GetNews() {
    this.cmsService.GetNews().subscribe(res => {
      if (res) {
        this.newsList = [];
        this.newsList = Object.assign(this.newsList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No News Found!");
      });
  }

  EditNews(id) {
    this.selectedCMSNews = this.newsList.find(a => a.newsId == id);
    this.previewImgURL = this.selectedCMSNews.imagePath;
    this.ImagePath = "";
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdateNews() {
    if (this.ImagePath !== "") {
      this.selectedCMSNews.imagePath = "";
      this.selectedCMSNews.imagePath = this.ImagePath;
      this.selectedCMSNews.isImagePathUploaded = true;
    }
    else {
      this.selectedCMSNews.imagePath = this.selectedCMSNews.imagePath;
    }
    //this.selectedCMSNews.imagePath = this.ImagePath;
    this.cmsService.UpdateNews(this.selectedCMSNews).subscribe(res => {
      // this.showPopupModel=false;
      this.GetNews();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteNews(id) {
    if (confirm("Do you Want to delete this News?")) {
      this.cmsService.DeleteNews(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetNews();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSNews = new News();
    this.previewImgURL = null;
    this.EditMode = false;
    this.routing.navigate(['cms/addnews']);
    // this.showPopupModel = true;   
  }
}
