
import { Component, OnInit } from '@angular/core';
import { Package } from '../../models/danphecare.cms.model';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';


@Component({
  templateUrl: './package.component.html'
})


export class CMSPackageComponent implements OnInit {

  public selectedPackage: Package = new Package();
  public packageList: Array<Package> = new Array<Package>();
  EditMode: boolean;
  p: number = 1;
  public searchText: any;
  public filteredSortingList: Array<any> = new Array<any>();
  public filteredList: Array<any> = new Array<any>();
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService) {
    ////
  }
  ngOnInit() {
    this.GetPackages();
  }

  AddPackage(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {

      this.filteredList = new Array<any>();
      this.cmsService.AddPackage(this.selectedPackage).subscribe(res => {
        // this.showPopupModel=false;
        this.GetPackages();
        this.notifyService.showSuccess('Success', 'Packages Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdatePackage();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

 
  GetPackages() {
    this.cmsService.GetPackage().subscribe(res => {
      if (res) {
        this.packageList = [];
        this.packageList = Object.assign(this.packageList, res);
      }
    },
      res => {
        console.log("Error Message: " + res);
        // this.notifyService.showError("Info", "No Services Found!");
      });
  }

  EditPackage(id) {
    this.selectedPackage = this.packageList.find(a => a.packageNameModelId === id);

    this.EditMode = true;
    this.filteredList = new Array<any>();
    document.getElementById("editButtonClicked").click();
    // this.showPopupModel = true;
  }

  UpdatePackage() {


    this.cmsService.UpdatePackage(this.selectedPackage).subscribe(res => {
      // this.showPopupModel=false;
      this.GetPackages();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeletePackage(id) {
    if (confirm("Do you Want to delete this package?")) {
      this.cmsService.DeletePackage(id).subscribe(res => {
        this.GetPackages();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedPackage = new Package();
    this.EditMode = false;  
  }
}
