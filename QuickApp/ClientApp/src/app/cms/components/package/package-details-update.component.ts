
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { PackageDetails } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  templateUrl: './package-details-update.component.html'
})


export class CMSPackageDetailsEditComponent implements OnInit {

  public selectedPackageDetails: PackageDetails = new PackageDetails();
  public packageDetailsList: Array<PackageDetails> = new Array<PackageDetails>();
  
  EditMode: boolean;
  
  public filteredPackageList: Array<any> = new Array<any>();
  public selPackage: any;
  p: number = 1;
  public searchText: any;
  
  public packageDetails: string;
  public packageDetailId: number;
  constructor(public http: HttpClient, public cmsService: CMSService, private route: ActivatedRoute, public notifyService: NotificationService, public routing: Router) {
    this.packageDetails = this.route.snapshot.queryParamMap.get('id');
    this.packageDetailId = parseInt(this.packageDetails, 10);
  }
  ngOnInit() {
    this.packageDetailId = parseInt(this.packageDetails, 10);
    this.GetPackage();
    this.GetPackageDetails();
  }
 

  GetPackage() {
    this.cmsService.GetPackage()
      .subscribe(res => this.SuccessPackage(res),
        res => this.Error(res));
  }
  SuccessPackage(res) {
    this.filteredPackageList = res;

  }

  AddPackageDetails(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.selectedPackageDetails.PackageNameModelId = this.selPackage.packageModelId;

      this.cmsService.AddPackageDetails(this.selectedPackageDetails).subscribe(res => {
        this.GetPackageDetails();
        
        this.notifyService.showSuccess('Success', 'Package Details Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdatePackageDetails();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

  
  GetPackageDetails() {
    this.cmsService.GetPackageDetails().subscribe(res => {
      if (res) {
        this.packageDetailsList = [];
        this.packageDetailsList = Object.assign(this.packageDetailsList, res);
        this.EditPackageDetails(this.packageDetailId);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Package details Found!");
      });
  }

  EditPackageDetails(id) {

    this.selectedPackageDetails = this.packageDetailsList.find(a => a.packageDetailId === id);
    this.selPackage = this.filteredPackageList.find(a => a.packageModelId == this.selectedPackageDetails.PackageNameModelId);
    this.EditMode = true;
    //document.getElementById("editButtonClicked").click();
  }

  UpdatePackageDetails() {

    this.selectedPackageDetails.PackageNameModelId = this.selPackage.packageModelId;

    this.cmsService.UpdatePackageDetails(this.selectedPackageDetails).subscribe(res => {
      this.GetPackageDetails();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      this.routing.navigate(['cms/cmspackagedetails']);
      //document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeletePackageDetails(id) {
    if (confirm("Do you Want to delete this Package details?")) {
      this.cmsService.DeletePackageDetails(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetPackageDetails();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedPackageDetails = new PackageDetails();
    this.EditMode = false;
  }
  Back() {
    this.EditMode = false;
    this.routing.navigate(['cms/cmspackagedetails']);
  }
}
