
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { PackageDetails } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './package-details.component.html'
})


export class CMSPackageDetailsComponent implements OnInit {

  public selectedPackageDetails: PackageDetails = new PackageDetails();
  public packageList: Array<PackageDetails> = new Array<PackageDetails>();

  EditMode: boolean;

  public filteredPackageNameList: Array<any> = new Array<any>();
  public selPackageName: any;
  p: number = 1;
  public searchText: any;


  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetPackageDetails();
    this.GetPackageName();
  }


  GetPackageName() {
    this.cmsService.GetPackage()
      .subscribe(res => this.SuccessPackage(res),
        res => this.Error(res));
  }
  SuccessPackage(res) {
    this.filteredPackageNameList = res;

  }

  AddCMSPackageDetails(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.selectedPackageDetails.PackageNameModelId = this.selPackageName.packageNameModelId;
      this.cmsService.AddPackageDetails(this.selectedPackageDetails).subscribe(res => {
        this.GetPackageDetails();

        this.notifyService.showSuccess('Success', 'Package details Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdatePackageDetails();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }

 

  GetPackageDetails() {
    this.cmsService.GetPackageDetails().subscribe(res => {
      if (res) {
        this.packageList = [];
        this.packageList = Object.assign(this.packageList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Package Details Found!");
      });
  }

  EditPackageDetails(id) {
    this.selectedPackageDetails = new PackageDetails();
    this.EditMode = false;
    this.routing.navigate(['cms/cmspackagedetailsedit'], { queryParams: { id: id } });

  }

  UpdatePackageDetails() {

    this.selectedPackageDetails.PackageNameModelId = this.selPackageName.packageModelId;

    this.cmsService.UpdatePackageDetails(this.selectedPackageDetails).subscribe(res => {
      this.GetPackageDetails();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeletePackageDetails(id) {
    if (confirm("Do you Want to delete this Package details?")) {
      this.cmsService.DeletePackageDetails(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetPackageDetails();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedPackageDetails = new PackageDetails();
    this.EditMode = false;
  }
}
