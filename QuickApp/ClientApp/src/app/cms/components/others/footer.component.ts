import { Component } from '@angular/core';
import { AboutUsModel } from '../../models/aboutUs.model';
import { DanpheCareContact } from '../../models/danphecare.cms.model';
import { WebsiteService } from '../../../website/websiteservice/website.service';
import { NotificationService } from '../../../services/notification.service';


@Component({
  selector: 'app-cms-footer',
  templateUrl: './footer.component.html'
})
export class FooterCMSComponent {

  public aboutUs: AboutUsModel = new AboutUsModel();
  public contact: DanpheCareContact = new DanpheCareContact();
  constructor(public websiteService: WebsiteService, private notifyService: NotificationService) {
    this.GetAboutUs();
    this.GetContact();
  }
  ngOnInit() {
    this.GetAboutUs();
    this.GetContact();
  }
  GetAboutUs() {
    this.websiteService.GetAboutUs().subscribe(res => {
      if (res && res.length > 0) {
        this.aboutUs = Object.assign(this.aboutUs, res[0]);

      }
    },
      res => {
        this.notifyService.showError("Error", "Internal Error")
      });
  }
  GetContact() {
    this.websiteService.GetContact().subscribe(res => {
      if (res && res.length > 0) {
        this.contact = Object.assign(this.contact, res[0]);

      }
    },
      res => {
        this.notifyService.showError("Error", "Internal Error")
      });
  }

}

