import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { OurServiceModel } from '../../models/OurServices.model';
import { DanpheCareDepartment } from '../../models/danphecare.cms.model';
import { WebsiteService } from '../../../website/websiteservice/website.service';
import { NotificationService } from '../../../services/notification.service';


@Component({
  selector: 'app-cms-header',
  templateUrl: './header.component.html'
})
export class HeaderCMSComponent {
  icon = require("../../../../assets/images/danphecarelogo.png");
  isUserLoggedIn: boolean;
  public servicesList: Array<OurServiceModel> = new Array<OurServiceModel>();
  public servicesId: number;
  public departmentList: Array<DanpheCareDepartment> = new Array<DanpheCareDepartment>();
  public departmentId: number;
  constructor(public routing: Router, private authService: AuthService, public websiteService: WebsiteService, public notifyService: NotificationService) {
//
    this.GetServices();
    this.GetDepartment();
    this.loadmenu(); 
    this.isUserLoggedIn = this.authService.isLoggedIn;
  }
  loadmenu() {
    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown');
    //Checks if li has sub (ul) and adds class for toggle icon - just an UI


    $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)

    $(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\"></a>");

    //Adds menu-mobile class (for mobile toggle menu) before the normal menu
    //Mobile menu is hidden if width is more then 1199px, but normal menu is displayed
    //Normal menu is hidden if width is below 1199px, and $ adds mobile menu
    //Done this way so it can be used with wordpress without any trouble

    // $(".menu > ul > li").hover(function (e) {
    //     if ($(window).width() > 1170) {
    //         $(this).children("ul").stop(true, false).fadeToggle(300);
    //         e.preventDefault();
    //     }
    // });
    //If width is more than 1170px dropdowns are displayed on hover

    $(".menu > ul > li").on('click', function () {
      if ($(window).width() <= 1170) {
        $(this).children("ul").fadeToggle(300);
      }
    });
    //If width is less or equal to 1170px dropdowns are displayed on click

    $(".menu-mobile").on('click', function (e) {
      $(".menu > ul").toggleClass('show-on-mobile');
      e.preventDefault();
    });
    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story
  }

  GotoTelemedicine() {
    this.routing.navigate(['/login']);
  }
  GetServices() {
    this.websiteService.GetOurServices().subscribe(res => {
      if (res) {

        //this.servicesList = [];
        this.servicesList = Object.assign(this.servicesList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Services Found!");
      });
  }
  GoToServiceDetails(id) {
    this.servicesId = id;
    this.routing.navigate(['/servicedetails'], { queryParams: { id: this.servicesId } });


  }
  GetDepartment() {
    this.websiteService.GetDepartment().subscribe(res => {
      if (res) {
        this.departmentList = [];
        this.departmentList = Object.assign(this.departmentList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Department Found!");
      });
  }
  DepartmentDetails(id) {
    this.departmentId = id;
    this.routing.navigate(['/department'], { queryParams: { id: this.departmentId } });
  }
  logout() {
    this.authService.logout();
    this.authService.redirectLogoutUser();
  }
}
