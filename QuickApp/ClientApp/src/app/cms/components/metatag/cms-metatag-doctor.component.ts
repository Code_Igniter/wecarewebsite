
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { Blogs, DanpheCareDepartment, DanpheCareDoctor, MetaTag } from '../../models/danphecare.cms.model';
import { Router } from '@angular/router';
import { OurServiceModel } from '../../models/OurServices.model';


@Component({
  templateUrl: './cms-metatag-doctor.component.html'
})


export class CMSMetaTagDoctorComponent implements OnInit {

  public metatag: MetaTag = new MetaTag();
  public selectedCMSMetaTagContent: MetaTag = new MetaTag();
  public metaTagList: Array<MetaTag> = new Array<MetaTag>();
  public ourServicesList: Array<OurServiceModel> = new Array<OurServiceModel>();
  public departmentList: Array<DanpheCareDepartment> = new Array<DanpheCareDepartment>();
  public blogsList: Array<Blogs> = new Array<Blogs>();
  public doctorList: Array<DanpheCareDoctor> = new Array<DanpheCareDoctor>();
  public selMetaTag: any;

  p = 1;
  public searchText: string;

  EditMode: boolean;


  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {

    this.GetMetaTag();
    this.GetDoctor();

  }
  AddMetaTag(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }
    if (!this.EditMode) {
      this.selectedCMSMetaTagContent.page = "Doctor Details Page";
      this.selectedCMSMetaTagContent.doctorId = this.selMetaTag.doctorId;

      this.cmsService.AddMetaTag(this.selectedCMSMetaTagContent).subscribe(res => {
        this.GetMetaTag();
        this.notifyService.showSuccess('Success', 'Meta tag Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateMetaTag();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }


  GetMetaTag() {
    this.cmsService.GetDoctorMetaTag().subscribe(res => {
      if (res) {
        this.metaTagList = [];
        this.metaTagList = Object.assign(this.metaTagList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Meta tag Found!");
      });
  }

  EditMetaTag(id) {
    this.selectedCMSMetaTagContent = this.metaTagList.find(a => a.metaTagId === id);
    this.selMetaTag = this.doctorList.find(a => a.doctorId === this.selectedCMSMetaTagContent.doctorId);
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
  }

  UpdateMetaTag() {
    this.selectedCMSMetaTagContent.page = "Doctor Details Page";
    this.selectedCMSMetaTagContent.doctorId = this.selMetaTag.doctorId;
    this.cmsService.UpdateMetaTag(this.selectedCMSMetaTagContent).subscribe(res => {
      this.GetMetaTag();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteMetaTag(id) {
    if (confirm("Do you Want to delete this Meta Tag?")) {
      this.cmsService.DeleteMetaTag(id).subscribe(res => {
        this.GetMetaTag();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSMetaTagContent = new MetaTag();
    this.EditMode = false;
  }



  GetDoctor() {
    this.cmsService.GetDoctor().subscribe(res => {
      if (res) {
        this.doctorList = [];
        this.doctorList = Object.assign(this.doctorList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Doctor Found!");
      });
  }
}
