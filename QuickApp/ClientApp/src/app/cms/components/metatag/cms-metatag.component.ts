
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { Blogs, DanpheCareDepartment, DanpheCareDoctor, MetaTag } from '../../models/danphecare.cms.model';
import { Router } from '@angular/router';
import { OurServiceModel } from '../../models/OurServices.model';


@Component({
  templateUrl: './cms-metatag.component.html'
})


export class CMSMetaTagComponent implements OnInit {

  public metatag: MetaTag = new MetaTag();
  public selectedCMSMetaTagContent: MetaTag = new MetaTag();
  public metaTagList: Array<MetaTag> = new Array<MetaTag>();
  public ourServicesList: Array<OurServiceModel> = new Array<OurServiceModel>();
  public departmentList: Array<DanpheCareDepartment> = new Array<DanpheCareDepartment>();
  public blogsList: Array<Blogs> = new Array<Blogs>();
  public doctorList: Array<DanpheCareDoctor> = new Array<DanpheCareDoctor>();
  public selMetaTag: any;
  public selDoctor: any;
  public selDepartment: any;
  public selService: any;
  public selBlog: any;
  filteredPage = [
    { id: 1, title: "Home Page" },
    { id: 2, title: "About Page" },
    { id: 3, title: "Services List Page" },
    { id: 4, title: "Department List Page" },
    { id: 5, title: "Specialist Page" },
    //{ id: 6, title: "Specialist Profile Page" },
    //{ id: 7, title: "Department Details Page" },
    //{ id: 8, title: "Services Details Page" },
    { id: 6, title: "Blog List Page" },
 /*   { id: 10, title: "Blog Details Page" },*/
    { id: 7, title: "Expat Page" },
  ];
  p: number = 1;
  public searchText: any;

  EditMode: boolean;


  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {

    this.GetMetaTag();

  }
  AddMetaTag(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }
    if (!this.EditMode) {
      this.selectedCMSMetaTagContent.page = this.selMetaTag.title;
      //this.selectedCMSMetaTagContent.blogId = this.selBlog.blogId;
      //this.selectedCMSMetaTagContent.serviceId = this.selService.serviceId;
      //this.selectedCMSMetaTagContent.doctorId = this.selDoctor.doctorId;
      //this.selectedCMSMetaTagContent.departmentId = this.selDepartment.departmentId;
      this.cmsService.AddMetaTag(this.selectedCMSMetaTagContent).subscribe(res => {
        this.GetMetaTag();
        this.notifyService.showSuccess('Success', 'Meta tag Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateMetaTag();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }


  GetMetaTag() {
    this.cmsService.GetMetaTag().subscribe(res => {
      if (res) {
        this.metaTagList = [];
        this.metaTagList = Object.assign(this.metaTagList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Meta tag Found!");
      });
  }

  EditMetaTag(id) {
    this.selectedCMSMetaTagContent = this.metaTagList.find(a => a.metaTagId === id);
    this.selMetaTag = this.filteredPage.find(a => a.title === this.selectedCMSMetaTagContent.page);
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
  }

  UpdateMetaTag() {
    debugger;
    this.selectedCMSMetaTagContent.page = this.selMetaTag.title;
    this.cmsService.UpdateMetaTag(this.selectedCMSMetaTagContent).subscribe(res => {
      this.GetMetaTag();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteMetaTag(id) {
    if (confirm("Do you Want to delete this Meta Tag?")) {
      this.cmsService.DeleteMetaTag(id).subscribe(res => {
        this.GetMetaTag();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSMetaTagContent = new MetaTag();
    this.EditMode = false;
  }

  GetOurServices() {
    this.cmsService.GetOurServices().subscribe(res => {
      if (res) {
        this.ourServicesList = [];
        this.ourServicesList = Object.assign(this.ourServicesList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Services Found!");
      });
  }
  GetDepartment() {
    this.cmsService.GetDepartment().subscribe(res => {
      if (res) {
        this.departmentList = [];
        this.departmentList = Object.assign(this.departmentList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Department Found!");
      });
  }

  GetBlogs() {
    this.cmsService.GetBlogs().subscribe(res => {
      if (res) {
        this.blogsList = [];

        this.blogsList = Object.assign(this.blogsList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Blogs Found!");
      });
  }

  GetDoctor() {
    this.cmsService.GetDoctor().subscribe(res => {
      if (res) {
        this.doctorList = [];
        this.doctorList = Object.assign(this.doctorList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Doctor Found!");
      });
  }
}
