
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CMSService } from '../../services/cms.service';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { Blogs, DanpheCareDepartment, DanpheCareDoctor, MetaTag } from '../../models/danphecare.cms.model';
import { Router } from '@angular/router';
import { OurServiceModel } from '../../models/OurServices.model';



@Component({
  templateUrl: './cms-metatag-blog.component.html'
})


export class CMSMetaTagBlogComponent implements OnInit {

  public metatag: MetaTag = new MetaTag();
  public selectedCMSMetaTagContent: MetaTag = new MetaTag();
  public metaTagList: Array<MetaTag> = new Array<MetaTag>();
  public ourServicesList: Array<OurServiceModel> = new Array<OurServiceModel>();
  public departmentList: Array<DanpheCareDepartment> = new Array<DanpheCareDepartment>();
  public blogsList: Array<Blogs> = new Array<Blogs>();
  public doctorList: Array<DanpheCareDoctor> = new Array<DanpheCareDoctor>();
  public selMetaTag: any;
  p= 1;
  public searchText: any;

  EditMode: boolean;


  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {

    this.GetMetaTag();
    this.GetBlogs();

  }
  AddMetaTag(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }
    if (!this.EditMode) {
      this.selectedCMSMetaTagContent.page = "Blog Details Page";
      this.selectedCMSMetaTagContent.blogId = this.selMetaTag.blogId;
      this.cmsService.AddMetaTag(this.selectedCMSMetaTagContent).subscribe(res => {
        this.GetMetaTag();
        this.notifyService.showSuccess('Success', 'Meta tag Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateMetaTag();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }


  GetMetaTag() {
    this.cmsService.GetBlogMetaTag().subscribe(res => {
      if (res) {
        this.metaTagList = [];
        this.metaTagList = Object.assign(this.metaTagList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Meta tag Found!");
      });
  }

  EditMetaTag(id) {
    this.selectedCMSMetaTagContent = this.metaTagList.find(a => a.metaTagId === id);
    this.selMetaTag = this.blogsList.find(a => a.blogId === this.selectedCMSMetaTagContent.blogId);
    this.EditMode = true;
    document.getElementById("editButtonClicked").click();
  }

  UpdateMetaTag() {
    this.selectedCMSMetaTagContent.page = "Blog Details Page";
    this.selectedCMSMetaTagContent.blogId = this.selMetaTag.blogId;
    this.cmsService.UpdateMetaTag(this.selectedCMSMetaTagContent).subscribe(res => {
      this.GetMetaTag();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteMetaTag(id) {
    if (confirm("Do you Want to delete this Meta Tag?")) {
      this.cmsService.DeleteMetaTag(id).subscribe(res => {
        this.GetMetaTag();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSMetaTagContent = new MetaTag();
    this.EditMode = false;
  }


  GetBlogs() {
    this.cmsService.GetBlogs().subscribe(res => {
      if (res) {
        this.blogsList = [];

        this.blogsList = Object.assign(this.blogsList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Blogs Found!");
      });
  }
}
