
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { SubServices } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  templateUrl: './cms-sub-services-edit.component.html'
})


export class CMSSubServicesEditComponent implements OnInit {

  public selectedCMSSubServices: SubServices = new SubServices();
  public subServicesList: Array<SubServices> = new Array<SubServices>();
  progress: number;
  progress1: number;
  progress2: number;
  message: string;
  file: any;
  previewImgURL1: any;
  previewImgURL2: any;
  EditMode: boolean;
  ImagePath: string;
  CoverPhoto: string;
  public filteredServicesList: Array<any> = new Array<any>();
  public selServices: any;
  p: number = 1;
  public searchText: any;
  
  public subservId: string;
  public subServicesId: number;
  constructor(public http: HttpClient, public cmsService: CMSService, private route: ActivatedRoute, public notifyService: NotificationService, public routing: Router) {
    this.subservId = this.route.snapshot.queryParamMap.get('id');
    this.subServicesId = parseInt(this.subservId, 10);
  }
  ngOnInit() {
    this.subServicesId = parseInt(this.subservId, 10);
    this.GetSubServices();
    this.GetServices();
   

  }
  

  GetServices() {
    this.cmsService.GetOurServices()
      .subscribe(res => this.SuccessServices(res),
        res => this.Error(res));
  }
  SuccessServices(res) {
    this.filteredServicesList = res;

  }


  AddCMSSubServices(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.selectedCMSSubServices.ourServicesId = this.selServices.serviceId;
      this.selectedCMSSubServices.imagePath = this.ImagePath;
      this.cmsService.AddSubServices(this.selectedCMSSubServices).subscribe(res => {
        this.GetSubServices();

        this.notifyService.showSuccess('Success', 'Sub Services Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateSubServices();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }


  upload(files, id: number) {

    if (files.length === 0)
      return;

    let file = files[0];
    let mimeType = file.type;
    if (mimeType.match(/image\/*/) === null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files) {
      formData.append(file.name, file);

      if (id == 0) { //  Image Upload
        this.selectedCMSSubServices.imagePath = file.name;

      }

    }


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {

        if (id == 0) { // Icon Image Upload
          this.progress = Math.round(100 * event.loaded / event.total);
          this.selectedCMSSubServices.isImagePathUploaded = true;

        }


      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();

    });
    this.ShowPreviewImage(file, id);

  }

  ShowPreviewImage(file, id: number) {

    if (file) {
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        if (id == 0) { // Icon Image Preview
          this.previewImgURL1 = reader.result;
        }

      }
    } else {
      var reader = new FileReader();
    }
  }

  GetSubServices() {
    this.cmsService.GetSubServices().subscribe(res => {
      if (res) {
        this.subServicesList = [];
        this.subServicesList = Object.assign(this.subServicesList, res);
        this.EditSubServices(this.subServicesId);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Sub Services Found!");
      });
  }

  EditSubServices(id) {

    this.selectedCMSSubServices = this.subServicesList.find(a => a.subServicesId == id);
    this.selServices = this.filteredServicesList.find(a => a.servicesId == this.selectedCMSSubServices.ourServicesId);

    this.previewImgURL1 = null;
    this.previewImgURL2 = null;
    this.previewImgURL1 = this.selectedCMSSubServices.imagePath;
    this.ImagePath = "";
    this.EditMode = true;
    //document.getElementById("editButtonClicked").click();
  }

  UpdateSubServices() {
    if (this.ImagePath !== "") {
      this.selectedCMSSubServices.imagePath = "";
      this.selectedCMSSubServices.imagePath = this.ImagePath;
      this.selectedCMSSubServices.isImagePathUploaded = true;
    }
    else {
      this.selectedCMSSubServices.imagePath = this.selectedCMSSubServices.imagePath;
    }

   /* this.selectedCMSSubServices.ourServicesId = this.selServices.servicesId;*/
    

    this.cmsService.UpdateSubServices(this.selectedCMSSubServices).subscribe(res => {
      this.GetSubServices();
      
      this.notifyService.showSuccess('', 'Successfully Updated!');
      this.routing.navigate(['cms/cmssubservices']);
      //document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteSubServices(id) {
    if (confirm("Do you Want to delete this Sub Services?")) {
      this.cmsService.DeleteSubServices(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetSubServices();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSSubServices = new SubServices();
    this.previewImgURL1 = null;
    this.EditMode = false;
  }
  Back() {
    this.EditMode = false;
    this.routing.navigate(['cms/cmssubservices']);
  }
}
