
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import { DanpheCareDoctor, SubServicesDetails } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  templateUrl: './cms-sub-services-details-edit.component.html'
})


export class CMSSubServicesDetailsEditComponent implements OnInit {

  public selectedCMSSubServicesDetails: SubServicesDetails = new SubServicesDetails();
  public servicesDetailsList: Array<SubServicesDetails> = new Array<SubServicesDetails>();
  progress: number;
  progress1: number;
  progress2: number;
  message: string;
  file: any;
  previewImgURL1: any;
  previewImgURL2: any;
  EditMode: boolean;
  ImagePath: string;
  CoverPhoto: string;
  public filteredSubServicesList: Array<any> = new Array<any>();
  public selSubServices: any;
  p: number = 1;
  public searchText: any;
 
  public detailId: string;
  public subServicesDetailsId: number;
  constructor(public http: HttpClient, public cmsService: CMSService, private route: ActivatedRoute, public notifyService: NotificationService, public routing: Router) {
    this.detailId = this.route.snapshot.queryParamMap.get('id');
    this.subServicesDetailsId = parseInt(this.detailId, 10);
  }
  ngOnInit() {
    this.subServicesDetailsId = parseInt(this.detailId, 10);
    this.GetSubServicesDetails();
    this.GetSubServices();

  }
  
  GetSubServices() {
    this.cmsService.GetSubServices()
      .subscribe(res => this.SuccessSubServices(res),
        res => this.Error(res));
  }
  SuccessSubServices(res) {
    this.filteredSubServicesList = res;

  }


  AddCMSSubServicesDetails(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.selectedCMSSubServicesDetails.subServicesId = this.selSubServices.subServicesId;
      this.selectedCMSSubServicesDetails.imagePath = this.ImagePath;
      this.cmsService.AddSubServicesDetails(this.selectedCMSSubServicesDetails).subscribe(res => {
        this.GetSubServicesDetails();
        this.notifyService.showSuccess('Success', 'Sub Services Details Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateSubServicesDetails();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }


  upload(files, id: number) {

    if (files.length === 0)
      return;

    let file = files[0];
    let mimeType = file.type;
    if (mimeType.match(/image\/*/) === null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files) {
      formData.append(file.name, file);

      if (id == 0) { //  Image Upload
        this.selectedCMSSubServicesDetails.imagePath = file.name;

      }

    }


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {

        if (id == 0) { // Icon Image Upload
          this.progress = Math.round(100 * event.loaded / event.total);
          this.selectedCMSSubServicesDetails.isImagePathUploaded = true;

        }


      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();

    });
    this.ShowPreviewImage(file, id);

  }

  ShowPreviewImage(file, id: number) {

    if (file) {
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        if (id == 0) { // Icon Image Preview
          this.previewImgURL1 = reader.result;
        }

      }
    } else {
      var reader = new FileReader();
    }
  }

  GetSubServicesDetails() {
    this.cmsService.GetSubServicesDetails().subscribe(res => {
      if (res) {
        this.servicesDetailsList = [];
        this.servicesDetailsList = Object.assign(this.servicesDetailsList, res);
        this.EditSubServicesDetails(this.subServicesDetailsId);
      }
    },
      res => {
        this.notifyService.showError("Info", "No details Found!");
      });
  }

  EditSubServicesDetails(id) {
    this.selectedCMSSubServicesDetails = this.servicesDetailsList.find(a => a.subServicesDetailsId == id);
    this.selSubServices = this.filteredSubServicesList.find(a => a.subServicesId == this.selectedCMSSubServicesDetails.subServicesId);

    this.previewImgURL1 = null;
    this.previewImgURL2 = null;
    this.previewImgURL1 = this.selectedCMSSubServicesDetails.imagePath;
    this.ImagePath = "";
    this.CoverPhoto = "";
    this.EditMode = true;
  /*  document.getElementById("editButtonClicked").click();*/
  }

  UpdateSubServicesDetails() {
    if (this.ImagePath !== "") {
      this.selectedCMSSubServicesDetails.imagePath = "";
      this.selectedCMSSubServicesDetails.imagePath = this.ImagePath;
      this.selectedCMSSubServicesDetails.isImagePathUploaded = true;
    }
    else {
      this.selectedCMSSubServicesDetails.imagePath = this.selectedCMSSubServicesDetails.imagePath;
    }

    this.selectedCMSSubServicesDetails.subServicesId = this.selSubServices.subServicesId;


    this.cmsService.UpdateSubServicesDetails(this.selectedCMSSubServicesDetails).subscribe(res => {
      this.GetSubServicesDetails();
      this.notifyService.showSuccess('', 'Successfully Updated!');
      /*document.getElementById("close1").click();*/
    },
      res => this.Error(res));
  }


  DeleteSubServicesDetails(id) {
    if (confirm("Do you Want to delete this details?")) {
      this.cmsService.DeleteDoctor(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetSubServicesDetails();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSSubServicesDetails = new SubServicesDetails();
    this.previewImgURL1 = null;
    this.EditMode = false;
  }
  Back() {
    this.EditMode = false;
    this.routing.navigate(['cms/cmssubservicesdetails']);
  }
}
