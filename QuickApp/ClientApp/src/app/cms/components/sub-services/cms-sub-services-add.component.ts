
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpEventType, HttpRequest } from '@angular/common/http';
import { NotificationService } from '../../../services/notification.service';
import {  SubServices } from '../../models/danphecare.cms.model';
import { CMSService } from '../../services/cms.service';
import { Router } from '@angular/router';


@Component({
  templateUrl: './cms-sub-services-add.component.html'
})


export class CMSSubServicesAddComponent implements OnInit {

  public selectedCMSSubServices: SubServices = new SubServices();
  public subServicesList: Array<SubServices> = new Array<SubServices>();
  progress: number;
  progress1: number;
  progress2: number;
  message: string;
  file: any;
  previewImgURL1: any;
  previewImgURL2: any;
  EditMode: boolean;
  ImagePath: string;

  public filteredServicesList: Array<any> = new Array<any>();
  public selServices: any;
  p: number = 1;
  public searchText: any;
  
  constructor(public http: HttpClient, public cmsService: CMSService, public notifyService: NotificationService, public routing: Router) {
    ////
  }
  ngOnInit() {
    this.GetSubServices();
    this.GetServices();


  }
  
  GetServices() {
    this.cmsService.GetOurServices()
      .subscribe(res => this.SuccessServices(res),
        res => this.Error(res));
  }
  SuccessServices(res) {
    this.filteredServicesList = res;

  }

  AddCMSSubServices(form: NgForm) {
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }

    if (!this.EditMode) {
      this.selectedCMSSubServices.ourServicesId = this.selServices.serviceId;
      this.selectedCMSSubServices.imagePath = this.ImagePath;
      this.cmsService.AddSubServices(this.selectedCMSSubServices).subscribe(res => {
        this.GetSubServices();

        this.notifyService.showSuccess('Success', 'Sub Services Added Successfully!');
        document.getElementById("close1").click();
      },
        res => this.Error(res));
    } else {
      this.UpdateSubServices();
    }

  }

  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }


  upload(files, id: number) {

    if (files.length === 0)
      return;

    let file = files[0];
    let mimeType = file.type;
    if (mimeType.match(/image\/*/) === null) {
      this.notifyService.showError("error", ["Only images are supported."]);
      return;
    }

    const formData = new FormData();

    for (let file of files) {
      formData.append(file.name, file);

      if (id == 0) { //  Image Upload
        this.selectedCMSSubServices.imagePath = file.name;

      }

    }


    const uploadReq = new HttpRequest('POST', `api/DanpheCareCMS/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {

        if (id == 0) { // Icon Image Upload
          this.progress = Math.round(100 * event.loaded / event.total);
          this.selectedCMSSubServices.isImagePathUploaded = true;

        }


      }
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();

    });
    this.ShowPreviewImage(file, id);

  }

  ShowPreviewImage(file, id: number) {

    if (file) {
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        this.notifyService.showError("error", ["Only images are supported."]);
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        if (id == 0) { // Icon Image Preview
          this.previewImgURL1 = reader.result;
        }

      }
    } else {
      var reader = new FileReader();
    }
  }


  GetSubServices() {
    this.cmsService.GetSubServices().subscribe(res => {
      if (res) {
        this.subServicesList = [];
        this.subServicesList = Object.assign(this.subServicesList, res);
      }
    },
      res => {
        this.notifyService.showError("Info", "No Sub Services Found!");
      });
  }

  EditSubServices(id) {
    this.selectedCMSSubServices = new SubServices();
    this.previewImgURL1 = null;
    this.EditMode = false;
    this.routing.navigate(['cms/cmssubservicesedit'], { queryParams: { id: id } });

  }

  UpdateSubServices() {
    if (this.ImagePath !== "") {
      this.selectedCMSSubServices.imagePath = "";
      this.selectedCMSSubServices.imagePath = this.ImagePath;
      this.selectedCMSSubServices.isImagePathUploaded = true;
    }
    else {
      this.selectedCMSSubServices.imagePath = this.selectedCMSSubServices.imagePath;
    }

    //this.selectedCMSDoctor.imagePath = this.ImagePath;
    //this.selectedCMSDoctor.coverPhoto = this.CoverPhoto;
    //this.selectedCMSDoctor.isImagePathUploaded = true;
    //this.selectedCMSDoctor.isCoverPhotoUploaded = true;
    this.selectedCMSSubServices.ourServicesId = this.selServices.serviceId;


    this.cmsService.UpdateSubServices(this.selectedCMSSubServices).subscribe(res => {
      this.GetSubServices();

      this.notifyService.showSuccess('', 'Successfully Updated!');
      document.getElementById("close1").click();
    },
      res => this.Error(res));
  }


  DeleteSubServices(id) {
    if (confirm("Do you Want to delete this Doctor?")) {
      this.cmsService.DeleteDoctor(id).subscribe(res => {
        // this.showPopupModel=false;
        this.GetSubServices();
        this.notifyService.showSuccess('', 'Successfully Deleted!');
        document.getElementById("close1").click();

      },
        res => this.Error(res));
    }
  }
  AddBtnClick() {
    this.selectedCMSSubServices = new SubServices();
    this.previewImgURL1 = null;
    this.EditMode = false;
  }
  Back() {
    this.EditMode = false;
    this.routing.navigate(['cms/cmssubservices']);
  }
}
