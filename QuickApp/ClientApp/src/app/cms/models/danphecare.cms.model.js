"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubServicesDetails = exports.SubServices = exports.PackageDetails = exports.Package = exports.DepartmentSubHeading = exports.DepartmentTabcontent = exports.DepartmentTab = exports.Webinar = exports.Expat = exports.DanpheCareContact = exports.Testimonial = exports.TestimonialMain = exports.MediaCoverage = exports.TeamMember = exports.MetaTag = exports.Blogs = exports.News = exports.ResourcefulArticles = exports.DepartmentConsultation = exports.DanpheCareDoctor = exports.DanpheCareDepartment = void 0;
var DanpheCareDepartment = /** @class */ (function () {
    function DanpheCareDepartment() {
    }
    return DanpheCareDepartment;
}());
exports.DanpheCareDepartment = DanpheCareDepartment;
var DanpheCareDoctor = /** @class */ (function () {
    function DanpheCareDoctor() {
    }
    return DanpheCareDoctor;
}());
exports.DanpheCareDoctor = DanpheCareDoctor;
var DepartmentConsultation = /** @class */ (function () {
    function DepartmentConsultation() {
    }
    return DepartmentConsultation;
}());
exports.DepartmentConsultation = DepartmentConsultation;
var ResourcefulArticles = /** @class */ (function () {
    function ResourcefulArticles() {
    }
    return ResourcefulArticles;
}());
exports.ResourcefulArticles = ResourcefulArticles;
var News = /** @class */ (function () {
    function News() {
    }
    return News;
}());
exports.News = News;
var Blogs = /** @class */ (function () {
    function Blogs() {
    }
    return Blogs;
}());
exports.Blogs = Blogs;
var MetaTag = /** @class */ (function () {
    function MetaTag() {
    }
    return MetaTag;
}());
exports.MetaTag = MetaTag;
var TeamMember = /** @class */ (function () {
    function TeamMember() {
    }
    return TeamMember;
}());
exports.TeamMember = TeamMember;
var MediaCoverage = /** @class */ (function () {
    function MediaCoverage() {
    }
    return MediaCoverage;
}());
exports.MediaCoverage = MediaCoverage;
var TestimonialMain = /** @class */ (function () {
    function TestimonialMain() {
    }
    return TestimonialMain;
}());
exports.TestimonialMain = TestimonialMain;
var Testimonial = /** @class */ (function () {
    function Testimonial() {
    }
    return Testimonial;
}());
exports.Testimonial = Testimonial;
var DanpheCareContact = /** @class */ (function () {
    function DanpheCareContact() {
    }
    return DanpheCareContact;
}());
exports.DanpheCareContact = DanpheCareContact;
var Expat = /** @class */ (function () {
    function Expat() {
    }
    return Expat;
}());
exports.Expat = Expat;
var Webinar = /** @class */ (function () {
    function Webinar() {
    }
    return Webinar;
}());
exports.Webinar = Webinar;
var DepartmentTab = /** @class */ (function () {
    function DepartmentTab() {
    }
    return DepartmentTab;
}());
exports.DepartmentTab = DepartmentTab;
var DepartmentTabcontent = /** @class */ (function () {
    function DepartmentTabcontent() {
    }
    return DepartmentTabcontent;
}());
exports.DepartmentTabcontent = DepartmentTabcontent;
var DepartmentSubHeading = /** @class */ (function () {
    function DepartmentSubHeading() {
    }
    return DepartmentSubHeading;
}());
exports.DepartmentSubHeading = DepartmentSubHeading;
var Package = /** @class */ (function () {
    function Package() {
    }
    return Package;
}());
exports.Package = Package;
var PackageDetails = /** @class */ (function () {
    function PackageDetails() {
    }
    return PackageDetails;
}());
exports.PackageDetails = PackageDetails;
var SubServices = /** @class */ (function () {
    function SubServices() {
    }
    return SubServices;
}());
exports.SubServices = SubServices;
var SubServicesDetails = /** @class */ (function () {
    function SubServicesDetails() {
    }
    return SubServicesDetails;
}());
exports.SubServicesDetails = SubServicesDetails;
//# sourceMappingURL=danphecare.cms.model.js.map