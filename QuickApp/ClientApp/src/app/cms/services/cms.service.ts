import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { mergeMap } from 'rxjs/operators';
import { CMSEndpoint } from './cms-endpoint.service';
import { AboutUsModel } from '../models/aboutUs.model';
import { OurServiceModel } from '../models/OurServices.model';
import { Blogs, DanpheCareDepartment, DanpheCareDoctor, DepartmentConsultation, ResourcefulArticles, News, MetaTag, DanpheCareContact, Expat, Webinar, MediaCoverage, TeamMember, Testimonial, TestimonialMain, DepartmentTabcontent, DepartmentTab, DepartmentSubHeading, Package, PackageDetails, SubServices, SubServicesDetails  } from '../models/danphecare.cms.model';
import { OurTeamMemberComponent } from '../components/our-team/cms-our-team.component';

@Injectable()
export class CMSService {
  constructor(private router: Router, private http: HttpClient, private cmsEndPoint: CMSEndpoint) {
  }

  //#region About Us page
  GetAboutUs(){
    return this.cmsEndPoint.GeAboutUsEndpoint<AboutUsModel[]>();
  }
  AddAboutUs(data: AboutUsModel){
    return this.cmsEndPoint.AddAboutUsEndpoint<AboutUsModel[]>(data);
  }  
  UpdateAboutUs(data: AboutUsModel) {
    if (data.aboutUsId) {
      return this.cmsEndPoint.UpdateAboutUsEndpoint(data, data.aboutUsId);
    }
    else {
      return this.cmsEndPoint.UpdateAboutUsEndpoint<AboutUsModel>(data, data.aboutUsId).pipe(
        mergeMap(map => {
          data.aboutUsId = data.aboutUsId;
          return this.cmsEndPoint.UpdateAboutUsEndpoint(data, data.aboutUsId)
        }));
    }
  }
//#endregion About Us page

//#region Our Services
  GetOurServices(){
    return this.cmsEndPoint.GetOurServicesEndpoint<OurServiceModel[]>();
  }
  AddService(data: OurServiceModel){
    return this.cmsEndPoint.AddServiceEndpoint<OurServiceModel>(data);
  }  
  UpdateService(data: OurServiceModel) {
    if (data.serviceId) {
      return this.cmsEndPoint.UpdateServiceEndpoint(data, data.serviceId);
    }
    else {
      return this.cmsEndPoint.UpdateServiceEndpoint<OurServiceModel>(data, data.serviceId).pipe(
        mergeMap(map => {
          data.serviceId = data.serviceId;
          return this.cmsEndPoint.UpdateServiceEndpoint(data, data.serviceId)
        }));
    }
  }
  DeleteService(id: number) {
    return this.cmsEndPoint.DeleteServiceEndpoint<OurServiceModel>(id);
  }
  //#endregion 

  //#region Danphe Department

  GetDepartmentById(DepartmentId?: number) {
    return this.cmsEndPoint.GetDepartmentByIdEndpoint<DanpheCareDepartment>(DepartmentId);
  }

  GetDepartment() {
    return this.cmsEndPoint.GetDepartmentEndpoint<DanpheCareDepartment[]>();
  }
  AddDepartment(data: DanpheCareDepartment) {
    return this.cmsEndPoint.AddDepartmentEndpoint<DanpheCareDepartment>(data);
  }
  UpdateDepartment(data: DanpheCareDepartment) {
    if (data.departmentId) {
      return this.cmsEndPoint.UpdateDepartmentEndpoint(data, data.departmentId);
    }
    else {
      return this.cmsEndPoint.UpdateDepartmentEndpoint<DanpheCareDepartment>(data, data.departmentId).pipe(
        mergeMap(map => {
          data.departmentId = data.departmentId;
          return this.cmsEndPoint.UpdateDepartmentEndpoint(data, data.departmentId)
        }));
    }
  }
  DeleteDepartment(id: number) {
    return this.cmsEndPoint.DeleteDepartmentEndpoint<DanpheCareDepartment>(id);
  }
  //#endregion Danphe Department

  //#region Danphe Department Sub Heading

  GetSubHeadingByDepartmentId(DepartmentId?: number) {
    return this.cmsEndPoint.GetSubHeadingByDepartmentIdEndpoint<DepartmentSubHeading>(DepartmentId);
  }

  GetDepartmentSubHeading() {
    return this.cmsEndPoint.GetDepartmentSubHeadingEndpoint<DepartmentSubHeading[]>();
  }
  AddDepartmentSubHeading(data: DepartmentSubHeading) {
    return this.cmsEndPoint.AddDepartmentSubHeadingEndpoint<DepartmentSubHeading>(data);
  }
  UpdateDepartmentSubHeading(data: DepartmentSubHeading) {
    if (data.departmentId) {
      return this.cmsEndPoint.UpdateDepartmentSubHeadingEndpoint(data, data.departmentSubHeadingId);
    }
    else {
      return this.cmsEndPoint.UpdateDepartmentSubHeadingEndpoint<DepartmentSubHeading>(data, data.departmentSubHeadingId).pipe(
        mergeMap(map => {
          data.departmentSubHeadingId = data.departmentSubHeadingId;
          return this.cmsEndPoint.UpdateDepartmentSubHeadingEndpoint(data, data.departmentSubHeadingId)
        }));
    }
  }
  DeleteDepartmentSubHeading(id: number) {
    return this.cmsEndPoint.DeleteDepartmentSubHeadingEndpoint<DepartmentSubHeading>(id);
  }
  //#endregion Danphe Department SubHeading

  //#region Danphe Doctor
  GetDoctorById(DoctorId?: number) {
    return this.cmsEndPoint.GetDoctorByIdEndpoint<DanpheCareDoctor>(DoctorId);
  }
  GetDoctor() {
    return this.cmsEndPoint.GetDoctorEndpoint<DanpheCareDoctor[]>();
  }
  GetDoctorSorting() {
    return this.cmsEndPoint.GetDoctorSortingEndpoint<DanpheCareDoctor[]>();
  }
  AddDoctor(data: DanpheCareDoctor) {
    return this.cmsEndPoint.AddDoctorEndpoint<DanpheCareDoctor>(data);
  }

  UpdateDoctor(data: DanpheCareDoctor) {
    if (data.doctorId) {
      return this.cmsEndPoint.UpdateDoctorEndpoint(data, data.doctorId);
    }
    else {
      return this.cmsEndPoint.UpdateDoctorEndpoint<DanpheCareDoctor>(data, data.doctorId).pipe(
        mergeMap(map => {
          data.doctorId = data.doctorId;
          return this.cmsEndPoint.UpdateDoctorEndpoint(data, data.doctorId)
        }));
    }
  }
  DeleteDoctor(id: number) {
    return this.cmsEndPoint.DeleteDoctorEndpoint<DanpheCareDoctor>(id);
  }
  //#endregion Danphe Doctor

  //#region Department Consultation
  GetConsultationById(ConsultationId?: number) {
    return this.cmsEndPoint.GetConsultationByIdEndpoint<DepartmentConsultation>(ConsultationId);
  }
  GetDepConsultation() {
    return this.cmsEndPoint.GetDepConsultationEndpoint<DepartmentConsultation[]>();
  }
  AddDepConsultation(data: DepartmentConsultation) {
    return this.cmsEndPoint.AddDepConsultationEndpoint<DepartmentConsultation>(data);
  }

  UpdateDepConsultation(data: DepartmentConsultation) {
    if (data.departmentConsultationId) {
      return this.cmsEndPoint.UpdateDepConsultationEndpoint(data, data.departmentConsultationId);
    }
    else {
      return this.cmsEndPoint.UpdateDepConsultationEndpoint<DepartmentConsultation>(data, data.departmentConsultationId).pipe(
        mergeMap(map => {
          data.departmentConsultationId = data.departmentConsultationId;
          return this.cmsEndPoint.UpdateDepConsultationEndpoint(data, data.departmentConsultationId)
        }));
    }
  }
  DeleteDepConsultation(id: number) {
    return this.cmsEndPoint.DeleteDepConsultationEndpoint<DepartmentConsultation>(id);
  }
  //#endregion Department Consultation

  //#region Articles
  GetArticlesById(ArticlesId?: number) {
    return this.cmsEndPoint.GetArticlesByIdEndpoint<ResourcefulArticles>(ArticlesId);
  }
  GetArticles() {
    return this.cmsEndPoint.GetArticlesEndpoint<ResourcefulArticles[]>();
  }
  AddArticles(data: ResourcefulArticles) {
    return this.cmsEndPoint.AddArticlesEndpoint<ResourcefulArticles>(data);
  }

  UpdateArticles(data: ResourcefulArticles) {
    if (data.resourcefulArticlesId) {
      return this.cmsEndPoint.UpdateArticlesEndpoint(data, data.resourcefulArticlesId);
    }
    else {
      return this.cmsEndPoint.UpdateArticlesEndpoint<ResourcefulArticles>(data, data.resourcefulArticlesId).pipe(
        mergeMap(map => {
          data.resourcefulArticlesId = data.resourcefulArticlesId;
          return this.cmsEndPoint.UpdateArticlesEndpoint(data, data.resourcefulArticlesId)
        }));
    }
  }
  DeleteArticles(id: number) {
    return this.cmsEndPoint.DeleteArticlesEndpoint<ResourcefulArticles>(id);
  }
  //#endregion Articles

  //#region News
  GetNewsById(NewsId?: number) {
    return this.cmsEndPoint.GetNewsByIdEndpoint<News>(NewsId);
  }
  GetNews() {
    return this.cmsEndPoint.GetNewsEndpoint<News[]>();
  }
  AddNews(data: News) {
    return this.cmsEndPoint.AddNewsEndpoint<News>(data);
  }

  UpdateNews(data: News) {
    if (data.newsId) {
      return this.cmsEndPoint.UpdateNewsEndpoint(data, data.newsId);
    }
    else {
      return this.cmsEndPoint.UpdateNewsEndpoint<News>(data, data.newsId).pipe(
        mergeMap(map => {
          data.newsId = data.newsId;
          return this.cmsEndPoint.UpdateNewsEndpoint(data, data.newsId)
        }));
    }
  }
  DeleteNews(id: number) {
    return this.cmsEndPoint.DeleteNewsEndpoint<News>(id);
  }
  //#endregion News

  //#region Blogs
  GetBlogsById(BlogId?: number) {
    return this.cmsEndPoint.GetBlogsByIdEndpoint<Blogs>(BlogId);
  }
  GetBlogs() {
    return this.cmsEndPoint.GetBlogsEndpoint<Blogs[]>();
  }
  AddBlogs(data: Blogs) {
    return this.cmsEndPoint.AddBlogsEndpoint<Blogs>(data);
  }
  UpdateBlogs(data: Blogs) {
    if (data.blogId) {
      return this.cmsEndPoint.UpdateBlogsEndpoint(data, data.blogId);
    }
    else {
      return this.cmsEndPoint.UpdateBlogsEndpoint<Blogs>(data, data.blogId).pipe(
        mergeMap(map => {
          data.blogId = data.blogId;
          return this.cmsEndPoint.UpdateServiceEndpoint(data, data.blogId)
        }));
    }
  }
  DeleteBlog(id: number) {
    return this.cmsEndPoint.DeleteBlogEndpoint<Blogs>(id);
  }
  //#endregion Blogs

  //#region MetaTag

  GetMetaTagById(MetaTagId?: number) {
    return this.cmsEndPoint.GetMetaTagByIdEndpoint<MetaTag>(MetaTagId);
  }

  GetMetaTag() {
    return this.cmsEndPoint.GetMetaTagEndpoint<MetaTag[]>();
  }
  GetDoctorMetaTag() {
    return this.cmsEndPoint.GetDoctorMetaTagEndpoint<MetaTag[]>();
  }
  GetServiceMetaTag() {
    return this.cmsEndPoint.GetServiceMetaTagEndpoint<MetaTag[]>();
  }
  GetDepartmentMetaTag() {
    return this.cmsEndPoint.GetDepartmentMetaTagEndpoint<MetaTag[]>();
  }
  GetBlogMetaTag() {
    return this.cmsEndPoint.GetBlogMetaTagEndpoint<MetaTag[]>();
  }
  AddMetaTag(data: MetaTag) {
    return this.cmsEndPoint.AddMetaTagEndpoint<MetaTag>(data);
  }
  UpdateMetaTag(data: MetaTag) {
    if (data.metaTagId) {
      return this.cmsEndPoint.UpdateMetaTagEndpoint(data, data.metaTagId);
    }
    else {
      return this.cmsEndPoint.UpdateMetaTagEndpoint<MetaTag>(data, data.metaTagId).pipe(
        mergeMap(map => {
          data.metaTagId = data.metaTagId;
          return this.cmsEndPoint.UpdateMetaTagEndpoint(data, data.metaTagId)
        }));
    }
  }
  DeleteMetaTag(id: number) {
    return this.cmsEndPoint.DeleteMetaTagEndpoint<MetaTag>(id);
  }
  //#endregion Blogs

  //#region DanpheCareContact
  GetContact() {
    return this.cmsEndPoint.GetContactEndpoint<DanpheCareContact[]>();
  }
  AddContact(data: DanpheCareContact) {
    return this.cmsEndPoint.AddContactEndpoint<DanpheCareContact>(data);
  }
  UpdateContact(data: DanpheCareContact) {
    if (data.danpheCareContactId) {
      return this.cmsEndPoint.UpdateContactEndpoint(data, data.danpheCareContactId);
    }
    else {
      return this.cmsEndPoint.UpdateContactEndpoint<DanpheCareContact>(data, data.danpheCareContactId).pipe(
        mergeMap(map => {
          data.danpheCareContactId = data.danpheCareContactId;
          return this.cmsEndPoint.UpdateContactEndpoint(data, data.danpheCareContactId)
        }));
    }
  }
  DeleteContact(id: number) {
    return this.cmsEndPoint.DeleteContactEndpoint<DanpheCareContact>(id);
  }
  //#endregion Blogs

  //#region Expat
  GetExpat() {
    return this.cmsEndPoint.GetExpatEndpoint<Expat[]>();
  }
  AddExpat(data: Expat) {
    return this.cmsEndPoint.AddExpatEndpoint<Expat>(data);
  }
  UpdateExpat(data: Expat) {
    if (data.expatId) {
      return this.cmsEndPoint.UpdateExpatEndpoint(data, data.expatId);
    }
    else {
      return this.cmsEndPoint.UpdateExpatEndpoint<Expat>(data, data.expatId).pipe(
        mergeMap(map => {
          data.expatId = data.expatId;
          return this.cmsEndPoint.UpdateExpatEndpoint(data, data.expatId)
        }));
    }
  }
  DeleteExpat(id: number) {
    return this.cmsEndPoint.DeleteExpatEndpoint<Expat>(id);
  }
  //#endregion Expat

  //#region Webinar
  GetWebinar() {
    return this.cmsEndPoint.GetWebinarEndpoint<Webinar[]>();
  }
  AddWebinar(data: Webinar) {
    return this.cmsEndPoint.AddWebinarEndpoint<Webinar>(data);
  }
  UpdateWebinar(data: Webinar) {
    if (data.webinarId) {
      return this.cmsEndPoint.UpdateWebinarEndpoint(data, data.webinarId);
    }
    else {
      return this.cmsEndPoint.UpdateWebinarEndpoint<Webinar>(data, data.webinarId).pipe(
        mergeMap(map => {
          data.webinarId = data.webinarId;
          return this.cmsEndPoint.UpdateWebinarEndpoint(data, data.webinarId)
        }));
    }
  }
  DeleteWebinar(id: number) {
    return this.cmsEndPoint.DeleteWebinarEndpoint<Webinar>(id);
  }
  //#endregion Webinar

  //#region Our Team Members
  GetOurTeamMember(){
    return this.cmsEndPoint.GetOurTeamMembersEndpoint<TeamMember[]>();
  }
  GetSorting() {
    return this.cmsEndPoint.GetSortingEndpoint<TeamMember[]>();
  }
  AddOurTeamMember(data: TeamMember){
    return this.cmsEndPoint.AddOurTeamMemberEndpoint<TeamMember>(data);
  }  
  UpdateOurTeamMember(data: TeamMember) {
    if (data.teamMemberId) {
      return this.cmsEndPoint.UpdateOurTeamMemberEndpoint(data, data.teamMemberId);
    }
    else {
      return this.cmsEndPoint.UpdateOurTeamMemberEndpoint<TeamMember>(data, data.teamMemberId).pipe(
        mergeMap(map => {
          data.teamMemberId = data.teamMemberId;
          return this.cmsEndPoint.UpdateOurTeamMemberEndpoint(data, data.teamMemberId)
        }));
    }
  }
  DeleteOurTeamMember(id: number) {
    return this.cmsEndPoint.DeleteOurTeamMemberEndpoint<TeamMember>(id);
  }
  //#endregion Our Team Members

 //#region Our Media Coverage
 GetOurMediaCoverage(){
  return this.cmsEndPoint.GetOurMediaCoverageEndpoint<MediaCoverage[]>();
}

AddOurMediaCoverage(data: MediaCoverage){
  return this.cmsEndPoint.AddOurMediaCoverageEndpoint<MediaCoverage>(data);
}

  UpdateOurMediaCoverage(data: MediaCoverage) {
    if (data.mediaId) {
      return this.cmsEndPoint.UpdateOurMediaCoverageEndpoint(data, data.mediaId);
  }
  else {
      return this.cmsEndPoint.UpdateOurTeamMemberEndpoint<MediaCoverage>(data, data.mediaId).pipe(
      mergeMap(map => {
        data.mediaId = data.mediaId;
        return this.cmsEndPoint.UpdateOurMediaCoverageEndpoint(data, data.mediaId)
      }));
  }
}

DeleteOurMediaCoverage(id: number) {
  return this.cmsEndPoint.DeleteOurMediaCoverageEndpoint<MediaCoverage>(id);
}
//#endregion Our Team Members

//#region Testimonials
GetTestimonials(){
  return this.cmsEndPoint.GetTestimonialsEndpoint<Testimonial[]>();
}

AddTestimonial(data: Testimonial){
  return this.cmsEndPoint.AddTestimonialEndpoint<Testimonial>(data);
}

  UpdateTestimonial(data: Testimonial) {
    if (data.testimonialId) {
      return this.cmsEndPoint.UpdateTestimonialEndpoint(data, data.testimonialId);
  }
  else {
      return this.cmsEndPoint.UpdateTestimonialEndpoint<Testimonial>(data, data.testimonialId).pipe(
      mergeMap(map => {
        data.testimonialId = data.testimonialId;
        return this.cmsEndPoint.UpdateOurMediaCoverageEndpoint(data, data.testimonialId)
      }));
  }
}

DeleteTestimonial(id: number) {
  return this.cmsEndPoint.DeleteTestimonialEndpoint<Testimonial>(id);
}
//#endregion Testimonials

//#region Testimonial Main
GetTestimonialMain(){
  return this.cmsEndPoint.GetTestimonialMainEndpoint<TestimonialMain[]>();
}

AddTestimonialMain(data: TestimonialMain){
  return this.cmsEndPoint.AddTestimonialMainEndpoint<TestimonialMain>(data);
}

  UpdateTestimonialMain(data: TestimonialMain) {
    if (data.testimonialMainId) {
      return this.cmsEndPoint.UpdateTestimonialMainEndpoint(data, data.testimonialMainId);
  }
  else {
      return this.cmsEndPoint.UpdateTestimonialMainEndpoint<TestimonialMain>(data, data.testimonialMainId).pipe(
      mergeMap(map => {
        data.testimonialMainId = data.testimonialMainId;
        return this.cmsEndPoint.UpdateTestimonialMainEndpoint(data, data.testimonialMainId)
      }));
  }
}
//#endregion Testimonial Main

  //#region Department Content
  GetTabContentById(TabContentId?: number) {
    return this.cmsEndPoint.GetTabContentByIdEndpoint<DepartmentTabcontent>(TabContentId);
  }
  GetDepTabContent() {
    return this.cmsEndPoint.GetDepTabContentEndpoint<DepartmentTabcontent[]>();
  }
  GetDepTab() {
    return this.cmsEndPoint.GetDepTabEndpoint<DepartmentTab[]>();
  }
  AddDepTabContent(data: DepartmentTabcontent) {
    return this.cmsEndPoint.AddDepTabContentEndpoint<DepartmentTabcontent>(data);
  }

  UpdateDepTabContent(data: DepartmentTabcontent) {
    if (data.departmentContentId) {
      return this.cmsEndPoint.UpdateDepTabContentEndpoint(data, data.departmentContentId);
    }
    else {
      return this.cmsEndPoint.UpdateDepTabContentEndpoint<DepartmentTabcontent>(data, data.departmentContentId).pipe(
        mergeMap(map => {
          data.departmentContentId = data.departmentContentId;
          return this.cmsEndPoint.UpdateDepTabContentEndpoint(data, data.departmentContentId)
        }));
    }
  }
  DeleteDepTabContent(id: number) {
    return this.cmsEndPoint.DeleteDepTabContentEndpoint<DepartmentTabcontent>(id);
  }
  //#endregion Department Tab Content

  GetPackage() {
    return this.cmsEndPoint.GetPackagesEndpoint<Package[]>();
  }

  AddPackage(data: Package) {
    return this.cmsEndPoint.AddPackageEndpoint<Package>(data);
  }
  UpdatePackage(data: Package) {
    if (data.packageModelId) {
      return this.cmsEndPoint.UpdatePackageEndpoint(data, data.packageModelId);
    }
    else {
      return this.cmsEndPoint.UpdatePackageEndpoint<Package>(data, data.packageModelId).pipe(
        mergeMap(map => {
          data.packageModelId = data.packageModelId;
          return this.cmsEndPoint.UpdatePackageEndpoint(data, data.packageModelId)
        }));
    }
  }
  DeletePackage(id: number) {
    return this.cmsEndPoint.DeletePackageEndpoint<Package>(id);
  }

  GetPackageDetailsById(detailId?: number) {
    return this.cmsEndPoint.GetPackageDetailsByIdEndpoint<PackageDetails>(detailId);
  }
  GetPackageDetails() {
    return this.cmsEndPoint.GetPackageDetailsEndpoint<PackageDetails[]>();
  }

  AddPackageDetails(data: PackageDetails) {
    return this.cmsEndPoint.AddPackageDetailsEndpoint<PackageDetails>(data);
  }

  UpdatePackageDetails(data: PackageDetails) {
    if (data.packageDetailId) {
      return this.cmsEndPoint.UpdatePackageDetailsEndpoint(data, data.packageDetailId);
    }
    else {
      return this.cmsEndPoint.UpdatePackageDetailsEndpoint<PackageDetails>(data, data.packageDetailId).pipe(
        mergeMap(map => {
          data.packageDetailId = data.packageDetailId;
          return this.cmsEndPoint.UpdatePackageDetailsEndpoint(data, data.packageDetailId)
        }));
    }
  }
  DeletePackageDetails(id: number) {
    return this.cmsEndPoint.DeletePackageDetailsEndpoint<PackageDetails>(id);
  }

  GetSubServicesById(subservicesId?: number) {
    return this.cmsEndPoint.GetSubServicesByIdEndpoint<SubServices>(subservicesId);
  }
  GetSubServices() {
    return this.cmsEndPoint.GetSubServicesEndpoint<SubServices[]>();
  }

  AddSubServices(data: SubServices) {
    return this.cmsEndPoint.AddSubServicesEndpoint<SubServices>(data);
  }

  UpdateSubServices(data: SubServices) {
    if (data.subServicesId) {
      return this.cmsEndPoint.UpdateSubServicesEndpoint(data, data.subServicesId);
    }
    else {
      return this.cmsEndPoint.UpdateSubServicesEndpoint<SubServices>(data, data.subServicesId).pipe(
        mergeMap(map => {
          data.subServicesId = data.subServicesId;
          return this.cmsEndPoint.UpdateSubServicesEndpoint(data, data.subServicesId)
        }));
    }
  }
  DeleteSubServices(id: number) {
    return this.cmsEndPoint.DeleteSubServicesEndpoint<SubServices>(id);
  }

  GetSubServicesDetailsById(subservicesdetailsId?: number) {
    return this.cmsEndPoint.GetSubServicesDetailsByIdEndpoint<SubServicesDetails>(subservicesdetailsId);
  }
  GetSubServicesDetails() {
    return this.cmsEndPoint.GetSubServicesDetailsEndpoint<SubServicesDetails[]>();
  }

  AddSubServicesDetails(data: SubServicesDetails) {
    return this.cmsEndPoint.AddSubServicesDetailsEndpoint<SubServicesDetails>(data);
  }

  UpdateSubServicesDetails(data: SubServicesDetails) {
    if (data.subServicesId) {
      return this.cmsEndPoint.UpdateSubServicesDetailsEndpoint(data, data.subServicesDetailsId);
    }
    else {
      return this.cmsEndPoint.UpdateSubServicesDetailsEndpoint<SubServicesDetails>(data, data.subServicesDetailsId).pipe(
        mergeMap(map => {
          data.subServicesId = data.subServicesDetailsId;
          return this.cmsEndPoint.UpdateSubServicesDetailsEndpoint(data, data.subServicesId)
        }));
    }
  }
  DeleteSubServicesDetails(id: number) {
    return this.cmsEndPoint.DeleteSubServicesDetailsEndpoint<SubServicesDetails>(id);
  }

}
