
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EndpointBase } from '../../services/endpoint-base.service';
import { ConfigurationService } from '../../services/configuration.service';
import { AuthService } from '../../services/auth.service';

@Injectable()
export class CMSEndpoint extends EndpointBase {

  private readonly _cmsUrl: string = "/api/danphecarecms";

  // private readonly _hospitaldeleteUrl: string = "/api/hospital/deleteHospital";


  constructor(private configurations: ConfigurationService, http: HttpClient, authService: AuthService) {
    super(http, authService);
  }

//#region Start: About Us Page
  // About Us page: Getting already recored About Us details


  GeAboutUsEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getAboutUs`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GeAboutUsEndpoint()));
      }));
  }

  // About Us page: Adding service  
  AddAboutUsEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addAboutUs', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddAboutUsEndpoint(userObject)));
      }));
  }

  // About Us page: Updating service  
  UpdateAboutUsEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateAboutUs/${Id}` : `${this._cmsUrl}/UpdateAboutUs`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateAboutUsEndpoint(userObject, Id)));
      }));
  }
//#endregion End: About Us Page

  //#region Our Services Page
  // Our Services page: getting all recorded services  
  GetOurServicesEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getOurServices`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetOurServicesEndpoint()));
      }));
  }

  // Our Services page: Adding service  
  AddServiceEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addService', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddServiceEndpoint(userObject)));
      }));
  }

  // Our Services page: updating service  
  UpdateServiceEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateService/${Id}` : `${this._cmsUrl}/updateService`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateServiceEndpoint(userObject, Id)));
      }));
  }

  // Our Services page: deleting service  
  DeleteServiceEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteService/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteServiceEndpoint(id)));
      }));
  }

  //#region Danphe Department


  GetDepartmentByIdEndpoint<T>(DepId: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getDepartmentById/${DepId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDepartmentByIdEndpoint(DepId)));
      }));
  }

  GetDepartmentEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getDepartmentList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDepartmentEndpoint()));
      }));
  }

  AddDepartmentEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addDanpheCareDepartment', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddDepartmentEndpoint(userObject)));
      }));
  }
 
  UpdateDepartmentEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateDanpheCareDepartment/${Id}` : `${this._cmsUrl}/updateDanpheCareDepartment`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateDepartmentEndpoint(userObject, Id)));
      }));
  }
 
  DeleteDepartmentEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteDanpheDepartment/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteDepartmentEndpoint(id)));
      }));
  }
  //#endregion Danphe Department

  //#region Danphe Department SubHeading


  GetSubHeadingByDepartmentIdEndpoint<T>(DepId: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getSubHeadingByDepartmentId/${DepId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetSubHeadingByDepartmentIdEndpoint(DepId)));
      }));
  }

  GetDepartmentSubHeadingEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getDepartmentSubHeading`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDepartmentSubHeadingEndpoint()));
      }));
  }

  AddDepartmentSubHeadingEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addDepartmentSubHeading', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddDepartmentSubHeadingEndpoint(userObject)));
      }));
  }

  UpdateDepartmentSubHeadingEndpoint<T>(userObject: any, Id: number): Observable<T> {
    const endpointUrl = Id ? `${this._cmsUrl}/updateDepartmentSubHeading/${Id}` : `${this._cmsUrl}/updateDepartmentSubHeading`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateDepartmentSubHeadingEndpoint(userObject, Id)));
      }));
  }

  DeleteDepartmentSubHeadingEndpoint<T>(id: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/deleteDepartmentSubHeading/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteDepartmentSubHeadingEndpoint(id)));
      }));
  }
  //#endregion Danphe Department SubHeading

  //#region Danphe Doctor

  GetDoctorByIdEndpoint<T>(DoctorId: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getDoctorById/${DoctorId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDoctorByIdEndpoint(DoctorId)));
      }));
  }

  GetDoctorEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getDoctorList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDoctorEndpoint()));
      }));
  }
  GetDoctorSortingEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getDoctorSortingList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDoctorSortingEndpoint()));
      }));
  }

  AddDoctorEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addDanpheCareDoctor', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddDoctorEndpoint(userObject)));
      }));
  }

  UpdateDoctorEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateDanpheCareDoctor/${Id}` : `${this._cmsUrl}/updateDanpheCareDoctor`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateDoctorEndpoint(userObject, Id)));
      }));
  }

  DeleteDoctorEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteDanpheDoctor/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteDoctorEndpoint(id)));
      }));
  }
  //#endregion Danphe Doctor

  //#region Danphe DepartmentConsultation


  GetConsultationByIdEndpoint<T>(ConId: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getConsultationById/${ConId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetConsultationByIdEndpoint(ConId)));
      }));
  }

  GetDepConsultationEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getConsultationList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDepConsultationEndpoint()));
      }));
  }

  AddDepConsultationEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addConsultation', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddDepConsultationEndpoint(userObject)));
      }));
  }

  UpdateDepConsultationEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateDepartmentConsultation/${Id}` : `${this._cmsUrl}/updateDepartmentConsultation`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateDepConsultationEndpoint(userObject, Id)));
      }));
  }

  DeleteDepConsultationEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteDepartmentConsultation/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteDepConsultationEndpoint(id)));
      }));
  }
  //#endregion Danphe Department Consultation

  //#region Danphe Resourceful Articles

  GetArticlesByIdEndpoint<T>(ArticlesId: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getArticlesById/${ArticlesId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetArticlesByIdEndpoint(ArticlesId)));
      }));
  }

  GetArticlesEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getArtiles`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetArticlesEndpoint()));
      }));
  }

  AddArticlesEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addArticles', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddArticlesEndpoint(userObject)));
      }));
  }

  UpdateArticlesEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateArticles/${Id}` : `${this._cmsUrl}/updateArticles`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateArticlesEndpoint(userObject, Id)));
      }));
  }

  DeleteArticlesEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteArticles/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteArticlesEndpoint(id)));
      }));
  }
  //#endregion Danphe Resourceful Articles

  //#region Danphe News

  GetNewsByIdEndpoint<T>(NewsId: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getNewsById/${NewsId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetNewsByIdEndpoint(NewsId)));
      }));
  }

  GetNewsEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getNews`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetNewsEndpoint()));
      }));
  }

  AddNewsEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addNews', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddNewsEndpoint(userObject)));
      }));
  }

  UpdateNewsEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateNews/${Id}` : `${this._cmsUrl}/updateNews`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateNewsEndpoint(userObject, Id)));
      }));
  }

  DeleteNewsEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteNews/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteNewsEndpoint(id)));
      }));
  }
  //#endregion Danphe News

  //#region Danphe Blogs

  GetBlogsByIdEndpoint<T>(BlogsId: number): Observable<T> {

   const endpointUrl = `${this._cmsUrl}/getBlogsById/${BlogsId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetBlogsByIdEndpoint(BlogsId)));
      }));
  }

  GetBlogsEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getBlogs`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetBlogsEndpoint()));
      }));
  }

  AddBlogsEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addBlogs', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddBlogsEndpoint(userObject)));
      }));
  }

  UpdateBlogsEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateBlogs/${Id}` : `${this._cmsUrl}/updateBlogs`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateBlogsEndpoint(userObject, Id)));
      }));
  }

  DeleteBlogEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteBlogs/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteBlogEndpoint(id)));
      }));
  }
  //#endregion Danphe Blogs

  //#region Danphe MetaTag


  GetMetaTagByIdEndpoint<T>(MetaTagId: number): Observable<T> {
    
   const endpointUrl = `${this._cmsUrl}/getMetaTagById/${MetaTagId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetMetaTagByIdEndpoint(MetaTagId)));
      }));
  }

  GetMetaTagEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getMetaTag`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetMetaTagEndpoint()));
      }));
  }
  GetBlogMetaTagEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/blogMetaTag`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetBlogMetaTagEndpoint()));
      }));
  }
  GetDoctorMetaTagEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/doctorMetaTag`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDoctorMetaTagEndpoint()));
      }));
  }
  GetServiceMetaTagEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/serviceMetaTag`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetServiceMetaTagEndpoint()));
      }));
  }
  GetDepartmentMetaTagEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/departmentMetaTag`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDepartmentMetaTagEndpoint()));
      }));
  }

  AddMetaTagEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addMetaTag', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddMetaTagEndpoint(userObject)));
      }));
  }

  UpdateMetaTagEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateMetaTags/${Id}` : `${this._cmsUrl}/updateMetaTags`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateMetaTagEndpoint(userObject, Id)));
      }));
  }

  DeleteMetaTagEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteMetaTag/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteMetaTagEndpoint(id)));
      }));
  }
  //#endregion Danphe MetaTag

  //#region Danphe Contact
  GetContactEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getDanpheCareContact`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetContactEndpoint()));
      }));
  }

  AddContactEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addDanpheContact', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddContactEndpoint(userObject)));
      }));
  }

  UpdateContactEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateContact/${Id}` : `${this._cmsUrl}/updateContact`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateContactEndpoint(userObject, Id)));
      }));
  }

  DeleteContactEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteContact/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteContactEndpoint(id)));
      }));
  }
  //#endregion Danphe Contact

  //#region Danphe Expat
  GetExpatEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getExpat`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetExpatEndpoint()));
      }));
  }

  AddExpatEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addExpat', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddExpatEndpoint(userObject)));
      }));
  }

  UpdateExpatEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateExpat/${Id}` : `${this._cmsUrl}/updateExpat`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateExpatEndpoint(userObject, Id)));
      }));
  }

  DeleteExpatEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteExpat/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteExpatEndpoint(id)));
      }));
  }
  //#endregion Danphe Expat

  //#region Danphe Webinar
  GetWebinarEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getWebinar`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetWebinarEndpoint()));
      }));
  }

  AddWebinarEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addWebinar', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddWebinarEndpoint(userObject)));
      }));
  }

  UpdateWebinarEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateWebinar/${Id}` : `${this._cmsUrl}/updateWebinar`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateWebinarEndpoint(userObject, Id)));
      }));
  }

  DeleteWebinarEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteWebinar/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteWebinarEndpoint(id)));
      }));
  }
  //#endregion Danphe Webinar


  //#region  Start: Our Team Member 

  GetOurTeamMembersEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getOurTeamMembers`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetOurTeamMembersEndpoint()));
      }));
  }
  GetSortingEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getTeamSortingList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetSortingEndpoint()));
      }));
  }

  AddOurTeamMemberEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addOurTeamMember', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddOurTeamMemberEndpoint(userObject)));
      }));
  }

  UpdateOurTeamMemberEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateOurTeam/${Id}` : `${this._cmsUrl}/updateOurTeam`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateOurTeamMemberEndpoint(userObject, Id)));
      }));
  }

  DeleteOurTeamMemberEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteOurTeamMember/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteOurTeamMemberEndpoint(id)));
      }));
  }
  //#endregion End : Our Team Member 

  //#region : Our Media Coverage  

  GetOurMediaCoverageEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getMediaCoverage`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetOurMediaCoverageEndpoint()));
      }));
  }

  AddOurMediaCoverageEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addMediaCoverage', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddOurMediaCoverageEndpoint(userObject)));
      }));
  }

  UpdateOurMediaCoverageEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateMediaCoverage/${Id}` : `${this._cmsUrl}/updateMediaCoverage`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateOurMediaCoverageEndpoint(userObject, Id)));
      }));
  }

  DeleteOurMediaCoverageEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteMediaCoverage/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteOurMediaCoverageEndpoint(id)));
      }));
  }
  //#endregion  Our Media Coverage 

  //#region : Testimonials

  GetTestimonialsEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getTestimonials`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetTestimonialsEndpoint()));
      }));
  }

  AddTestimonialEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addTestimonial', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddTestimonialEndpoint(userObject)));
      }));
  }

  UpdateTestimonialEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateTestimonial/${Id}` : `${this._cmsUrl}/updateTestimonial`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateTestimonialEndpoint(userObject, Id)));
      }));
  }

  DeleteTestimonialEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteTestimonial/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteTestimonialEndpoint(id)));
      }));
  }
  //#endregion : Testimonials

  //#region : TestimonialsMain
  GetTestimonialMainEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getTestimonialMain`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetTestimonialMainEndpoint()));
      }));
  }

  AddTestimonialMainEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addTestimonialMain', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddTestimonialMainEndpoint(userObject)));
      }));
  }

  UpdateTestimonialMainEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateTestimonialMain/${Id}` : `${this._cmsUrl}/updateTestimonialMain`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateTestimonialMainEndpoint(userObject, Id)));
      }));
  }

  //#endregion : TestimonialsMain

  //#region Department Tab Content 

  GetTabContentByIdEndpoint<T>(TabContentId: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getTabContentById/${TabContentId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetTabContentByIdEndpoint(TabContentId)));
      }));
  }

  GetDepTabContentEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getContent`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDepTabContentEndpoint()));
      }));
  }

  GetDepTabEndpoint<T>(): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/getTab`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetDepTabEndpoint()));
      }));
  }

  AddDepTabContentEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addDepartmentTabContent', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddDepTabContentEndpoint(userObject)));
      }));
  }

  UpdateDepTabContentEndpoint<T>(userObject: any, Id: number): Observable<T> {
   const endpointUrl = Id ? `${this._cmsUrl}/updateDepartmentTabContent/${Id}` : `${this._cmsUrl}/updateDepartmentTabContent`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateDepTabContentEndpoint(userObject, Id)));
      }));
  }

  DeleteDepTabContentEndpoint<T>(id: number): Observable<T> {
   const endpointUrl = `${this._cmsUrl}/deleteTabContent/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteDepTabContentEndpoint(id)));
      }));
  }
  
  //#endregion Department Tab Content

  GetPackagesEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getPackageNameList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetPackagesEndpoint()));
      }));
  }

  AddPackageEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addPackageName', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddPackageEndpoint(userObject)));
      }));
  }

  UpdatePackageEndpoint<T>(userObject: any, Id: number): Observable<T> {
    const endpointUrl = Id ? `${this._cmsUrl}/updatePackageName/${Id}` : `${this._cmsUrl}/updatePackageName`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdatePackageEndpoint(userObject, Id)));
      }));
  }

  DeletePackageEndpoint<T>(id: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/deletePackageName/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeletePackageEndpoint(id)));
      }));
  }

  GetPackageDetailsByIdEndpoint<T>(detailId: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getPackageDetailsById/${detailId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetPackageDetailsByIdEndpoint(detailId)));
      }));
  }

  GetPackageDetailsEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getPackageList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetPackageDetailsEndpoint()));
      }));
  }


  AddPackageDetailsEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addPackageDetails', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddPackageDetailsEndpoint(userObject)));
      }));
  }

  UpdatePackageDetailsEndpoint<T>(userObject: any, Id: number): Observable<T> {
    const endpointUrl = Id ? `${this._cmsUrl}/updatePackageDetails/${Id}` : `${this._cmsUrl}/updatePackageDetails`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdatePackageDetailsEndpoint(userObject, Id)));
      }));
  }

  DeletePackageDetailsEndpoint<T>(id: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/deletePackageDetails/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeletePackageDetailsEndpoint(id)));
      }));
  }


  GetSubServicesByIdEndpoint<T>(subservicesId: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getSubServicesById/${subservicesId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetSubServicesByIdEndpoint(subservicesId)));
      }));
  }

  GetSubServicesEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getSubServicesList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetSubServicesEndpoint()));
      }));
  }


  AddSubServicesEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addSubServices', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddSubServicesEndpoint(userObject)));
      }));
  }

  UpdateSubServicesEndpoint<T>(userObject: any, Id: number): Observable<T> {
    const endpointUrl = Id ? `${this._cmsUrl}/updateSubServices/${Id}` : `${this._cmsUrl}/updateSubServices`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateSubServicesEndpoint(userObject, Id)));
      }));
  }

  DeleteSubServicesEndpoint<T>(id: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/deleteSubServices/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteSubServicesEndpoint(id)));
      }));
  }

  GetSubServicesDetailsByIdEndpoint<T>(subservicesdetailsId: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getSubServicesDetailsById/${subservicesdetailsId}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetSubServicesByIdEndpoint(subservicesdetailsId)));
      }));
  }

  GetSubServicesDetailsEndpoint<T>(): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/getSubServicesDetailsList`;
    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.GetSubServicesDetailsEndpoint()));
      }));
  }


  AddSubServicesDetailsEndpoint<T>(userObject: any): Observable<T> {
    return this.http.post<T>(this._cmsUrl + '/addSubServicesDetails', JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.AddSubServicesDetailsEndpoint(userObject)));
      }));
  }

  UpdateSubServicesDetailsEndpoint<T>(userObject: any, Id: number): Observable<T> {
    const endpointUrl = Id ? `${this._cmsUrl}/updateSubServicesDetails/${Id}` : `${this._cmsUrl}/updateSubServicesDetails`;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.UpdateSubServicesDetailsEndpoint(userObject, Id)));
      }));
  }

  DeleteSubServicesDetailsEndpoint<T>(id: number): Observable<T> {
    const endpointUrl = `${this._cmsUrl}/deleteSubServicesDetails/${id}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.DeleteSubServicesDetailsEndpoint(id)));
      }));
  }

}
