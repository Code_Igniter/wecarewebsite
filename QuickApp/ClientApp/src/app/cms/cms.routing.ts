import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AboutUsComponent } from "./components/about-us/about-us.component";
import { SideMenuComponent } from "./components/sidemenu/side-menu.component";
import { OurServicesComponent } from "./components/our-services/our-services.component";
import { OurTeamMemberComponent } from "./components/our-team/cms-our-team.component";
import { OurMediaCoverageComponent } from "./components/our-media-coverage/cms-our-media-coverage.component";
import { TestimonialComponent } from "./components/testimonial/cms-our-testimonial.component";
import { CMSBlogsComponent } from "./components/blogs/cms-blogs.component";
import { CMSContactComponent } from "./components/contact/cms-contact.component";
import { CMSDepartmentComponent } from "./components/department/cms-department.component";
import { CMSDepartmentConsultationComponent } from "./components/departmentconsultation/cms-dep-consultation.compoent";
import { CMSDoctorComponent } from "./components/doctor/cms-doctor.component";
import { CMSArticlesComponent } from "./components/resourcefularticles/cms-articles.component";
import { CMSExpatComponent } from "./components/expat/cms-expat.component";
import { CMSMetaTagComponent } from "./components/metatag/cms-metatag.component";
import { CMSNewsComponent } from "./components/news/cms-news.component";
import { CMSWebinarComponent } from "./components/webinar/cms-webinar.component";
import { CMSDepartmentTabContentComponent } from "./components/department/department-tab.component";
import { CMSAddBlogsComponent } from './components/blogs/cms-blogs-add.component';
import { CMSDepartmentTabAddContentComponent } from './components/department/department-tab-add.component';
import { CMSDepartmentAddConsultationComponent } from './components/departmentconsultation/cms-dep-consultation-add.component';
import { CMSAddExpatComponent } from './components/expat/cms-expat-add.component';
import { CMSAddNewsComponent } from './components/news/cms-news-add.component';
import { OurServicesAddComponent } from './components/our-services/our-services-add.component';
import { CMSArticlesAddComponent } from './components/resourcefularticles/cms-articles-add.component';
import { TestimonialAddComponent } from './components/testimonial/cms-testimonial-add.component';
import { DepartmentSubHeadingComponent } from './components/department/department-sub-heading.component';
import { CMSDoctorAddComponent } from './components/doctor/cms-doctor-add.component';
import { CMSDoctorEditComponent } from './components/doctor/cms-doctor-edit.component';
import { CMSDepartmentEditComponent } from './components/department/cms-department-edit.component';
import { OurServicesEditComponent } from './components/our-services/our-services-edit.component';
import { CMSMetaTagServiceComponent } from "./components/metatag/cms-metatag-service.component";
import { CMSMetaTagBlogComponent } from "./components/metatag/cms-metatag-blog.component";
import { CMSMetaTagDepartmentComponent } from "./components/metatag/cms-metatag-department.component";
import { CMSMetaTagDoctorComponent } from "./components/metatag/cms-metatag-doctor.component";
import { CMSPackageComponent } from "./components/package/package.component";
import { CMSPackageDetailsComponent } from "./components/package/package-details.component";
import { CMSPackageDetailsAddComponent } from "./components/package/package-details-add.component";
import { CMSPackageDetailsEditComponent } from "./components/package/package-details-update.component";
import { CMSSubServicesComponent } from "./components/sub-services/cms-sub-services.component";
import { CMSSubServicesAddComponent } from "./components/sub-services/cms-sub-services-add.component";
import { CMSSubServicesEditComponent } from "./components/sub-services/cms-sub-services-edit.component";
import { CMSSubServicesDetailsComponent } from "./components/sub-services/cms-sub-services-details.component";
import { CMSSubServicesDetailsAddComponent } from "./components/sub-services/cms-sub-services-details-add.component";
import { CMSSubServicesDetailsEditComponent } from "./components/sub-services/cms-sub-services-details-edit.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '', component: SideMenuComponent,
                children: [
                    { path: 'services', component: OurServicesComponent },
                    { path: 'aboutus', component: AboutUsComponent },
                    { path: 'ourteam', component: OurTeamMemberComponent },
                    { path: 'ourmedia', component: OurMediaCoverageComponent },
                    { path: 'testimonial', component: TestimonialComponent },
                    { path: 'cmsblogs', component: CMSBlogsComponent },
                    { path: 'cmscontact', component: CMSContactComponent },
                    { path: 'cmsdepartment', component: CMSDepartmentComponent },
                    { path: 'cmsconsultation', component: CMSDepartmentConsultationComponent },
                    { path: 'cmsdoctor', component: CMSDoctorComponent },
                    { path: 'cmsarticles', component: CMSArticlesComponent },
                    { path: 'cmsexpat', component: CMSExpatComponent },
                    { path: 'cmsmetatag', component: CMSMetaTagComponent },
                    { path: 'cmsnews', component: CMSNewsComponent },
                  { path: 'cmswebinar', component: CMSWebinarComponent },
                  { path: 'departmentdetails', component: CMSDepartmentTabContentComponent },
                  { path: 'addblogs', component: CMSAddBlogsComponent },
                  { path: 'adddeptabcontent', component: CMSDepartmentTabAddContentComponent },
                  { path: 'adddepconsultation', component: CMSDepartmentAddConsultationComponent },
                  { path: 'addexpat', component: CMSAddExpatComponent },
                  { path: 'addnews', component: CMSAddNewsComponent },
                  { path: 'addservices', component: OurServicesAddComponent },
                  { path: 'addarticles', component: CMSArticlesAddComponent },
                  { path: 'addtestimonial', component: TestimonialAddComponent },
                  { path: 'departmentsubheading', component: DepartmentSubHeadingComponent },
                  { path: 'cmsdoctoradd', component: CMSDoctorAddComponent },
                  { path: 'cmsdoctoredit', component: CMSDoctorEditComponent },
                  { path: 'cmsdepartmentedit', component: CMSDepartmentEditComponent },
                  { path: 'cmsservicesedit', component: OurServicesEditComponent },
                  { path: 'metatagservice', component: CMSMetaTagServiceComponent },
                  { path: 'metatagblog', component: CMSMetaTagBlogComponent },
                  { path: 'metatagdepartment', component: CMSMetaTagDepartmentComponent },
                  { path: 'metatagdoctor', component: CMSMetaTagDoctorComponent },
                  { path: 'cmspackage', component: CMSPackageComponent, data: { title: 'Package' } },
                  { path: 'cmspackagedetails', component: CMSPackageDetailsComponent, data: { title: 'Package Details' } },
                  { path: 'cmspackagedetailsadd', component: CMSPackageDetailsAddComponent, data: { title: 'Package Details Add' } },
                  { path: 'cmspackagedetailsedit', component: CMSPackageDetailsEditComponent, data: { title: 'Package Details Update' } },
                  { path: 'cmssubservices', component: CMSSubServicesComponent, data: { title: 'CMS Services' } },
                  { path: 'cmssubservicesadd', component: CMSSubServicesAddComponent, data: { title: 'CMS Services Add' } },
                  { path: 'cmssubservicesedit', component: CMSSubServicesEditComponent, data: { title: 'CMS Services Edit' } },
                  { path: 'cmssubservicesdetails', component: CMSSubServicesDetailsComponent, data: { title: 'CMS SubServices Details' } },
                  { path: 'cmsservicesdetailsadd', component: CMSSubServicesDetailsAddComponent, data: { title: 'CMS SubServices Details Add' } },
                  { path: 'cmssubservicesdetailsedit', component: CMSSubServicesDetailsEditComponent, data: { title: 'CMS SubServices Details Update' } },
                    { path: '**', redirectTo:''}
                ],
                
            }
        ])
    ],
    exports: [                                                                                                                                                                                                                                                   
        RouterModule
    ]
})
export class CMSRoutingModule {

}
