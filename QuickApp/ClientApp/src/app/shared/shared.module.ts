import { NgModule } from "@angular/core";
import { SafePipe } from "src/common/common.safe";

@NgModule({
    declarations:[SafePipe],
    exports:[SafePipe],
})
export class SharedModule {

}