
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from "jquery";
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { DanpheCareDepartment, MetaTag } from '../../cms/models/danphecare.cms.model';
import { Title, Meta } from '@angular/platform-browser';
// declare var $: any;

@Component({
  selector: 'app-department-list',
  templateUrl: './departmentdetails-list.component.html'
})

export class DepartmentDetailsListComponent implements OnInit {
  public departmentList: Array<DanpheCareDepartment> = new Array<DanpheCareDepartment>();
  public departmentId: number;
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  constructor(public routing: Router, public websiteService: WebsiteService, public notifyService: NotificationService, private titleService: Title, private metaService: Meta) {



  }
  ngOnInit() {
    this.GetDepartment();
    /*this.GetMetaTag();*/
  }
  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Department List Page');
  
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.updateTag({ name: 'keywords', content: `${this.tag.keywords}` });
      this.metaService.updateTag({ name: 'description', content: `${this.tag.content}` });
      //this.metaService.addTags([
      //  { name: 'keywords', content: `${this.tag.keywords}` },
      //  { name: 'description', content: `${this.tag.content}` },
      //  { name: 'robots', content: 'index, follow' }
      //]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }
  GetDepartment() {
    this.websiteService.GetDepartment().subscribe(res => {
      if (res) {
        this.departmentList = [];

        this.departmentList = Object.assign(this.departmentList, res);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Department Found!");
      });
  }
  DepartmentDetails(permaLink) {
    //this.departmentId = id;
    this.routing.navigate(['/department', permaLink]);
  }

}
