

import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { DanpheCareDepartment, DanpheCareDoctor, DanpheCareContact, ResourcefulArticles, DepartmentSubHeading, Blogs, DepartmentTabcontent, MetaTag } from '../../cms/models/danphecare.cms.model';
import { OurServiceModel } from '../../cms/models/OurServices.model';
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { DanphecareService } from '../../services/danphecare/danphe.service';
import { DanpheCareReachUsQuicklyModel } from '../../models/danphecare/danphecare.model';
import { MetaService } from '@ngx-meta/core';




@Component({
  selector: 'app-department-details-page',
  templateUrl: './departmentdetails-page.component.html'
})
export class DepartmentDetailsPageComponent implements OnInit, AfterViewInit, OnDestroy {
  public departmentdetailId: string;
  public departmentId: number;
  public departmentPageId: number;
  public departmentDetails: any;
  public departmentFaq: Array<DepartmentTabcontent> = new Array<DepartmentTabcontent>();
  public deptFaq= true;
  public departmentComponents: any;
  public departmentSetup: any;
  public departmentWarranty: any;
  public departmentService: any;
  public servicesList: Array<OurServiceModel> = new Array<OurServiceModel>();
  public servicesId: number;
  public articlesId: number;
  public doctorId: number;
  public reachForm1: FormGroup;
  public submitForm1: boolean = false;
  public reachForm2: FormGroup;
  public submitForm2: boolean = false;
  public reachForm3: FormGroup;
  public submitForm3: boolean = false;
  public departmentList: Array<DanpheCareDepartment> = new Array<DanpheCareDepartment>();
  public subheadingList: Array<DepartmentSubHeading> = new Array<DepartmentSubHeading>();
  public doctorList: Array<DanpheCareDoctor> = new Array<DanpheCareDoctor>();
  public articlesList: Array<ResourcefulArticles> = new Array<ResourcefulArticles>();
  public contact: DanpheCareContact = new DanpheCareContact();
  public department: DanpheCareDepartment = new DanpheCareDepartment();
  public loading1: boolean = false;
  public loading2: boolean = false;
  public loading3: boolean = false;
  public content: string;
  //public departmentId: number;
  //expert = require("../content/img/about-expert.png");
  isolation = require("../content/img/home-isolation.png");
  initiation1 = require("../content/img/initiation1.png");
  safety = require("../content/img/initiation2.png");
  learning = require("../content/img/initiation3.png");
  public blogsList: Array<Blogs> = new Array<Blogs>();
  public blogId: number;
  public IsSubHeading = true;
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  public showModelBox: boolean = false;
  public DhcareRuq: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  public DhcareRuq1: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  public DhcareRuq2: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  public permaLink: any;
  constructor(public routing: Router, private route: ActivatedRoute, public websiteService: WebsiteService, public formBuilder: FormBuilder,
    private notifyService: NotificationService, private titleService: Title, private metaService: Meta,
    public danphecareservice: DanphecareService, private readonly meta: MetaService) {
    this.route.params.subscribe((params: Params) => {
      this.permaLink = params.permalink;
    });
    //this.departmentdetailId = this.route.snapshot.queryParamMap.get('id');
    //this.departmentId = parseInt(this.departmentdetailId, 10);
  }

  ngOnInit() {

    //this.departmentId = parseInt(this.departmentdetailId, 10);
    this.GetDepartmentTabContent(this.permaLink);
    this.GetServiceByDepartment(this.permaLink);
    this.GetDepartmentById(this.permaLink);
    this.GetDoctorByDepartmentId(this.permaLink);
    //this.GetDepartmentTabContent(this.departmentId);
    //this.GetServiceByDepartment(this.departmentId);
    //this.GetDepartmentById(this.departmentId);
    //this.GetDoctorByDepartmentId(this.departmentId);
    this.GetServices();

    this.GetDepartment();

    this.GetContact();
    this.GetBlogs();
    //this.GetResourcefulArticles();
    this.GetDepartmentSubHeading();
    /*this.GetMetaTag();*/
    this.routing.routeReuseStrategy.shouldReuseRoute = () => false;
    this.reachForm1 = this.formBuilder.group({
      phonenumber1: ['', [Validators.required]],
      name1: ['', [Validators.required]],
      email1: ["", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ]
    });
    this.reachForm2 = this.formBuilder.group({
      phonenumber2: ['', [Validators.required]],
      name2: ['', [Validators.required]]
    });
    this.reachForm3 = this.formBuilder.group({
      phonenumber3: ['', [Validators.required]],
      name3: ['', [Validators.required]],
      email3: ["", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ]
    });
  }
  ngAfterViewInit() {
    //
  }
  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Department Details Page');
 
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.updateTag({ name: 'keywords', content: `${this.tag.keywords}` });
      this.metaService.updateTag({ name: 'description', content: `${this.tag.content}` });
      //this.metaService.addTags([
      //  { name: 'keywords', content: `${this.tag.keywords}` },
      //  { name: 'description', content: `${this.tag.content}` },
      //  { name: 'robots', content: 'index, follow' }
      //]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }

  GetSocialCard() {
    this.content = "";
    this.content = this.department.introduction.replace(/<[^>]*>/g, '').trim();
    this.content = this.content.slice(0, 200);
    const imgSubstr = this.department.imagePath.substr(23);
    const imageUrl = "https://danphecare.com/Upload/DanpheCareCMS" + imgSubstr;
    this.meta.setTitle(this.department.title);
    this.meta.setTag('og:image', imageUrl);
    this.meta.setTag('og:url', location.href);
    this.meta.setTag('og:type', "Department");
    this.meta.setTag('og:description', this.content + " ... ");
    this.meta.setTag('twitter:card', location.href);
    this.meta.setTag('twitter:title', this.department.title);
    this.meta.setTag('twitter:description', this.content + " ... ");
    this.meta.setTag('twitter:image', imageUrl);
    this.meta.setTag('og:app_id', "260103905625004");
    //this.metaService.updateTag({ property: 'fb:app_id', content: "260103905625004" });
    //this.metaService.updateTag({ property: 'og:url', content: location.href });
    //this.metaService.updateTag({ property: 'og:title', content: this.department.title });
    //this.metaService.updateTag({ property: 'og:type', content: "Department" });
    //this.metaService.updateTag({ property: 'og:description', content: this.content + " ... " });
    //this.metaService.updateTag({ property: 'og:image', content: imageUrl });
    //this.metaService.updateTag({ property: 'og:image:height', content: "150" });
    //this.metaService.updateTag({ property: 'og:image:width', content: "150" });
    //this.metaService.updateTag({ property: 'twitter:card', content: location.href });
    //this.metaService.updateTag({ property: 'twitter:title', content: this.department.title });
    //this.metaService.updateTag({ property: 'twitter:description', content: this.content + " ... " });
    //this.metaService.updateTag({ property: 'twitter:image', content: imageUrl });

  }
  GetDepartmentTabContent(permalink) {
    this.websiteService.GetDepartmentTabContentById(permalink)
      .subscribe(res => this.SuccessTabContent(res),
        res => this.Error(res));
  }
  SuccessTabContent(res) {
    this.departmentDetails = res;
    this.departmentFaq = res.filter(x => x.tabName === 'FAQs');
    if (this.departmentFaq.length === 0) {
      this.deptFaq = false;
    }
    this.departmentComponents = res.filter(x => x.tabName === 'Components');
    this.departmentSetup = res.filter(x => x.tabName === 'Shock Setup');
    this.departmentWarranty = res.filter(x => x.tabName === 'Warranty and Registration');
  }
  Error(res) {
    //this.notifyService.showError("Info", "No Details Found!");
  }

  GetServiceByDepartment(permalink) {
    this.websiteService.GetServiceByDepartmentId(permalink)
      .subscribe(res => this.SuccessServices(res),
        res => this.ErrorServices(res));
  }
  SuccessServices(res) {

    this.departmentService = res;
  }
  ErrorServices(res) {
    //this.notifyService.showError("Info", "No Services Found!");
  }

  GetDoctorByDepartmentId(permalink) {
    this.websiteService.GetDoctorByDepartmentId(permalink)
      .subscribe(res => this.SuccessDoctor(res),
        res => this.ErrorDoctor(res));
  }
  SuccessDoctor(res) {
    this.doctorList = res;
  }
  ErrorDoctor(res) {
    //this.notifyService.showError("Info", "No Doctor Found!");
  }

  GetDepartmentById(permalink) {
    this.websiteService.GetDepartmentById(permalink)
      .subscribe(res => this.SuccessDepartmentById(res),
        res => this.ErrorDepartment(res));
  }
  SuccessDepartmentById(res) {
    //this.departmentList = res;
    //this.departmentList = res.filter((dep, idx) => idx < 4);

    this.department = res;
    this.GetSocialCard();
  }

  GetDepartment() {
    this.websiteService.GetDepartment()
      .subscribe(res => this.SuccessDepartment(res),
        res => this.ErrorDepartment(res));
  }
  SuccessDepartment(res) {
    this.departmentList = res;
    this.departmentList = res.filter((dep, idx) => idx < 4);
  }

  ErrorDepartment(res) {
    //this.notifyService.showError("Info", "No Department Found!");
  }

  GetDepartmentSubHeading() {
    this.websiteService.GetSubHeadingByDepartmentId(this.permaLink)
      .subscribe(res => this.SuccessDepartmentSubHeading(res),
        res => this.ErrorDepartmentSubHeading(res));
  }
  SuccessDepartmentSubHeading(res) {
    this.subheadingList = res;
    if (res.length === 0) {
      this.IsSubHeading = false;
    }
  }
  ErrorDepartmentSubHeading(res) {
    //this.notifyService.showError("Info", "No Department SubHeading Found!");
  }

  GetServices() {
    this.websiteService.GetOurServices()
      .subscribe(res => this.SuccessServicesList(res),
        res => this.ErrorServicesList(res));
  }
  SuccessServicesList(res) {
  
    //this.servicesList = res;
    this.servicesList = res.filter((dep, idx) => idx < 3);
  }
  ErrorServicesList(res) {
    //this.notifyService.showError("Info", "No Services Found!");
  }
  GetContact() {
    this.websiteService.GetContact().subscribe(res => {
      if (res && res.length > 0) {
        this.contact = Object.assign(this.contact, res[0]);

      }
    },
      res => {
        //this.notifyService.showError("Error", "Internal Error")
      });
  }
  GetResourcefulArticles() {
    this.websiteService.GetArticles()
      .subscribe(res => this.SuccessArticles(res),
        res => this.ErrorArticles(res));
  }
  SuccessArticles(res) {
    this.articlesList = res;
  }
  ErrorArticles(res) {
    //this.notifyService.showError("Info", "No Articles Found!");
  }

  GoToArticlesPage(id) {
    this.articlesId = id;
    this.routing.navigate(['/articlespage'], { queryParams: { id: this.articlesId } });
  }
  GoToProfile(id) {
    this.doctorId = id;
    this.routing.navigate(['/profilepage'], { queryParams: { id: this.doctorId } });
  }
  GotToService(id) {
    this.servicesId = id;
    this.routing.navigate(['/service'], { queryParams: { id: this.servicesId } });
  }
  DepartmentDetails(permaLink) {
    //this.departmentPageId = id;
    this.routing.navigate(['/department', permaLink]);
    this.GetDepartmentTabContent(this.departmentPageId);
    this.GetServiceByDepartment(this.departmentPageId);
    this.GetServices();
    this.GetDepartment();
    this.GetDoctorByDepartmentId(this.departmentPageId);
    this.GetContact();
    this.GetResourcefulArticles();
  }

  GetBlogs() {
    this.websiteService.GetBlogs().subscribe(res => {
      if (res) {
        this.blogsList = [];
        this.blogsList = Object.assign(this.blogsList, res);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Blogs Found!");
      });
  }
  GoToBlogDetails(permaLink) {
    //this.blogId = id;
    this.routing.navigate(['/blog', permaLink]);


  }
  get PhoneNumberControl2() {
    return this.reachForm2.get("phonenumber2") as FormControl;
  }
  
  get NameControl2() {
    return this.reachForm2.get("name2") as FormControl;
  }
  get PhoneNumberControl1() {
    return this.reachForm1.get("phonenumber1") as FormControl;
  }

  get NameControl1() {
    return this.reachForm1.get("name1") as FormControl;
  }
  get EmailControl1() {
    return this.reachForm1.get("email1") as FormControl;
  }
  get PhoneNumberControl3() {
    return this.reachForm3.get("phonenumber3") as FormControl;
  }

  get NameControl3() {
    return this.reachForm3.get("name3") as FormControl;
  }
  get EmailControl3() {
    return this.reachForm3.get("email3") as FormControl;
  }

  onSubmitBook() {
    if (this.reachForm2.valid) {
      this.submitForm2 = true;
      this.loading2 = true;
      this.danphecareservice.reachUsQuickly(this.DhcareRuq1)
        .subscribe(res => this.SuccessPostBook(res),
          res => this.Error1(res));
    }
    else {
      this.submitForm2 = true;
      this.loading2 = false;
      this.notifyService.showInfo("Info", "Please enter the required field");
      //this.reachForm2.reset();
      (this.submitForm2) ? setTimeout(() => { this.submitForm2 = false }, 5250) : null 
    }
      
    
  }
  SuccessPostBook(res) {
    this.loading2 = false;
    this.DhcareRuq1 = new DanpheCareReachUsQuicklyModel();
    this.notifyService.showSuccess('success', 'application submitted successfully ');
    this.submitForm2 = false;
    this.reachForm2.reset();
  }
  hide() {
    this.showModelBox = false;
    this.submitForm2 = false;
    this.loading2 = false;
    this.reachForm2.reset();
  }
  open() {
    this.showModelBox = true;
  }
  Error1(res) {
    const response = res;
    this.loading2 = false;
    this.notifyService.showError("Error", " Please fill up the required field")
  }

  onSubmitReachUsQuickly() {
    if (this.reachForm1.valid) {
      this.submitForm1 = true;
      this.loading1 = true;
      this.danphecareservice.reachUsQuickly(this.DhcareRuq)
        .subscribe(res => this.SuccessPostReachUsQuickly(res),
          res => this.Error1(res));
    }
    else {
      this.submitForm1 = true;
      this.loading1 = true;
      //this.reachForm1.reset();
      this.notifyService.showInfo("Info", "Please enter the required field");
      (this.submitForm1) ? setTimeout(() => { this.submitForm1 = false }, 5250) : null 
    }
  }
  SuccessPostReachUsQuickly(res) {
    this.loading1 = false;
    this.DhcareRuq = new DanpheCareReachUsQuicklyModel();
    this.notifyService.showSuccess('success', 'application submitted successfully ');
    this.submitForm1 = false;
    this.reachForm1.reset();
  }
  onSubmitReachUsQuickly1() {
    
    if (this.reachForm3.valid) {
      this.submitForm3 = true;
      this.loading3 = true;
      this.danphecareservice.reachUsQuickly(this.DhcareRuq2)
        .subscribe(res => this.SuccessPostReachUsQuickly1(res),
          res => this.Error1(res));
    }
    else {
      this.submitForm3 = true;
      this.loading3 = false;
      
      this.notifyService.showInfo("Info", "Please enter the required field");
      (this.submitForm3) ? setTimeout(() => { this.submitForm3 = false }, 5250) : null 
      //this.reachForm3.reset();
    }
  }
  SuccessPostReachUsQuickly1(res) {
    this.loading3 = false;
    this.DhcareRuq2 = new DanpheCareReachUsQuicklyModel();
    this.notifyService.showSuccess('success', 'application submitted successfully ');
    this.submitForm3 = false;
    this.reachForm3.reset();
  }



  ngOnDestroy() {
    this.meta.removeTag('property="og:type"');
  }
 
}
