

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AboutUsModel } from '../../cms/models/aboutUs.model';
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { MetaTag } from '../../cms/models/danphecare.cms.model';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-about-page',
  templateUrl: './aboutus.component.html'
})
export class AboutUsPageComponent implements OnInit, AfterViewInit {
  expert = require("../content/img/about-expert.png");
  public aboutUs: AboutUsModel = new AboutUsModel();
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  public content: string;
  constructor(public websiteService: WebsiteService, private notifyService: NotificationService, private titleService: Title, private metaService: Meta) {
    //
  }

  ngOnInit() {
    this.GetAboutUs();
   /* this.GetMetaTag();*/
    //this.loadScripts();

  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  loadScripts() {
    const dynamicScripts = [
      //'../content/js/jquery.min.js',
      './content/js/custom.js',
      './content/js/move-img-effect.js',
      './content/js/plugins.js',
      //'../content/js/popper.min.js',
      './content/js/slick.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'About Page');
   
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.updateTag({ name: 'keywords', content: `${this.tag.keywords}` });
      this.metaService.updateTag({ name: 'description', content: `${this.tag.content}` });
      //this.metaService.addTags([
      //  { name: 'keywords', content: `${this.tag.keywords}` },
      //  { name: 'description', content: `${this.tag.content}` },
      //  { name: 'robots', content: 'index, follow' }
      //]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }
  GetSocialCard() {
    this.content = "";
    this.content = this.aboutUs.content.replace(/<[^>]*>/g, '').trim();
    this.content = this.content.slice(0, 200);
    this.metaService.updateTag({ property: 'og:url', content: location.href });
    this.metaService.updateTag({ property: 'og:title', content: this.aboutUs.title });
    this.metaService.updateTag({ property: 'og:type', content: "About Us" });
    this.metaService.updateTag({ property: 'og:description', content: this.content + " ... " });
    this.metaService.updateTag({ property: 'og:image', content: this.aboutUs.imagePath });
    this.metaService.updateTag({ property: 'og:image:height', content: "200px" });
    this.metaService.updateTag({ property: 'og:image:width', content: "200px" });
    this.metaService.updateTag({ property: 'twitter:card', content: location.href });
    this.metaService.updateTag({ property: 'twitter:title', content: this.aboutUs.title });
    this.metaService.updateTag({ property: 'twitter:description', content: this.content + " ... " });
    this.metaService.updateTag({ property: 'twitter:image', content: this.aboutUs.imagePath });

  }
  GetAboutUs() {
    this.websiteService.GetAboutUs().subscribe(res => {
      if (res && res.length > 0) {
        this.aboutUs = Object.assign(this.aboutUs, res[0]);
        this.GetSocialCard();
       
      }
    },
      res => {
        //this.notifyService.showError("Error", "Internal Error")
      });
  }

}
