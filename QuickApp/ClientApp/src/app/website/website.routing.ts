import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { HomePageComponent } from "./home/homepage.component";
import { AboutUsPageComponent } from "./aboutus/aboutus.component";
import { BlogPageComponent } from "./blog/blog.component";
import { BlogDetailsPageComponent } from "./blog/blogdetails.component";
import { ContactPageComponent } from "./contact/contact.component";
import { ExpatPageComponent } from "./expat/expat.component";
import { NewsPageComponent } from "./news/news.component";
import { ProfilePageComponent } from "./profile/profile.component";
import { ServicesPageComponent } from "./services/services.component";
import { ServiceDetailsPageComponent } from "./services/servicedetails.component";
import { TeamPageComponent } from "./team/team.component";
import { NewsDetailsPageComponent } from "./news/newsdetails.component";
import { SpecialistPageComponent } from "./profile/specialist.component";
import { DepartmentDetailsPageComponent } from "./department/departmentdetails-page.component";
import { ArticlesPageComponent } from "./profile/articles-page.component";
import { CoreTeamProfileComponent } from "./team/core-team-profile.component";
import { TeamMember } from '../cms/models/danphecare.cms.model';
import { DepartmentDetailsListComponent } from './department/departmentdetails-list.component';



@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '', component: HomePageComponent,
        
      },
      { path: 'aboutpage', component: AboutUsPageComponent },
      { path: 'homepage', component: HomePageComponent },
      { path: 'blogpage', component: BlogPageComponent },
      { path: 'blog/:permalink', component: BlogDetailsPageComponent },
      { path: 'contactpage', component: ContactPageComponent },
      { path: 'expatpage', component: ExpatPageComponent },
      { path: 'newspage', component: NewsPageComponent },
      { path: 'newsdetails', component: NewsDetailsPageComponent },
      { path: 'profilepage/:permalink', component: ProfilePageComponent },
      { path: 'servicepage', component: ServicesPageComponent },
      { path: 'service/:permalink', component: ServiceDetailsPageComponent },
      { path: 'specialist', component: SpecialistPageComponent },
      { path: 'teampage', component: TeamPageComponent },
      { path: 'teamprofile', component: CoreTeamProfileComponent },
      { path: 'department/:permalink', component: DepartmentDetailsPageComponent },
      { path: 'articlespage', component: ArticlesPageComponent },
      { path: 'coreteamdetails', component: CoreTeamProfileComponent },
      { path: 'departmentdetailslist', component: DepartmentDetailsListComponent },
       //{ path: '**', redirectTo: '' },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class WebsiteRoutingModule {

}
