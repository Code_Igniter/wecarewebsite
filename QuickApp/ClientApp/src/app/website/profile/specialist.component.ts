

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DanpheCareDoctor, MetaTag } from '../../cms/models/danphecare.cms.model';
import { HttpClient } from '@angular/common/http';
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-specialist-page',
  templateUrl: './specialist.component.html'
})
export class SpecialistPageComponent implements OnInit, AfterViewInit {

  public specialistList: Array<DanpheCareDoctor> = new Array<DanpheCareDoctor>();
  public specialistId: number;
  public departmentId: number;
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  constructor(public http: HttpClient, public websiteService: WebsiteService, public notifyService: NotificationService, public router: Router, private titleService: Title, private metaService: Meta) {
    //
  }

  ngOnInit() {
    this.GetSpecialist();
   /* this.GetMetaTag();*/
    //this.loadScripts();

  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  loadScripts() {
    const dynamicScripts = [
      //'../content/js/jquery.min.js',
      './content/js/custom.js',
      './content/js/move-img-effect.js',
      './content/js/plugins.js',
      //'../content/js/popper.min.js',
      './content/js/slick.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Specialist Page');

      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.addTags([
        { name: 'keywords', content: `${this.tag.keywords}` },
        { name: 'description', content: `${this.tag.content}` },
        { name: 'robots', content: 'index, follow' }
      ]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }

  GetSpecialist() {
    this.websiteService.GetDoctorSorting().subscribe(res => {
      if (res) {
        this.specialistList = [];
        this.specialistList = Object.assign(this.specialistList, res);
        const doctor = this.specialistList.sort(x => x.sorting).filter(y => y.sorting !== 0);
        const doctorwithzero = this.specialistList.filter(y => y.sorting === 0);
        this.specialistList = doctor;
        for (const item of doctorwithzero) {
          this.specialistList.push(item);
        }
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Specialist Found!");
      });
  }
  //GoToSpecialistDetails(id,did) {
  //  this.specialistId = id;
  //  this.departmentId = did
  //  this.router.navigate(['/profilepage'], { queryParams: { id: this.specialistId, did: this.departmentId } });
  //}
  GoToSpecialistDetails(permalink) {

    this.router.navigate(['/profilepage', permalink]);
  }
}
