

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { ResourcefulArticles, DepartmentConsultation, DanpheCareDoctor, DepartmentSubHeading, Blogs, MetaTag } from '../../cms/models/danphecare.cms.model';
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DanphecareService } from '../../services/danphecare/danphe.service';
import { DanpheCareReachUsQuicklyModel } from '../../models/danphecare/danphecare.model';


@Component({
  selector: 'app-profile-page',
  templateUrl: './profile.component.html'
})
export class ProfilePageComponent implements OnInit, AfterViewInit {
  public profiledetailId: string;
  public departmentdetailId: string;
  public profileId: number;
  public profile: DanpheCareDoctor = new DanpheCareDoctor();
  public articlesList: Array<ResourcefulArticles> = new Array<ResourcefulArticles>();
  public articlesId: number;
  public departmentList: Array<DepartmentConsultation> = new Array<DepartmentConsultation>();
  public departmentId: any;
  articleId: any;
  public blogsList: Array<Blogs> = new Array<Blogs>();
  public blogId: number;
  public IsSubHeading = true;
  public subheadingList: Array<DepartmentSubHeading> = new Array<DepartmentSubHeading>();
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  public reachForm2: FormGroup;
  public submitForm2: boolean = false;
  public loading2: boolean = false;
  public showModelBox: boolean = false;
  public DhcareRuq1: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  public content: string;
  public permaLink: any;
  //expert = require("../content/img/about-expert.png");
  constructor(public routing: Router, private route: ActivatedRoute, public websiteService: WebsiteService, private notifyService: NotificationService, private titleService: Title, private metaService: Meta,
    public formBuilder: FormBuilder, public danphecareservice: DanphecareService) {
    //this.profiledetailId = this.route.snapshot.queryParamMap.get('id');
    //this.departmentdetailId = this.route.snapshot.queryParamMap.get('did');
    //this.profileId = parseInt(this.profiledetailId, 10);
    this.route.params.subscribe((params: Params) => {
      this.permaLink = params.permalink;
    });
  }

  ngOnInit() {
    //this.profileId = parseInt(this.profiledetailId, 10);
    //this.departmentId = parseInt(this.departmentdetailId, 10);
    this.GetProfile(this.permaLink);
    this.GetBlogs();
    this.GetArticles();
    this.GetDepartment();
    this.GetDepartmentSubHeading();
    /*this.GetMetaTag();*/
    this.reachForm2 = this.formBuilder.group({
      phonenumber2: ['', [Validators.required]],
      name2: ['', [Validators.required]]
    });
    //this.loadScripts();

  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  loadScripts() {
    const dynamicScripts = [
      //'../content/js/jquery.min.js',
      './content/js/custom.js',
      './content/js/move-img-effect.js',
      './content/js/plugins.js',
      //'../content/js/popper.min.js',
      './content/js/slick.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Specialist Profile Page');
   
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.addTags([
        { name: 'keywords', content: `${this.tag.keywords}` },
        { name: 'description', content: `${this.tag.content}` },
        { name: 'robots', content: 'index, follow' }
      ]);
    }
   
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }

  GetSocialCard() {
    //this.metaService.addTags([]);
    this.content = "";
    this.content = this.profile.content.replace(/<[^>]*>/g, '').trim();
    this.content = this.content.slice(0, 200);
    this.metaService.updateTag({ property: 'fb:app_id', content: "260103905625004" });
    this.metaService.updateTag({ property: 'og:url', content: location.href });
    this.metaService.updateTag({ property: 'og:title', content: this.profile.fullName });
    this.metaService.updateTag({ property: 'og:type', content: "Profile"  });
    this.metaService.updateTag({ property: 'og:description', content: this.content + " ... " });
    this.metaService.updateTag({ property: 'og:image', content: this.profile.imagePath });
    this.metaService.updateTag({ property: 'og:image:height', content: "200px" });
    this.metaService.updateTag({ property: 'og:image:width', content: "200px"  });
    this.metaService.updateTag({ property: 'twitter:card', content: location.href });
    this.metaService.updateTag({ property: 'twitter:title', content: this.profile.fullName });
    this.metaService.updateTag({ property: 'twitter:description', content: this.content + " ... "  });
    this.metaService.updateTag({ property: 'twitter:image', content: this.profile.imagePath  });
   
  }

  GetProfile(permalink) {
    this.websiteService.GetDoctorById(permalink)
      .subscribe(res => this.SuccessProfile(res),
        res => this.Error(res));
  }
  SuccessProfile(res) {
    this.profile = res;
    this.GetSocialCard();
  }
  Error(res) {
    //this.notifyService.showError("Info", "No Profile Found!");
  }

  GetArticles() {
    this.websiteService.GetArticles()
      .subscribe(res => this.SuccessArticles(res),
        res => this.ErrorArticles(res));
  }
  SuccessArticles(res) {
    this.articlesList = res;
  }
  ErrorArticles(res) {
    //this.notifyService.showError("Info", "No Articles Found!");
  }
  GetDepartmentSubHeading() {
    this.websiteService.GetSubHeadingByDoctorId(this.permaLink)
      .subscribe(res => this.SuccessDepartmentSubHeading(res),
        res => this.ErrorDepartmentSubHeading(res));
  }
  SuccessDepartmentSubHeading(res) {
    this.subheadingList = res;
    if (res.length === 0) {
      this.IsSubHeading = false;
    }
  }
  ErrorDepartmentSubHeading(res) {
    //this.notifyService.showError("Info", "No Department SubHeading Found!");
  }

  GetDepartment() {
    this.websiteService.GetDepConsultation()
      .subscribe(res => this.SuccessDepartment(res),
        res => this.ErrorDepartment(res));
  }
  SuccessDepartment(res) {
    this.departmentList = res;
  }
  ErrorDepartment(res) {
    //this.notifyService.showError("Info", "No Department Found!");
  }

  GoToArticle(id) {
    this.articleId = id;
    this.routing.navigate(['/articlespage'], { queryParams: { id: this.articleId } });


  }
  GetBlogs() {
    this.websiteService.GetBlogs().subscribe(res => {
      if (res) {
        this.blogsList = [];
        this.blogsList = Object.assign(this.blogsList, res);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Blogs Found!");
      });
  }
  GoToBlogDetails(permaLink) {
    //this.blogId = id;
    this.routing.navigate(['/blog', permaLink]);


  }

  onSubmitBook() {
    if (this.reachForm2.valid) {
      this.submitForm2 = true;
      this.loading2 = true;
      this.danphecareservice.reachUsQuickly(this.DhcareRuq1)
        .subscribe(res => this.SuccessPostBook(res),
          res => this.Error1(res));
    }
    else {
      this.submitForm2 = true;
      this.loading2 = false;
      this.notifyService.showInfo("Info", "Please enter the required field");
      //this.reachForm2.reset();
      (this.submitForm2) ? setTimeout(() => { this.submitForm2 = false }, 5250) : null
    }


  }
  SuccessPostBook(res) {
    this.loading2 = false;
    this.DhcareRuq1 = new DanpheCareReachUsQuicklyModel();
    this.notifyService.showSuccess('success', 'application submitted successfully ');
    this.submitForm2 = false;
    this.reachForm2.reset();
  }
  hide() {
    this.showModelBox = false;
    this.submitForm2 = false;
    this.loading2 = false;
    this.reachForm2.reset();
  }
  open() {
    this.showModelBox = true;
  }
  Error1(res) {
    const response = res;
    this.loading2 = false;
    this.notifyService.showError("Error", " Please fill up the required field")
  }
  get NameControl2() {
    return this.reachForm2.get("name2") as FormControl;
  }
  get PhoneNumberControl2() {
    return this.reachForm2.get("phonenumber2") as FormControl;
  }

}
