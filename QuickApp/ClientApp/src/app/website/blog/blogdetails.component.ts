

import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NotificationService } from '../../services/notification.service';
import { WebsiteService } from '../websiteservice/website.service';
import { Blogs, MetaTag } from '../../cms/models/danphecare.cms.model';
import { Title, Meta } from '@angular/platform-browser';
import { MetaService } from '@ngx-meta/core';


@Component({
  selector: 'app-blog-detail-page',
  templateUrl: './blogdetails.component.html'
})

export class BlogDetailsPageComponent implements OnInit, AfterViewInit, OnDestroy {

  public blogdetailId: any;
  public blogId: number;
  public blog: Blogs = new Blogs();
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  public content: string;
  public permaLink: any;
  public url: any;
  public description: string;
  public image: string;
  constructor(public routing: Router, private route: ActivatedRoute, public websiteService: WebsiteService, private notifyService: NotificationService, private titleService: Title, private metaService: Meta, private readonly meta: MetaService) {
    this.route.params.subscribe((params: Params) => {
      this.permaLink = params.permalink; 
    });
    //this.permaLink = this.route.snapshot.firstChild.data;
    //this.blogdetailId = this.route.snapshot.queryParamMap.get('id');
    //this.blogId = parseInt(this.blogdetailId, 10);
  }

  ngOnInit() {
    //this.blogId = parseInt(this.blogdetailId, 10);
    this.GetBlogs(this.permaLink);
    //this.loadScripts();
    /*this.GetMetaTag();*/

  }
  ngAfterViewInit() {
    //this.loadScripts();
    //this.blogId = parseInt(this.blogdetailId, 10);
    //this.GetBlogs(this.blogId);
  }

  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Blog Details Page');
   
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.updateTag({ name: 'keywords', content: `${this.tag.keywords}` });
      this.metaService.updateTag({ name: 'description', content: `${this.tag.content}` });
     
      //this.metaService.addTags([
      //  { name: 'keywords', content: `${this.tag.keywords}` },
      //  { name: 'description', content: `${this.tag.content}` },
      //  { name: 'robots', content: 'index, follow' }
      //]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }
  GetSocialCard() {
    const imgSubstr = this.blog.imagePath.substr(23);
    const imageUrl = "https://danphecare.com/Upload/DanpheCareCMS" + imgSubstr;
    
    this.content = "";
    this.content = this.blog.content.replace(/<[^>]*>/g, '').trim();
    this.content = this.content.slice(0, 200);
    this.url = location.href;
    this.description = this.content + "...";
  /*  this.image = imageUrl;*/
    this.meta.setTitle(this.blog.title);
    this.meta.setTag('og:image', imageUrl);
    this.meta.setTag('og:url', location.href);
    this.meta.setTag('og:type', "Blogs");
    this.meta.setTag('og:description', this.content + " ... " );
    this.meta.setTag('twitter:card', location.href);
    this.meta.setTag('twitter:title', this.blog.title);
    this.meta.setTag('twitter:description', this.content + " ... ");
    this.meta.setTag('twitter:image', imageUrl);
    this.meta.setTag('og:app_id', "260103905625004");
    //this.metaService.updateTag({ property: 'fb:app_id', content: "260103905625004" });
    //this.metaService.updateTag({ property: 'og:url', content: location.href });
    //this.metaService.updateTag({ property: 'og:title', content: this.blog.title });
    //this.metaService.updateTag({ property: 'og:type', content: "Blogs" });
    //this.metaService.updateTag({ property: 'og:description', content: this.content + " ... " });
    //this.metaService.updateTag({ property: 'og:image', content: imageUrl });
    //this.metaService.updateTag({ property: 'og:image:height', content: "150" });
    //this.metaService.updateTag({ property: 'og:image:width', content: "150" });
    //this.metaService.updateTag({ property: 'twitter:card', content: location.href });
    //this.metaService.updateTag({ property: 'twitter:title', content: this.blog.title });
    //this.metaService.updateTag({ property: 'twitter:description', content: this.content + " ... " });
    //this.metaService.updateTag({ property: 'twitter:image', content: imageUrl });

  }
  GetBlogs(permaLink) {
    this.websiteService.GetBlogsById(permaLink)
      .subscribe(res => this.SuccessBlogs(res),
        res => this.Error(res));
    //this.cmsService.GetBlogsById(id).subscribe(res => {
    //  if (res) {
    //    this.blog = Object.assign(this.blog, res);
    //  }
    //},
    //  res => {
    //    this.notifyService.showError("Info", "No Blog Found!");
    //  });
  }
  SuccessBlogs(res) {
    this.blog = res;
    this.GetSocialCard();
  }
  Error(res) {
    //this.notifyService.showError("Info", "No Blog Found!");
  }

  loadScripts() {
    const dynamicScripts = [
      //'../content/js/jquery.min.js',
      './content/js/custom.js',
      './content/js/move-img-effect.js',
      './content/js/plugins.js',
      //'../content/js/popper.min.js',
      './content/js/slick.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  ngOnDestroy() {
    this.meta.removeTag('property="og:type"');
  }
}
