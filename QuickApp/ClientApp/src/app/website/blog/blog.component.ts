

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../services/notification.service';
import { Blogs, MetaTag } from '../../cms/models/danphecare.cms.model';
import { Router } from '@angular/router';
import { WebsiteService } from '../websiteservice/website.service';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-blog-page',
  templateUrl: './blog.component.html'
})
export class BlogPageComponent implements OnInit, AfterViewInit {
  public blogsList: Array<Blogs> = new Array<Blogs>();
  public blogId: number;
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  constructor(public http: HttpClient, public websiteService: WebsiteService, public notifyService: NotificationService, public router: Router, private titleService: Title, private metaService: Meta) {
    //
  }

  ngOnInit() {
    this.GetBlogs();
   /* this.GetMetaTag();*/
    //this.loadScripts();

  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  loadScripts() {
    const dynamicScripts = [
      //'../content/js/jquery.min.js',
      './content/js/custom.js',
      './content/js/move-img-effect.js',
      './content/js/plugins.js',
      //'../content/js/popper.min.js',
      './content/js/slick.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Blog Page');
   
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.updateTag({ name: 'keywords', content: `${this.tag.keywords}` });
      this.metaService.updateTag({ name: 'description', content: `${this.tag.content}` });
      //this.metaService.addTags([
      //  { name: 'keywords', content: `${this.tag.keywords}` },
      //  { name: 'description', content: `${this.tag.content}` },
      //  { name: 'robots', content: 'index, follow' }
      //]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }


  GetBlogs() {
    this.websiteService.GetBlogs().subscribe(res => {
      if (res) {
        this.blogsList = [];
        this.blogsList = Object.assign(this.blogsList, res);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Blogs Found!");
      });
  }
  GoToBlogDetails(permaLink) {
    this.router.navigate(['/blog', permaLink]); 
  }
}
