

import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { OurServiceModel } from '../../cms/models/OurServices.model';
import { MetaTag } from '../../cms/models/danphecare.cms.model';
import { Title, Meta } from '@angular/platform-browser';
import { DanpheCareReachUsQuicklyModel } from '../../models/danphecare/danphecare.model';
import { DanphecareService } from '../../services/danphecare/danphe.service';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { MetaService } from '@ngx-meta/core';

@Component({
  selector: 'service-details-page',
  templateUrl: './servicedetails.component.html'
})
export class ServiceDetailsPageComponent implements OnInit, AfterViewInit, OnDestroy {
  public servicesdetailId: string;
  public servicesId: number;
  public services: OurServiceModel = new OurServiceModel();
  serviceimg = require("../content/img/expat-about.png");
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public DhcareRuq: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  public tag: MetaTag = new MetaTag();
  public selServices: any;
  public submitted: boolean = false;
  public reachForm: FormGroup;
  public loading: boolean = false;
  public isCompany: boolean = false;
  filteredPage = [
    { id: 1, title: "Kathmandu Services" },
    { id: 2, title: "Lalitpur Services" }
  ];
  public permaLink: any;
  public content: string;
  constructor(public routing: Router, private route: ActivatedRoute, public formBuilder: FormBuilder,
    public websiteService: WebsiteService, private notifyService: NotificationService, private titleService: Title,
    private metaService: Meta, public danphecareservice: DanphecareService, private readonly meta: MetaService) {
    //this.servicesdetailId = this.route.snapshot.queryParamMap.get('id');
    //this.servicesId = parseInt(this.servicesdetailId, 10);
    this.route.params.subscribe((params: Params) => {
      this.permaLink = params.permalink;
    });
  }

  ngOnInit() {
    //this.servicesId = parseInt(this.servicesdetailId, 10);
    //this.GetServices(this.servicesId);
    this.GetServices(this.permaLink);
  /*  this.GetMetaTag();*/
    this.routing.routeReuseStrategy.shouldReuseRoute = () => false;
    this.reachForm = this.formBuilder.group({
      phonenumber: ['', [Validators.required]],
      name: ['', [Validators.required]]
    });
    this.selServices = this.filteredPage[0].title;

  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  //loadScripts() {
  //  const dynamicScripts = [
  //    //'../content/js/jquery.min.js',
  //    './content/js/custom.js',
  //    './content/js/move-img-effect.js',
  //    './content/js/plugins.js',
  //    //'../content/js/popper.min.js',
  //    './content/js/slick.min.js'
  //  ];
  //  for (let i = 0; i < dynamicScripts.length; i++) {
  //    const node = document.createElement('script');
  //    node.src = dynamicScripts[i];
  //    node.type = 'text/javascript';
  //    node.async = false;
  //    node.charset = 'utf-8';
  //    document.getElementsByTagName('head')[0].appendChild(node);
  //  }
  //}
  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
      this.metaTag = res.filter(x => x.page === 'Services Details Page');

      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.addTags([
        { name: 'keywords', content: `${this.tag.keywords}` },
        { name: 'description', content: `${this.tag.content}` },
        { name: 'robots', content: 'index, follow' }
      ]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }

  GetSocialCard() {
    this.content = "";
    this.content = this.services.content.replace(/<[^>]*>/g, '').trim();
    this.content = this.content.slice(0, 200);
    const imgSubstr = this.services.iconPath.substr(23);
    const imageUrl = "https://danphecare.com/Upload/DanpheCareCMS" + imgSubstr;
    this.meta.setTitle(this.services.serviceName);
    this.meta.setTag('og:image', imageUrl);
    this.meta.setTag('og:url', location.href);
    this.meta.setTag('og:type', "Services");
    this.meta.setTag('og:description', this.content + " ... ");
    this.meta.setTag('twitter:card', location.href);
    this.meta.setTag('twitter:title', this.services.serviceName);
    this.meta.setTag('twitter:description', this.content + " ... ");
    this.meta.setTag('twitter:image', imageUrl);
    this.meta.setTag('og:app_id', "260103905625004");
    //this.metaService.updateTag({ property: 'fb:app_id', content: "260103905625004" });
    //this.metaService.updateTag({ property: 'og:url', content: location.href });
    //this.metaService.updateTag({ property: 'og:title', content: this.services.serviceName });
    //this.metaService.updateTag({ property: 'og:type', content: "Blogs" });
    //this.metaService.updateTag({ property: 'og:description', content: this.content + " ... " });
    //this.metaService.updateTag({ property: 'og:image', content: imgSubstr });
    //this.metaService.updateTag({ property: 'og:image:height', content: "150" });
    //this.metaService.updateTag({ property: 'og:image:width', content: "150" });
    //this.metaService.updateTag({ property: 'twitter:card', content: location.href });
    //this.metaService.updateTag({ property: 'twitter:title', content: this.services.serviceName });
    //this.metaService.updateTag({ property: 'twitter:description', content: this.content + " ... " });
    //this.metaService.updateTag({ property: 'twitter:image', content: imgSubstr });

  }

  GetServices(permaLink) {
    this.websiteService.GetServicesById(permaLink)
      .subscribe(res => this.SuccessServices(res),
        res => this.Error(res));
  }
  SuccessServices(res) {
    this.services = res;
    this.GetSocialCard();
  }
  Error(res) {
    //this.notifyService.showError("Info", "No Services Found!");
  }
  onSubmitReachUsQuickly() {
    this.submitted = true;
    if (this.DhcareRuq.company == null) {
      this.isCompany = true;
      this.reachForm.invalid;
    }
    if (this.reachForm.valid) {
      this.submitted = true;
      this.DhcareRuq.company = this.selServices.title;
      this.loading = true;
      this.danphecareservice.reachUsQuickly(this.DhcareRuq)
        .subscribe(res => this.SuccessPostReachUsQuickly(res),
          res => this.Error1(res));

    }
    else {

      this.submitted == true;
      this.loading == false;
      this.notifyService.showInfo("Info", "Please enter the required field");
      (this.submitted) ? setTimeout(() => { this.submitted = false }, 5230) : null 
    }
    
  }
  SuccessPostReachUsQuickly(res) {
    this.loading = false;
    this.DhcareRuq = new DanpheCareReachUsQuicklyModel();
    this.notifyService.showSuccess('success', 'application submitted successfully ');
    this.submitted = false;
    this.reachForm.reset();
  }

  Error1(res) {
    this.loading = false;
    const response = res;
    this.notifyService.showError("Error", " Please fill up the required field")
    //this.reachForm.reset();
  }
  get PhoneNumberControl() {
    return this.reachForm.get("phonenumber") as FormControl;
  }
  
  get NameControl() {
    return this.reachForm.get("name") as FormControl;
  }

  ngOnDestroy() {
    this.meta.removeTag('property="og:type"');
  }


}
