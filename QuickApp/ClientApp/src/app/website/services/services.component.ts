

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { OurServiceModel } from '../../cms/models/OurServices.model';
import { HttpClient } from '@angular/common/http';
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { Router } from '@angular/router';
import { MetaTag } from '../../cms/models/danphecare.cms.model';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-services-page',
  templateUrl: './services.component.html'
})
export class ServicesPageComponent implements OnInit, AfterViewInit {
  public servicesList: Array<OurServiceModel> = new Array<OurServiceModel>();
  public servicesId: number;
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  constructor(public http: HttpClient, public websiteService: WebsiteService, public notifyService: NotificationService, public router: Router, private titleService: Title, private metaService: Meta) {
    //
  }

  ngOnInit() {
    this.GetServices();
    //this.loadScripts();
   /* this.GetMetaTag();*/

  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  loadScripts() {
    const dynamicScripts = [
      //'../content/js/jquery.min.js',
      './content/js/custom.js',
      './content/js/move-img-effect.js',
      './content/js/plugins.js',
      //'../content/js/popper.min.js',
      './content/js/slick.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Services List Page');
 
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.addTags([
        { name: 'keywords', content: `${this.tag.keywords}` },
        { name: 'description', content: `${this.tag.content}` },
        { name: 'robots', content: 'index, follow' }
      ]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }

  GetServices() {
    this.websiteService.GetOurServices().subscribe(res => {
      if (res) {
        this.servicesList = [];
        this.servicesList = Object.assign(this.servicesList, res);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Services Found!");
      });
  }

  GoToServiceDetails(permaLink) {
    this.router.navigate(['/service', permaLink]);


  }

}
