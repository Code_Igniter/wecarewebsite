

import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { OurServiceModel } from '../../cms/models/OurServices.model';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../services/notification.service';
import { WebsiteService } from '../websiteservice/website.service';
import { DanpheCareDepartment, MetaTag } from '../../cms/models/danphecare.cms.model';

import { MediaCoverage, TeamMember, Testimonial, TestimonialMain, DanpheCareContact } from '../../cms/models/danphecare.cms.model';
import { DanphecareModel, DanpheCareReachUsQuicklyModel } from '../../models/danphecare/danphecare.model';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl } from '@angular/forms';
import { AlertService } from '../../services/alert.service';
import { Global } from '../../app.global';
import { DanphecareService } from '../../services/danphecare/danphe.service';
//import { AuthenticationService } from '../../services/authentication.service';
import { ConfigService } from '../../../common/common.appconfig';
import { Router } from '@angular/router';
import * as moment from 'moment';
//import { NgxCaptchaModule, ReCaptcha2Component } from 'ngx-captcha';
import { ScriptService } from '../scriptservice/script.service';
import { AuthService } from '../../services/auth.service';
import { Title, Meta } from '@angular/platform-browser';
import { MetaService } from '@ngx-meta/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './homepage.component.html',
  styleUrls: ["./homepage.component.css"]
})
export class HomePageComponent implements OnInit, AfterViewInit{

  public Dhcare: DanphecareModel = new DanphecareModel();

  public DhcareRuq: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  homeIsolationApplyForm: FormGroup;
  reachUsQuicklyForm: FormGroup;
  grecaptcha: any;
  loginVal: any;
  dataFromAppsettings: any;
  siteKey: string;
  siteKey_ruq: string;
  captchaRes: any[];
  public loading: boolean = false;
  loading_ruq = false;
  public emlError = false;
  public phnError = false;
  public nameErrror = false;
  public contact: DanpheCareContact = new DanpheCareContact();
  public showPaymentMethod: boolean = false;
  public showMoreContent: boolean = false;
  public submitted: boolean = false;
  public submitted_ruq: boolean = false;
  public reCAPTCHAstatus: boolean = false;

  public Usrlist: Array<any> = new Array<any>();
  banner = require("../content/img/banner.png");
  step1 = require("../content/img/step1.png");
  step2 = require("../content/img/step2.png");
  step3 = require("../content/img/step3.png");
  step4 = require("../content/img/step4.png");
  team1 = require("../content/img/team/team-1.jpg");
  team2 = require("../content/img/team/team-2.jpg");
  team3 = require("../content/img/team/team-3.jpg");
  team4 = require("../content/img/team/team-4.jpg");
  loadAPI: Promise<any>;
  star: number;
  registerForm: FormGroup;
  submitForm: boolean = false;
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  public content: string;
  icon = require("../../../assets/images/danphecarelogo.png");
  public ourServicesList: Array<OurServiceModel> = new Array<OurServiceModel>();
    public departmentList: Array<DanpheCareDepartment> = new Array<DanpheCareDepartment>();
    ourTeamMemberList: TeamMember[];
    coreTeamMemberList: TeamMember[];
    ourMediaCoverageList: MediaCoverage[];
    testimonialList: Array<Testimonial>= new Array<Testimonial>();
    testimonialMain: TestimonialMain = new TestimonialMain();
  constructor(public http: HttpClient, public websiteService: WebsiteService, public notifyService: NotificationService,
    private formBuilder: FormBuilder, public danphecareservice: DanphecareService, private alertService: AlertService, public scriptService: ScriptService,
    private authenticationService: AuthService, private configService: ConfigService, public global: Global,
    public routing: Router, private titleService: Title, private metaService: Meta, private readonly meta: MetaService) {
    this.GetContact();
    this.siteKey = this.global.config.SiteKey;

  }

  ngOnInit() {
    this.GetOurServices();
    this.GetDepartment();
    this.GetOurTeamMember();
    this.GetOurMediaCoverage();
    this.GetTestimonialMain();
    this.GetTestimonials();
   /* this.GetMetaTag();*/
   /* this.GetSocialCard();*/
   
    //this.GetContact();
    //this.scripts();
    //this.loadScripts();

    this.CaptchaCallback();
  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  scripts() {
    this.scriptService.load('slick', 'custom', 'moveimg', 'plugins', 'popper').then(data => {
      console.log('script loaded ', data);
    }).catch(error => console.log(error));

  }


  loadScripts() {
    const dynamicScripts = [
      '../../assets/scripts/jquery.min.js',
      '../../assets/scripts/bootstrap.min.js',
      '../../assets/scripts/slick.min.js',
      '../../assets/scripts/custom.js',
      '../../assets/scripts/move-img-effect.js',
      '../../assets/scripts/plugins.js',
      '../../assets/scripts/popper.min.js'
    ];



    for (let i = 0; i < dynamicScripts.length; i++) {
      console.log('preparing to load...')
      let node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = true;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
  GetOurServices() {
    this.websiteService.GetOurServices().subscribe(res => {
      if (res) {
        this.ourServicesList = [];
        this.ourServicesList = res.filter((dep, idx) => idx < 3);
        //this.ourServicesList = Object.assign(this.ourServicesList, res);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Services Found!");
      });
  }

  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Home Page');
  
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.updateTag({ name: 'keywords', content: `${this.tag.keywords}` });
      this.metaService.updateTag({ name: 'description', content: `${this.tag.content}` });
      //this.metaService.addTags([
      //  { name: 'keywords', content: `${this.tag.keywords}` },
      //  { name: 'description', content: `${this.tag.content}` },
      //  { name: 'robots', content: 'index, follow' }
      //]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }

  GetSocialCard() {
    this.content = "";
    this.content = "Our dedicated team of doctors, nurses and other health professionals provide compassionate care with a personal touch.";
    this.meta.setTitle("Committed for your health and wellbeing");
  /*  this.meta.setTag('og:image', imageUrl);*/
    this.meta.setTag('og:url', location.href);
    this.meta.setTag('og:type', "Home");
    this.meta.setTag('og:description', this.content + " ... ");
    this.meta.setTag('twitter:card', location.href);
    this.meta.setTag('twitter:title', "Committed for your health and wellbeing");
    this.meta.setTag('twitter:description', this.content + " ... ");
/*    this.meta.setTag('twitter:image', imageUrl);*/
    this.meta.setTag('og:app_id', "260103905625004");

    //this.metaService.updateTag({ property: 'fb:app_id', content: "260103905625004" });
    //this.metaService.updateTag({ property: 'og:url', content: location.href });
    //this.metaService.updateTag({ property: 'og:title', content: "Committed for your health and wellbeing" });
    //this.metaService.updateTag({ property: 'og:type', content: "Home" });
    //this.metaService.updateTag({ property: 'og:description', content: this.content + " ... " });
    //this.metaService.updateTag({ property: 'og:image', content: "../../../assets/images/danphecarelogo.png" });
    //this.metaService.updateTag({ property: 'og:image:height', content: "200px" });
    //this.metaService.updateTag({ property: 'og:image:width', content: "200px" });
    //this.metaService.updateTag({ property: 'twitter:card', content: location.href });
    //this.metaService.updateTag({ property: 'twitter:title', content: "Committed for your health and wellbeing" });
    //this.metaService.updateTag({ property: 'twitter:description', content: this.content + " ... " });
    //this.metaService.updateTag({ property: 'twitter:image', content: "../../../assets/images/danphecarelogo.png" });

  }

  GetDepartment() {
    this.websiteService.GetDepartment().subscribe(res => {
      if (res) {
        this.departmentList = [];
        this.departmentList = res.filter((dep, idx) => idx < 4);
        //this.departmentList = Object.assign(this.departmentList, res);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Department Found!");
      });
    }
  get PhoneNumberControl() {
    return this.registerForm.get("phonenumber") as FormControl;
  }
  get EmailControl() {
    return this.registerForm.get("email") as FormControl;
  }
  get NameControl() {
    return this.registerForm.get("name") as FormControl;
  }

  GetOurTeamMember() {
    this.websiteService.GetCoreTeamMembers().subscribe(res => {
      if (res) {
        this.ourTeamMemberList = [];
        this.ourTeamMemberList = Object.assign(this.ourTeamMemberList, res);
        this.coreTeamMemberList = this.ourTeamMemberList.filter(t=>t.isCoreTeam==true);
      }
    },
      res => {
        console.log("Error Message: " + res);
        // this.notifyService.showError("Info", "No Services Found!");
      });
  }

  GetOurMediaCoverage() {
    this.websiteService.GetOurMediaCoverage().subscribe(res => {
      if (res) {
        this.ourMediaCoverageList = [];
        this.ourMediaCoverageList = Object.assign(this.ourMediaCoverageList, res);
      }
    },
      res => {
        console.log("Error Message: " + res);
        // this.notifyService.showError("Info", "No Services Found!");
      });
  }

  GetTestimonialMain() {
    this.websiteService.GetTestimonialMain().subscribe(res => {
      if (res) {
        let testimonialMain = [];
        this.testimonialMain = Object.assign(testimonialMain, res[0]);
      }
    },
      res => {
        console.log("Error Message: " + res);
        // this.notifyService.showError("Info", "No Services Found!");
      });
  }

  GetTestimonials() {
    this.websiteService.GetTestimonials().subscribe(res => {
      if (res) {
        this.testimonialList = [];
        this.testimonialList = Object.assign(this.testimonialList, res);
        this.testimonialList.forEach(t=>{
          let starString = this.GenerateStar(t.star);
          t.starString = starString;
          this.star = Number(t.star);
        })
      }
    },
      res => {
        console.log("Error Message: " + res);
        // this.notifyService.showError("Info", "No Services Found!");
      });
  }

  GenerateStar(StarCount):string{
    let stringStar: string;
    switch (StarCount){
      case 1 :{
        stringStar =  `
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        `;
        break;
      }
      case 2:{
        stringStar =  `
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        `;
        break;

      }
      case 3:{
        stringStar =  `
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        `;
        break;

      }
      case 4:{
        stringStar =  `
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star-o" data-jest="empty star"></span>
        `;
        break;

      }
      case 5:{
        stringStar =  `
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        `;
        break;
      }
      default:{
        stringStar =  `
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        <span class="fa fa-star" data-jest="empty star"></span>
        `;
        break;
      }
    }
    return stringStar;
  }


  get f() {
    return this.homeIsolationApplyForm.controls;
  }
  get g() {
    return this.reachUsQuicklyForm.controls;
  }

  GetAppliedUsrlist() {
    this.danphecareservice.GetAppliedUsrlist()
      .subscribe(res => this.SuccessGetAppliedUsrlist(res),
        res => this.GetAppliedUsrlistError(res));

  }

  SuccessGetAppliedUsrlist(res) {
    this.Usrlist = res;
    for (var i = 0; i < this.Usrlist.length; i++) {
      this.Usrlist[i].CreatedOn = moment(this.Usrlist[i].CreatedOn).format("YYYY-MM-DD");
    }
    //this.notifyService.showSuccess('Success', ' User list Load');
  }
  GetAppliedUsrlistError(res) {
    //this.notifyService.showError("Error", " field to load User list")
  }

  onSubmit() {
    // this.CaptchaCallback();
    this.submitted = true;
    this.loading = true;
    if (this.homeIsolationApplyForm.invalid) {
      //this.submitted = false;
      this.loading = false;
      this.reCAPTCHAstatus == false;
      //this.notifyService.showError("Error", " Please fill up the form properly")
    }
    if (this.homeIsolationApplyForm.valid) {
      if (this.reCAPTCHAstatus == true) {
        if (this.Dhcare != null) {
          this.danphecareservice.applytoIsolation(this.Dhcare)
            .subscribe(res => this.SuccessPostapplyIso(res),
              res => this.Error(res));
        }
      } else {
        //this.notifyService.showError("Error", "Human verification failed.");
        this.loading = false;
      }
    }
    //if (this.homeIsolationApplyForm.value.name.invalid) {
    //  this.notifyService.showError("", "Name is required");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.phonenumber.invalid) {
    //  this.notifyService.showError("", "Mobile number is required");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.phonenumber.invalid) {
    //  this.notifyService.showError("", "Mobile number must be 10 digits");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.email.invalid) {
    //  this.notifyService.showError("", "Improper Email Format");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.email.invalid) {
    //  this.notifyService.showError("", "Email is required");
    //  this.loading = false;
    //}
    //if (this.homeIsolationApplyForm.value.message.invalid) {
    //  this.notifyService.showError("", "Message is required");
    //  this.loading = false;
    //}
  }
  onSubmitReachUsQuickly() {
    if (this.registerForm.valid) {
      this.submitForm = true;
      this.loading = true;
      this.danphecareservice.reachUsQuickly(this.DhcareRuq)
        .subscribe(res => this.SuccessPostReachUsQuickly(res),
          res => this.Error(res));
    }
    else {
      this.submitForm = true;
      this.loading = false;
      this.notifyService.showInfo("Info", "Please enter the required field");
      //this.registerForm.reset();
      
      (this.submitForm) ? setTimeout(() => { this.submitForm = false }, 5250) : null 

    }
      
      
    
    
  }
  SuccessPostapplyIso(res) {
    this.submitForm = true;
    this.homeIsolationApplyForm.reset();
    // this.GetAppliedUsrlist();
    this.notifyService.showSuccess('success', 'application submitted successfully');
    this.reCAPTCHAstatus = false;
    this.loading = false;
    this.submitForm = false;

  }
  SuccessPostReachUsQuickly(res) {
    this.loading = false;
    this.DhcareRuq = new DanpheCareReachUsQuicklyModel();
    
    // this.GetAppliedUsrlist();
    this.notifyService.showSuccess('success', 'application submitted successfully ');
    this.submitForm = false;
    this.reCAPTCHAstatus = false;
    this.registerForm.reset();
    //  this.reCAPTCHAstatusRUQ = false;
   
    //this.CaptchaCallback();
  }

  Error(res) {
    var response = res;
    this.submitted = false;
    this.submitted_ruq = false;
    this.loading = false;
    this.loading_ruq = false;
    this.notifyService.showError("Error", " Please fill up the required field")
  }

 

  PaymentMethod() {
    this.showPaymentMethod = true;
  }
  hide() {
    this.showPaymentMethod = false;
    this.showMoreContent = false;
  }
  knowmore() {
    this.showMoreContent = true;
  }
  handleSuccess(recaptchaRes: any[]) {
    this.captchaRes = recaptchaRes;
    if (this.captchaRes.length > 1) {
      this.reCAPTCHAstatus = true;
    }
    console.log("recaptchaResponse");
    console.log(this.captchaRes);
  }
  //handleRUQSuccess(recaptchaRes: any[]) {
  //  this.captchaRes = recaptchaRes;
  //  if (this.captchaRes.length > 1) {
  //    //this.reCAPTCHAstatusRUQ = true;
  //  }
  //  console.log("recaptchaResponse");
  //  console.log(this.captchaRes);
  //}
  GotoTelemedicine() {
    this.routing.navigate(['/login']);
  }
  public CaptchaCallback() {
    this.homeIsolationApplyForm = this.formBuilder.group({
      name: ["", [Validators.required],
      ],
      phonenumber: ['', [Validators.required]],
      //password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]]
      email: ["", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ],
      message: ["", [Validators.required],
      ],
      homeIsoRecaptcha: [""],

      termsandcondition: [false, Validators.requiredTrue]
    });
    this.reachUsQuicklyForm = this.formBuilder.group({
      ruq_name: ["", [Validators.required],
      ],
      ruq_phonenumber: ['', [Validators.required]],
      //password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]]
      ruq_email: [
        "", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ],
      ruq_message: ["", [Validators.required],
      ],
      ruq_company: ["", [Validators.required],
      ]
    });
    this.registerForm = this.formBuilder.group({
      phonenumber: ['', [Validators.required]],
      name: ['', [Validators.required]],
      email: ["", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ],
      //hospitalIdentifier: this.hospitalIdentifier != null ? this.hospitalIdentifier : "NA"
      //password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]],
      //confirmpassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]]
    });
  }

  GoToServiceDetails(permaLink) {
    // this.servicesId = id;
    this.routing.navigate(['/service', permaLink]);


  }

  DepartmentDetails(permaLink) {
    this.routing.navigate(['/department', permaLink]);
  }

  GoToTeamMemberDetails(id){
    this.routing.navigate(['/coreteamdetails'], { queryParams: { id: id } });
  }

  GetContact() {
    this.websiteService.GetContact().subscribe(res => {
      if (res) {



        this.contact = Object.assign(this.contact,res[0]);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Contact Found!");
      });
  }
}
