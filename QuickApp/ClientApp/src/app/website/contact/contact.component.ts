

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { DanpheCareContact } from '../../cms/models/danphecare.cms.model';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { DanphecareService } from '../../services/danphecare/danphe.service';
import { DanpheCareReachUsQuicklyModel } from '../../models/danphecare/danphecare.model';
import { trigger } from '@angular/animations';


@Component({
  selector: 'app-contact-page',
  templateUrl: './contact.component.html'
})
export class ContactPageComponent implements OnInit, AfterViewInit {
  public DhcareRuq: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  public contact: DanpheCareContact = new DanpheCareContact();
  public loading: boolean = false;
  public submitted: boolean = false;
  public isCompany: boolean = false;
  public form: FormGroup;
  constructor(public websiteService: WebsiteService, private notifyService: NotificationService, private formBuilder: FormBuilder,
    public danphecareservice: DanphecareService) {
    //
  }
  
  ngOnInit() {
    this.GetContact();
    //this.loadScripts();
    this.form = this.formBuilder.group({
      phone: ['', [Validators.required]],
      name: ['', [Validators.required]],
      email: ["", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ],
    });


  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  loadScripts() {
    const dynamicScripts = [
      //'../content/js/jquery.min.js',
      './content/js/custom.js',
      './content/js/move-img-effect.js',
      './content/js/plugins.js',
      //'../content/js/popper.min.js',
      './content/js/slick.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
  get PhoneNumberControl() {
    return this.form.get("phone") as FormControl;
  }
  get EmailControl() {
    return this.form.get("email") as FormControl;
  }
  get NameControl() {
    return this.form.get("name") as FormControl;
  }
  GetContact() {
    this.websiteService.GetContact().subscribe(res => {
      if (res && res.length > 0) {
        this.contact = Object.assign(this.contact, res[0]);
       
      }
    },
      res => {
        //this.notifyService.showError("Error", "Internal Error")
      });
  }

  onSubmitReachUsQuickly() {
    
    if (this.form.valid) {
      this.loading = true;
      this.danphecareservice.reachUsQuickly(this.DhcareRuq)
        .subscribe(res => this.SuccessPostReachUsQuickly(res),
          res => this.Error(res));

    }
    else {
      this.loading = false;
      this.submitted = true;
      this.notifyService.showInfo("Info", "Please enter the required field");
      (this.submitted) ? setTimeout(() => { this.submitted = false }, 5250) : null 
    }
        
  }
  SuccessPostReachUsQuickly(res) {
    this.loading = false;
    this.DhcareRuq = new DanpheCareReachUsQuicklyModel();
    this.notifyService.showSuccess('Success', 'application submitted successfully ');
    this.form.reset();
  }

  Error(res) {
    this.loading = false;
    const response = res;
    this.notifyService.showError("Error", " Please fill up the required field")
  }


}
