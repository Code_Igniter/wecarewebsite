

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Expat, Webinar, DanpheCareContact, TeamMember, MediaCoverage, MetaTag } from '../../cms/models/danphecare.cms.model';
import { WebsiteService } from '../websiteservice/website.service';
import { NotificationService } from '../../services/notification.service';
import { AboutUsModel } from '../../cms/models/aboutUs.model';
import { Title, Meta } from '@angular/platform-browser';
import { DanpheCareReachUsQuicklyModel } from '../../models/danphecare/danphecare.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DanphecareService } from '../../services/danphecare/danphe.service';


@Component({
  selector: 'app-expat-page',
  templateUrl: './expat.component.html'
})
export class ExpatPageComponent implements OnInit, AfterViewInit {
  expat = require("../content/img/expat-about.png");
  public expatList: Array<Expat> = new Array<Expat>();
  public webinarList: Array<Webinar> = new Array<Webinar>();
  public imagePath: string;
  public aboutUs: AboutUsModel = new AboutUsModel();
  public contact: DanpheCareContact = new DanpheCareContact();
  ourTeamMemberList: TeamMember[];
  coreTeamMemberList: TeamMember[];
  ourMediaCoverageList: MediaCoverage[];
  public metaTag: Array<MetaTag> = new Array<MetaTag>();
  public tag: MetaTag = new MetaTag();
  public submitted_ruq: boolean = false;
  public loading: boolean = false;
  submitForm: boolean = false;
  public reCAPTCHAstatus: boolean = false;
  public submitted: boolean = false;
  loading_ruq = false;
  public DhcareRuq: DanpheCareReachUsQuicklyModel = new DanpheCareReachUsQuicklyModel();
  registerForm: FormGroup;
  constructor(public websiteService: WebsiteService, private formBuilder: FormBuilder, public danphecareservice: DanphecareService, private notifyService: NotificationService, private titleService: Title, private metaService: Meta) {
    this.GetExpat();
    this.GetWebinar();
    this.GetContact();
    this.GetAboutUs();
    this.GetOurTeamMember();
    this.GetOurMediaCoverage();
   /* this.GetMetaTag();*/
  }

  ngOnInit() {
    this.CaptchaCallback();
    //this.loadScripts();

  }
  ngAfterViewInit() {
    //this.loadScripts();
  }
  loadScripts() {
    const dynamicScripts = [
      //'../content/js/jquery.min.js',
      './content/js/custom.js',
      './content/js/move-img-effect.js',
      './content/js/plugins.js',
      //'../content/js/popper.min.js',
      './content/js/slick.min.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  public CaptchaCallback() {
    this.registerForm = this.formBuilder.group({
      phonenumber: ['', [Validators.required]],
      name: ['', [Validators.required]],
      email: ["", [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)],
      ],
     
    });
  }

  GetMetaTag() {
    this.websiteService.GetMetaTag()
      .subscribe(res => this.SuccessMetaTag(res),
        res => this.ErrorMetaTag(res));
  }
  SuccessMetaTag(res) {
    this.metaTag = res;
    if (this.metaTag.length !== 0) {
    this.metaTag = res.filter(x => x.page === 'Expat Page');
  
      this.tag = this.metaTag[0];
      this.titleService.setTitle(this.tag.title);
      this.metaService.updateTag({ name: 'keywords', content: `${this.tag.keywords}` });
      this.metaService.updateTag({ name: 'description', content: `${this.tag.content}` });
      //this.metaService.addTags([
      //  { name: 'keywords', content: `${this.tag.keywords}` },
      //  { name: 'description', content: `${this.tag.content}` },
      //  { name: 'robots', content: 'index, follow' }
      //]);
    }
  }
  ErrorMetaTag(res) {
    //this.notifyService.showError("Info", "No Meta tag Found!");
  }

  GetExpat() {
    this.websiteService.GetExpat().subscribe(res => {
      if (res) {
        this.expatList = [];
        this.expatList = Object.assign(this.expatList, res);
        //this.imagePath = this.expatList[0].imagePath;
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Expat Found!");
      });
  }
  GetWebinar() {
    this.websiteService.GetWebinar().subscribe(res => {
      if (res) {
        this.webinarList = [];
        this.webinarList = Object.assign(this.webinarList, res);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Webinar Found!");
      });
  }
  GetContact() {
    this.websiteService.GetContact().subscribe(res => {
      if (res) {
        this.contact = Object.assign(this.contact, res[0]);
      }
    },
      res => {
        //this.notifyService.showError("Info", "No Contact Found!");
      });
  }
  GetAboutUs() {
    this.websiteService.GetAboutUs().subscribe(res => {
      if (res && res.length > 0) {
        this.aboutUs = Object.assign(this.aboutUs, res[0]);
      }
    },
      res => {
        //this.notifyService.showError("Error", "Internal Error")
      });
  }
  GetOurTeamMember() {
    this.websiteService.GetOurTeamMembers().subscribe(res => {
      if (res) {
        this.ourTeamMemberList = [];
        this.ourTeamMemberList = Object.assign(this.ourTeamMemberList, res);
        this.coreTeamMemberList = this.ourTeamMemberList.filter(t => t.isCoreTeam == true);
      }
    },
      res => {
        console.log("Error Message: " + res);
        // this.notifyService.showError("Info", "No Services Found!");
      });
  }
  GetOurMediaCoverage() {
    this.websiteService.GetOurMediaCoverage().subscribe(res => {
      if (res) {
        this.ourMediaCoverageList = [];
        this.ourMediaCoverageList = Object.assign(this.ourMediaCoverageList, res);
      }
    },
      res => {
        console.log("Error Message: " + res);
        // this.notifyService.showError("Info", "No Services Found!");
      });
  }


  onSubmitReachUsQuickly() {
    if (this.registerForm.valid) {
      this.submitForm = true;
      this.loading = true;
      this.danphecareservice.reachUsQuickly(this.DhcareRuq)
        .subscribe(res => this.SuccessPostReachUsQuickly(res),
          res => this.Error(res));
    }
    else {
      this.submitForm = true;
      this.loading = false;
      this.notifyService.showInfo("Info", "Please enter the required field");
      //this.registerForm.reset();

      (this.submitForm) ? setTimeout(() => { this.submitForm = false }, 5250) : null

    }
  }

  SuccessPostReachUsQuickly(res) {
    this.loading = false;
    this.DhcareRuq = new DanpheCareReachUsQuicklyModel();

    // this.GetAppliedUsrlist();
    this.notifyService.showSuccess('success', 'application submitted successfully ');
    this.submitForm = false;
    this.reCAPTCHAstatus = false;
    this.registerForm.reset();
    //  this.reCAPTCHAstatusRUQ = false;

    //this.CaptchaCallback();
  }

  Error(res) {
    var response = res;
    this.submitted = false;
    this.submitted_ruq = false;
    this.loading = false;
    this.loading_ruq = false;
    this.notifyService.showError("Error", " Please fill up the required field")
  }
  get PhoneNumberControl() {
    return this.registerForm.get("phonenumber") as FormControl;
  }
  get EmailControl() {
    return this.registerForm.get("email") as FormControl;
  }
  get NameControl() {
    return this.registerForm.get("name") as FormControl;
  }
}
