﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

using AutoMapper;
using DAL.Core;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telemedicine.ServerModel;
using TestTele.ViewModel;

namespace QuickApp.ViewModels
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ApplicationUser, UserViewModel>()
                   .ForMember(d => d.Roles, map => map.Ignore());
            CreateMap<UserViewModel, ApplicationUser>()
                .ForMember(d => d.Roles, map => map.Ignore())
                .ForMember(d => d.Id, map => map.Condition(src => src.Id != null));

            CreateMap<ApplicationUser, UserEditViewModel>()
                .ForMember(d => d.Roles, map => map.Ignore());
            CreateMap<UserEditViewModel, ApplicationUser>()
                .ForMember(d => d.Roles, map => map.Ignore())
                .ForMember(d => d.Id, map => map.Condition(src => src.Id != null));

            CreateMap<ApplicationUser, UserPatchViewModel>()
                .ReverseMap();

            CreateMap<ApplicationRole, RoleViewModel>()
                .ForMember(d => d.Permissions, map => map.MapFrom(s => s.Claims))
                .ForMember(d => d.UsersCount, map => map.MapFrom(s => s.Users != null ? s.Users.Count : 0))
                .ReverseMap();
            CreateMap<RoleViewModel, ApplicationRole>()
                .ForMember(d => d.Id, map => map.Condition(src => src.Id != null));

            CreateMap<IdentityRoleClaim<string>, ClaimViewModel>()
                .ForMember(d => d.Type, map => map.MapFrom(s => s.ClaimType))
                .ForMember(d => d.Value, map => map.MapFrom(s => s.ClaimValue))
                .ReverseMap();

            CreateMap<ApplicationPermission, PermissionViewModel>()
                .ReverseMap();

            CreateMap<IdentityRoleClaim<string>, PermissionViewModel>()
                .ConvertUsing(s => (PermissionViewModel)ApplicationPermissions.GetPermissionByValue(s.ClaimValue));

            CreateMap<Customer, CustomerViewModel>()
                .ReverseMap();

            CreateMap<Product, ProductViewModel>()
                .ReverseMap();

            CreateMap<Order, OrderViewModel>()
                .ReverseMap();
				CreateMap<Danphecare, DanphecareViewModel>();
            CreateMap<Danphecare, DanphecareViewModel>().ReverseMap();
            CreateMap<ContactUs, ContactUsViewModel>();
            CreateMap<ContactUs, ContactUsViewModel>().ReverseMap();
            CreateMap<AboutUsModel, AboutUsViewModel>();
            CreateMap<AboutUsModel, AboutUsViewModel>().ReverseMap();
            CreateMap<OurServicesModel, OurServicesViewModel>();
            CreateMap<OurServicesModel, OurServicesViewModel>().ReverseMap();
            CreateMap<OurTeamMemberModel, OurTeamMemberViewModel>();
            CreateMap<OurTeamMemberModel, OurTeamMemberViewModel>().ReverseMap();
            CreateMap<OurMediaCoverageModel, OurMediaCoverageViewModel>();
            CreateMap<OurMediaCoverageModel, OurMediaCoverageViewModel>().ReverseMap();
            CreateMap<TestimonialMainModel, TestimonialMainViewModel>();
            CreateMap<TestimonialMainModel, TestimonialMainViewModel>().ReverseMap();
            CreateMap<TestimonialModel, TestimonialViewModel>();
            CreateMap<TestimonialModel, TestimonialViewModel>().ReverseMap();
            CreateMap<DepartmentModel, DepartmentDanpheCareViewModel>();
            CreateMap<DepartmentModel, DepartmentDanpheCareViewModel>().ReverseMap();
            CreateMap<DanpheCareDoctorModel, DanpheCareDoctorViewModel>();
            CreateMap<DanpheCareDoctorModel, DanpheCareDoctorViewModel>().ReverseMap();
            CreateMap<DepartmentConsultationModel, DepartmentConsultationViewModel>();
            CreateMap<DepartmentConsultationModel, DepartmentConsultationViewModel>().ReverseMap();
            CreateMap<DepartmentConsultationModel, ConsultationViewModel>();
            CreateMap<DepartmentConsultationModel, ConsultationViewModel>().ReverseMap();
            CreateMap<ResourcefulArticlesModel, ResourcefulArticlesViewModel>();
            CreateMap<ResourcefulArticlesModel, ResourcefulArticlesViewModel>().ReverseMap();
            CreateMap<NewsModel, NewsViewModel>();
            CreateMap<NewsModel, NewsViewModel>().ReverseMap();
            CreateMap<BlogsModel, BlogsViewModel>();
            CreateMap<BlogsModel, BlogsViewModel>().ReverseMap();
            CreateMap<MetaTagModel, MetaTagViewModel>();
            CreateMap<MetaTagModel, MetaTagViewModel>().ReverseMap();
            CreateMap<DanpheCareContactModel, DanpheCareContactViewModel>();
            CreateMap<DanpheCareContactModel, DanpheCareContactViewModel>().ReverseMap();
            CreateMap<ExpatModel, ExpatViewModel>();
            CreateMap<ExpatModel, ExpatViewModel>().ReverseMap();
            CreateMap<WebinarModel, WebinarViewModel>();
            CreateMap<WebinarModel, WebinarViewModel>().ReverseMap();
            CreateMap<DepartmentTabContent, DepartmentTabContentViewModel>();
            CreateMap<DepartmentTabContent, DepartmentTabContentViewModel>().ReverseMap();
            CreateMap<DepartmentSubHeading, DepartmentSubHeadingViewModel>();
            CreateMap<DepartmentSubHeading, DepartmentSubHeadingViewModel>().ReverseMap();
            CreateMap<PackageDetails, PackageDetailsViewModel>();
            CreateMap<PackageDetails, PackageDetailsViewModel>().ReverseMap();
            CreateMap<PackageNameModel, PackageNameViewModel>();
            CreateMap<PackageNameModel, PackageNameViewModel>().ReverseMap();
            CreateMap<SubServices, SubServicesViewModel>();
            CreateMap<SubServices, SubServicesViewModel>().ReverseMap();
            CreateMap<SubServicesDetails, SubServicesDetailsViewModel>();
            CreateMap<SubServicesDetails, SubServicesDetailsViewModel>().ReverseMap();

        }
    }
}
