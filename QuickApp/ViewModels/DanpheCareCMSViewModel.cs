﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class AboutUsViewModel
    {
        public int AboutUsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ShortDescription { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }

    }

    public class OurServicesViewModel
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string Content { get; set; }
        public string IconPath { get; set; }
        public string CoverImage { get; set; }
        public string ImagePath { get; set; }
        public string SubscriptionPlan { get; set; }
        public string SubscriptionContent { get; set; }
        public string SubscriptionImage { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public bool IsCoverImagePathUploaded { get; set; }
        public bool IsIconPathUploaded { get; set; }
        public bool IsSubscriptionImageUploaded { get; set; }
        public int? DepartmentId { get; set; }
        public string ShortIntroduction { get; set; }
        public string PermaLink { get; set; }
        public string Color { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
    }

    public class DepartmentDanpheCareViewModel
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string IconPath { get; set; }
        public bool IsIconPathUploaded { get; set; }
        public string Title { get; set; }
        public string Introduction { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public string PermaLink { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
    }

    public class DanpheCareDoctorViewModel
    {
        public int DoctorId { get; set; }
        public string FullName { get; set; }
        public string Designation { get; set; }
        public string Experience { get; set; }
        public string DepartmentName { get; set; }
        public int? DepartmentId { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public string CoverPhoto { get; set; }
        public bool IsCoverPhotoUploaded { get; set; }
        public string Content { get; set; }
        public int Sorting { get; set; }
        public bool DisableSorting { get; set; }
        public string PermaLink { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }

    }
    public class DepartmentConsultationViewModel
    {
        public string DepartmentName { get; set; }
        public int DepartmentConsultationId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string IconPath { get; set; }
        public bool IsIconPathUploaded { get; set; }
        public int DepartmentId { get; set; }
    }
    public class ConsultationViewModel
    {
        public string DepartmentName { get; set; }
        public int DepartmentConsultationId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string IconPath { get; set; }
        public bool IsIconPathUploaded { get; set; }
        public int DepartmentId { get; set; }
    }

    public class ResourcefulArticlesViewModel
    {
        public int ResourcefulArticlesId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
    }

    public class NewsViewModel
    {
        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public string PostedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
    }

    public class BlogsViewModel
    {
        public int BlogId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public string PostedBy { get; set; }
        public string PermaLink { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
    }
    public class CommentsViewModel
    {
        public int CommentId { get; set; }
        public string Comment { get; set; }
        public Guid BlogId { get; set; }

    }
    public class MetaTagViewModel
    {
        public int MetaTagId { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Page { get; set; }
        public int BlogId { get; set; }
        public int DoctorId { get; set; }
        public int DepartmentId { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string DoctorName { get; set; }
        public string DepartmentName { get; set; }
        public string BlogName { get; set; }

    }

    public class OurTeamMemberViewModel
    {
        public int TeamMemberId { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public string Designation { get; set; }
        public string FullName { get; set; }
        public string Department { get; set; }
        public string ShortDescription { get; set; }
        public bool IsCoreTeam { get; set; }
        public string Content { get; set; }
        public int Sorting { get; set; }
        public bool DisableSorting { get; set; }
        public string CoverPhoto { get; set; }
        public bool IsCoverPhotoUploaded { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
    }

    public class OurMediaCoverageViewModel
    {
        public int MediaId { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public string MediaURL { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
    }

    public class TestimonialMainViewModel
    {
        public int TestimonialMainId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string CustomerCount { get; set; }
    }
    public class TestimonialViewModel
    {
        public int TestimonialId { get; set; }
        public int Star { get; set; }
        public string Message { get; set; }
        public string WriterName { get; set; }
        public string WriterDesignation { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
    }

    public class DanpheCareContactViewModel
    {
        public int DanpheCareContactId { get; set; }
        public string Location { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string GoogleMap { get; set; }
        public string OpeningHours { get; set; }

    }
    public class ExpatViewModel
    {
        public int ExpatId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public bool IsImagePathUploaded { get; set; }
        public string MetaContent { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
    }
    public class WebinarViewModel
    {
        public int WebinarId { get; set; }
        public string Title { get; set; }
        public string WebinarVideo { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class DepartmentTabContentViewModel
    {
        public int DepartmentContentId { get; set; }
        public int DepartmentTabId { get; set; }
        public string DepartmentTabName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }

    public class DepartmentSubHeadingViewModel
    {
        public int DepartmentSubHeadingId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string IconPath { get; set; }
        public bool IsIconPathUploaded { get; set; }
        public int DepartmentId { get; set; }
    }

    public class PackageNameViewModel
    {
        public int PackageNameModelId { get; set; }
        public string PackageName { get; set; }
    }

    public class PackageDetailsViewModel
    {
        public int PackageDetailId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int PackageNameModelId { get; set; }
    }

    public class SubServicesViewModel
    {
        public int SubServicesId { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public int OurServicesId { get; set; }
        public bool IsImagePathUploaded { get; set; }
    }

    public class SubServicesDetailsViewModel
    {
        public int SubServicesDetailsId { get; set; }
        public string SubTitle { get; set; }
        public string Introduction { get; set; }
        public string ImagePath { get; set; }
        public string Content { get; set; }
        public int SubServicesId { get; set; }
        public bool IsImagePathUploaded { get; set; }
    }
}
