﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class DanphecareViewModel
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string ContactNumber { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
        public bool IsEmailSent { get; set; }
        public int ServiceId { get; set; }
        public string Service { get; set; }       
        public decimal Price { get; set; }
        public string PaymentProvider { get; set; }
        public DateTime CreatedOn { get; set; }
    }
    public class ReachUsQuicklyViewModel
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Company { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
