﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class ContactUsViewModel
    {       
        public int UserContactId { get; set; }
        public string Name { get; set; }       
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }       
        public string Massage { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class NotificationMetadata
    {
        public string Sender { get; set; }
        public string Reciever { get; set; }
        public string SmtpServer { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class EmailMessage
    {
        public MailboxAddress Sender { get; set; }
        public MailboxAddress Reciever { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}
