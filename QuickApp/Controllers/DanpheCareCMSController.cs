﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTele.ViewModel;
using AutoMapper;
using Telemedicine.ServerModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using Microsoft.Extensions.Hosting.Internal;
using System.Net.Mail;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using DAL;
using DAL.Models;

namespace TestTele.Controllers
{
    [Route("api/[controller]/[action]")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DanpheCareCMSController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly ILogger<DanpheCareCMSController> _logger;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        //
        public DanpheCareCMSController(IUnitOfWork unitOfWork, IAuthorizationService authorizationService, IMapper mapper, IWebHostEnvironment hostingEnvironment,
                                  UserManager<ApplicationUser> userManager, ILogger<DanpheCareCMSController> logger)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _authorizationService = authorizationService;
            this._logger = logger;
            _userManager = userManager;
        }

        #region About Us  
        [HttpGet]
        public ActionResult GetAboutUs()
        {
            try
            {
                var abtus = _unitOfWork.AboutUs.GetAll();
                var abtList = _mapper.Map<List<AboutUsModel>, List<AboutUsViewModel>>(abtus.ToList());

                return Ok(abtList);
            }
            catch(System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
        #endregion

        #region GET Our Services 
        [HttpGet]
        public ActionResult GetOurServices()
        {
            try
            {
                var _getServices = _unitOfWork.OurServices.GetAll().OrderByDescending(x => x.CreatedDate).ToList();
                var _getDepartment = _unitOfWork.DepartmentModels.GetAll().ToList();
                var query = (from services in _getServices
                             join dep in _getDepartment on services.DepartmentId equals dep.DepartmentId
                                    select new
                                    {
                                        services.ServiceId,
                                        services.DepartmentId,
                                        services.IconPath,
                                        services.ImagePath,
                                        services.SubscriptionImage,
                                        services.ServiceName,
                                        services.SubscriptionContent,
                                        services.SubscriptionPlan,
                                        services.Content,
                                        dep.DepartmentName,
                                        services.CreatedDate,
                                        services.CoverImage,
                                        services.ShortIntroduction,
                                        services.Color,
                                        services.PermaLink
                                    }).OrderByDescending(x => x.CreatedDate).ToList();

                return Ok(_getServices);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
        #endregion

        #region GET Our Team Members 
        [HttpGet]
        public ActionResult GetOurTeamMembers()
        {
            try
            {
                var data = _unitOfWork.OurTeamMembers.GetAll();
                var dataList = _mapper.Map<List<OurTeamMemberModel>, List<OurTeamMemberViewModel>>(data.OrderByDescending(x => x.CreatedDate).ToList());

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        [HttpGet]
        public ActionResult GetTeamSortingList()
        {
            try
            {
                var data = _unitOfWork.OurTeamMembers.GetAll();
                var dataList = _mapper.Map<List<OurTeamMemberModel>, List<OurTeamMemberViewModel>>(data.OrderBy(x => x.Sorting).ToList());

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion

        #region GET Our Media Coverage 
        [HttpGet]
        public ActionResult GetMediaCoverage()
        {
            try
            {
                var data = _unitOfWork.OurMediaCoverage.GetAll();
                var dataList = _mapper.Map<List<OurMediaCoverageModel>, List<OurMediaCoverageViewModel>>(data.OrderByDescending(x => x.CreatedDate).ToList());

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion

        #region GET TestimonialMain
        [HttpGet]
        public ActionResult GetTestimonialMain()
        {
            try
            {
                var abtus = _unitOfWork.TestimonialMain.GetAll();
                var abtList = _mapper.Map<List<TestimonialMainModel>, List<TestimonialMainViewModel>>(abtus.ToList());

                return Ok(abtList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
        #endregion

        #region GET Our Testimonials 
        [HttpGet]
        public ActionResult GetTestimonials()
        {
            try
            {
                var data = _unitOfWork.Testimonials.GetAll();
                var dataList = _mapper.Map<List<TestimonialModel>, List<TestimonialViewModel>>(data.OrderByDescending(x => x.CreatedDate).ToList());

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion

        #region POST Upload File function 
        [HttpPost, DisableRequestSizeLimit]
        public ActionResult UploadFile()
        {
            try
            {
                var file = Request.Form.Files[0];
                string folderName = "Upload/DanpheCareCMS";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    //string fileName = DateTime.Now.ToFileTime().ToString();
                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                return Json("Upload Successful.");
            }
            catch (System.Exception ex)
            {
                return Json("Upload Failed: " + ex.Message);
            }
        }
        #endregion

        #region POST About Us Page
        [HttpPost]
        [Produces(typeof(AboutUsViewModel))]
        public IActionResult AddAboutUs([FromBody]AboutUsViewModel _aboutUsVM)
        {
            try
            {
                var aboutUs = _mapper.Map<AboutUsModel>(_aboutUsVM);
                if (_aboutUsVM.ImagePath != "" )
                {
                    aboutUs.ImagePath = "../Upload/DanpheCareCMS/" + _aboutUsVM.ImagePath.Substring(_aboutUsVM.ImagePath.LastIndexOf('\\') + 1);
                }
                aboutUs.CreatedDate = DateTime.Now;
                _unitOfWork.AboutUs.Add(aboutUs);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion

        #region POST Our Services Page
        [HttpPost]
        [Produces(typeof(OurServicesViewModel))]
        public IActionResult AddService([FromBody] OurServicesViewModel _ourServiceVM)
        {
            try
            {
                var ourServ = _mapper.Map<OurServicesModel>(_ourServiceVM);
                if (!string.IsNullOrEmpty(_ourServiceVM.ImagePath))
                {
                    ourServ.ImagePath = "../Upload/DanpheCareCMS/" + _ourServiceVM.ImagePath.Substring(_ourServiceVM.ImagePath.LastIndexOf('\\') + 1);
                }
                if (!string.IsNullOrEmpty(_ourServiceVM.IconPath))
                {
                    ourServ.IconPath = "../Upload/DanpheCareCMS/" + _ourServiceVM.IconPath.Substring(_ourServiceVM.IconPath.LastIndexOf('\\') + 1);
                }
                if (!string.IsNullOrEmpty(_ourServiceVM.SubscriptionImage))
                {
                    ourServ.SubscriptionImage = "../Upload/DanpheCareCMS/" + _ourServiceVM.SubscriptionImage.Substring(_ourServiceVM.SubscriptionImage.LastIndexOf('\\') + 1);
                }
                if (!string.IsNullOrEmpty(_ourServiceVM.CoverImage) )
                {
                    ourServ.CoverImage = "../Upload/DanpheCareCMS/" + _ourServiceVM.CoverImage.Substring(_ourServiceVM.CoverImage.LastIndexOf('\\') + 1);
                }
                ourServ.CreatedDate = DateTime.Now;
                _unitOfWork.OurServices.Add(ourServ);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion


        #region POST Our Team Member
        [HttpPost]
        [Produces(typeof(OurTeamMemberViewModel))]
        public IActionResult AddOurTeamMember([FromBody] OurTeamMemberViewModel _ourTeamMemberVM)
        {
            try
            {
                var ourTeam = _mapper.Map<OurTeamMemberModel>(_ourTeamMemberVM);
                if (!string.IsNullOrEmpty(_ourTeamMemberVM.ImagePath))
                {
                    ourTeam.ImagePath = "../Upload/DanpheCareCMS/" + _ourTeamMemberVM.ImagePath.Substring(_ourTeamMemberVM.ImagePath.LastIndexOf('\\') + 1);
                }
                ourTeam.CreatedDate = DateTime.Now;
                _unitOfWork.OurTeamMembers.Add(ourTeam);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion

        #region POST Our Media Coverage
        [HttpPost]
        [Produces(typeof(OurMediaCoverageViewModel))]
        public IActionResult AddMediaCoverage([FromBody] OurMediaCoverageViewModel _ourMediaCoverageVM)
        {
            try
            {
                var ourMedCov = _mapper.Map<OurMediaCoverageModel>(_ourMediaCoverageVM);
                if (!string.IsNullOrEmpty(_ourMediaCoverageVM.ImagePath))
                {
                    ourMedCov.ImagePath = "../Upload/DanpheCareCMS/" + _ourMediaCoverageVM.ImagePath.Substring(_ourMediaCoverageVM.ImagePath.LastIndexOf('\\') + 1);
                }
                ourMedCov.CreatedDate = DateTime.Now;
                _unitOfWork.OurMediaCoverage.Add(ourMedCov);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion

        #region POST Our Testimonial Main
        [HttpPost]
        [Produces(typeof(TestimonialMainViewModel))]
        public IActionResult AddTestimonialMain([FromBody] TestimonialMainViewModel _testimonialMainVM)
        {
            try
            {
                var tesMain = _mapper.Map<TestimonialMainModel>(_testimonialMainVM);
                //if (_testimonialMainVM.ImagePath != null)
                //{
                //    tesMain.ImagePath = "../Upload/DanpheCareCMS/" + _testimonialMainVM.ImagePath.Substring(_testimonialMainVM.ImagePath.LastIndexOf('\\') + 1);
                //}
                tesMain.CreatedDate = DateTime.Now;
                _unitOfWork.TestimonialMain.Add(tesMain);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion

        #region POST Our Testimonial
        [HttpPost]
        [Produces(typeof(TestimonialViewModel))]
        public IActionResult AddTestimonial([FromBody] TestimonialViewModel _testimonialVM)
        {
            try
            {
                var testi = _mapper.Map<TestimonialModel>(_testimonialVM);

                if (!string.IsNullOrEmpty(_testimonialVM.ImagePath))
                {
                    testi.ImagePath = "../Upload/DanpheCareCMS/" + _testimonialVM.ImagePath.Substring(_testimonialVM.ImagePath.LastIndexOf('\\') + 1);
                }
                testi.CreatedDate = DateTime.Now;
                _unitOfWork.Testimonials.Add(testi);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        #endregion

        #region PUT About Us 
        [HttpPut("{id}")]
        [Produces(typeof(AboutUsViewModel))]
        public ActionResult UpdateAboutUs([FromBody] AboutUsViewModel _aboutUsVM)
        {
            try
            {
                var aboutUs = _mapper.Map<AboutUsModel>(_aboutUsVM);

                if (_aboutUsVM.ImagePath != "" && _aboutUsVM.IsImagePathUploaded)
                {
                    aboutUs.ImagePath = "../Upload/DanpheCareCMS/" + _aboutUsVM.ImagePath.Substring(_aboutUsVM.ImagePath.LastIndexOf('\\') + 1);

                }
                aboutUs.UpdatedDate = DateTime.Now;
                _unitOfWork.AboutUs.Update(aboutUs);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PUT Our Services
        [HttpPut("{id}")]
        [Produces(typeof(OurServicesViewModel))]
        public ActionResult UpdateService([FromBody] OurServicesViewModel _ourServiceVM)
        {
            try
            {
                var ourServices = _mapper.Map<OurServicesModel>(_ourServiceVM);

                if (!string.IsNullOrEmpty(_ourServiceVM.ImagePath) && _ourServiceVM.IsImagePathUploaded)
                {
                    ourServices.ImagePath = "../Upload/DanpheCareCMS/" + _ourServiceVM.ImagePath.Substring(_ourServiceVM.ImagePath.LastIndexOf('\\') + 1);

                }
                if (!string.IsNullOrEmpty(_ourServiceVM.IconPath) && _ourServiceVM.IsIconPathUploaded)
                {
                    ourServices.IconPath = "../Upload/DanpheCareCMS/" + _ourServiceVM.IconPath.Substring(_ourServiceVM.IconPath.LastIndexOf('\\') + 1);
                }
                if (!string.IsNullOrEmpty(_ourServiceVM.SubscriptionImage) && _ourServiceVM.IsSubscriptionImageUploaded)
                {
                    ourServices.SubscriptionImage = "../Upload/DanpheCareCMS/" + _ourServiceVM.SubscriptionImage.Substring(_ourServiceVM.SubscriptionImage.LastIndexOf('\\') + 1);
                }
                
                     if (!string.IsNullOrEmpty(_ourServiceVM.CoverImage) && _ourServiceVM.IsCoverImagePathUploaded)
                {
                    ourServices.CoverImage = "../Upload/DanpheCareCMS/" + _ourServiceVM.CoverImage.Substring(_ourServiceVM.CoverImage.LastIndexOf('\\') + 1);
                }
                ourServices.UpdatedDate = DateTime.Now;
                _unitOfWork.OurServices.Update(ourServices);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PUT Our Team Members
        [HttpPut("{id}")]
        [Produces(typeof(OurTeamMemberViewModel))]
        public ActionResult UpdateOurTeam([FromBody] OurTeamMemberViewModel _ourTeamMemberVM)
        {
            try
            {
                var ourTeam = _mapper.Map<OurTeamMemberModel>(_ourTeamMemberVM);

                if (!string.IsNullOrEmpty(_ourTeamMemberVM.ImagePath) && _ourTeamMemberVM.IsImagePathUploaded)
                {
                    ourTeam.ImagePath = "../Upload/DanpheCareCMS/" + _ourTeamMemberVM.ImagePath.Substring(_ourTeamMemberVM.ImagePath.LastIndexOf('\\') + 1);

                }
                if (!string.IsNullOrEmpty(_ourTeamMemberVM.CoverPhoto) && _ourTeamMemberVM.IsCoverPhotoUploaded)
                {
                    ourTeam.CoverPhoto = "../Upload/DanpheCareCMS/" + _ourTeamMemberVM.CoverPhoto.Substring(_ourTeamMemberVM.CoverPhoto.LastIndexOf('\\') + 1);

                }
                if (_ourTeamMemberVM.DisableSorting == true )
                {
                    ourTeam.Sorting = 0;

                }
                ourTeam.UpdatedDate = DateTime.Now;
                _unitOfWork.OurTeamMembers.UpdateProperties(ourTeam, p => p.FullName, p => p.Designation, p => p.Department, p => p.ImagePath, p => p.ShortDescription, p => p.IsCoreTeam, p => p.Content, p => p.CoverPhoto, p => p.DisableSorting, p =>p.Sorting);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PUT Our Media Coverage
        [HttpPut("{id}")]
        [Produces(typeof(OurMediaCoverageViewModel))]
        public ActionResult UpdateMediaCoverage([FromBody] OurMediaCoverageViewModel _ourMediaCoverageVM)
        {
            try
            {
                var ourMediaCov = _mapper.Map<OurMediaCoverageModel>(_ourMediaCoverageVM);

                if (!string.IsNullOrEmpty(_ourMediaCoverageVM.ImagePath) && _ourMediaCoverageVM.IsImagePathUploaded)
                {
                    ourMediaCov.ImagePath = "../Upload/DanpheCareCMS/" + _ourMediaCoverageVM.ImagePath.Substring(_ourMediaCoverageVM.ImagePath.LastIndexOf('\\') + 1);

                }
                ourMediaCov.UpdatedDate = DateTime.Now;
                _unitOfWork.OurMediaCoverage.UpdateProperties(ourMediaCov, p => p.ImagePath, p => p.Name, p => p.MediaURL, p => p.ShortDescription);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PUT Testimonial Main
        [HttpPut("{id}")]
        [Produces(typeof(TestimonialMainViewModel))]
        public ActionResult UpdateTestimonialMain([FromBody] TestimonialMainViewModel _testimonialMainVM)
        {
            try
            {
                var testiMain = _mapper.Map<TestimonialMainModel>(_testimonialMainVM);

                //if (_testimonialMainVM.ImagePath != null)
                //{
                //    ourMediaCov.ImagePath = "../Upload/DanpheCareCMS/" + _testimonialMainVM.ImagePath.Substring(_testimonialMainVM.ImagePath.LastIndexOf('\\') + 1);

                //}
                testiMain.UpdatedDate = DateTime.Now;
                _unitOfWork.TestimonialMain.UpdateProperties(testiMain, p => p.Title, p => p.Content, p => p.CustomerCount);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PUT Testimonial
        [HttpPut("{id}")]
        [Produces(typeof(TestimonialViewModel))]
        public ActionResult UpdateTestimonial([FromBody] TestimonialViewModel _testimonialVM)
        {
            try
            {
                var testiMain = _mapper.Map<TestimonialModel>(_testimonialVM);

                if (!string.IsNullOrEmpty(_testimonialVM.ImagePath) && _testimonialVM.IsImagePathUploaded)
                {
                    testiMain.ImagePath = "../Upload/DanpheCareCMS/" + _testimonialVM.ImagePath.Substring(_testimonialVM.ImagePath.LastIndexOf('\\') + 1);

                }
                testiMain.UpdatedDate = DateTime.Now;
                _unitOfWork.Testimonials.UpdateProperties(testiMain, p => p.Star, p => p.Message, p => p.WriterName, p => p.WriterDesignation, p => p.ImagePath);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DELETE Our Services
        [HttpDelete("{id}")]
        [Produces(typeof(OurServicesViewModel))]
        public ActionResult DeleteService(int id)
        {
            var _serviceToDelete = _unitOfWork.OurServices.Get(id);

            try
            {
                _unitOfWork.OurServices.Remove(_serviceToDelete);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DELETE Our Team Members
        [HttpDelete("{id}")]
        [Produces(typeof(OurTeamMemberViewModel))]
        public ActionResult DeleteOurTeamMember(int id)
        {
            var _dataToDelete = _unitOfWork.OurTeamMembers.Get(id);

            try
            {
                _unitOfWork.OurTeamMembers.Remove(_dataToDelete);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DELETE Our Media Coverage
        [HttpDelete("{id}")]
        [Produces(typeof(OurMediaCoverageViewModel))]
        public ActionResult DeleteMediaCoverage(int id)
        {
            var _dataToDelete = _unitOfWork.OurMediaCoverage.Get(id);

            try
            {
                _unitOfWork.OurMediaCoverage.Remove(_dataToDelete);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DELETE Our Testimonial
        [HttpDelete("{id}")]
        [Produces(typeof(TestimonialViewModel))]
        public ActionResult DeleteTestimonial(int id)
        {
            var _dataToDelete = _unitOfWork.Testimonials.Get(id);

            try
            {
                _unitOfWork.Testimonials.Remove(_dataToDelete);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Services

        #region Danphe Care Department

        [HttpGet("{id}")]
        public async Task<ActionResult> GetDepartmentById(int id)
        {
            var deps = await _unitOfWork.DepartmentModels.GetAllAsync();
            var doctor = _unitOfWork.DanpheCareDoctorModels.GetAll().ToList();
            var query = (from doc in doctor
                         join dep in deps on doc.DepartmentId equals dep.DepartmentId
                          where dep.DepartmentId == id
                          select new
                          {
                              doc.Designation,
                              doc.Experience,
                              doc.FullName,
                              doc.ImagePath,
                              dep.DepartmentName
                          }).ToList();

            return Ok(query);

        }

        public ActionResult GetDepartmentList()
        {
            try
            {
                var _getDept = _unitOfWork.DepartmentModels.GetAll();
                var _depList = _mapper.Map<List<DepartmentModel>, List<DepartmentDanpheCareViewModel>>(_getDept.OrderByDescending(x => x.CreatedDate).ToList());

                return Ok(_depList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(DepartmentDanpheCareViewModel))]
        public IActionResult AddDanpheCareDepartment([FromBody] DepartmentDanpheCareViewModel _danpheCareDept)
        {
            try
            {
                var danpheDept = _mapper.Map<DepartmentModel>(_danpheCareDept);

                if (!string.IsNullOrEmpty(_danpheCareDept.IconPath))
                {
                    danpheDept.IconPath = "../Upload/DanpheCareCMS/" + _danpheCareDept.IconPath.Substring(_danpheCareDept.IconPath.LastIndexOf('\\') + 1);
                }
                if (!string.IsNullOrEmpty(_danpheCareDept.ImagePath))
                {
                    danpheDept.ImagePath = "../Upload/DanpheCareCMS/" + _danpheCareDept.ImagePath.Substring(_danpheCareDept.ImagePath.LastIndexOf('\\') + 1);
                }
                danpheDept.CreatedDate = DateTime.Now;
                _unitOfWork.DepartmentModels.Add(danpheDept);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(DepartmentDanpheCareViewModel))]
        public ActionResult UpdateDanpheCareDepartment([FromBody] DepartmentDanpheCareViewModel _danpheCareDeptVm)
        {
            try
            {
                var danpheDept = _mapper.Map<DepartmentModel>(_danpheCareDeptVm);

                if (!string.IsNullOrEmpty(_danpheCareDeptVm.IconPath) && _danpheCareDeptVm.IsIconPathUploaded)
                {
                    danpheDept.IconPath = "../Upload/DanpheCareCMS/" + _danpheCareDeptVm.IconPath.Substring(_danpheCareDeptVm.IconPath.LastIndexOf('\\') + 1);

                }
                if (!string.IsNullOrEmpty(_danpheCareDeptVm.ImagePath) && _danpheCareDeptVm.IsImagePathUploaded)
                {
                    danpheDept.ImagePath = "../Upload/DanpheCareCMS/" + _danpheCareDeptVm.ImagePath.Substring(_danpheCareDeptVm.ImagePath.LastIndexOf('\\') + 1);

                }
                danpheDept.UpdatedDate = DateTime.Now;
                _unitOfWork.DepartmentModels.Update(danpheDept);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(DepartmentDanpheCareViewModel))]
        public void DeleteDanpheDepartment(int id)
        {

            var _departmentToDelete = _unitOfWork.DepartmentModels.GetAll().Where(x=>x.DepartmentId == id).FirstOrDefault();
            try
            {
                _unitOfWork.DepartmentModels.Remove(_departmentToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Danphe Care Department

        #region Danphe Care Doctor


        [HttpGet("{id}")]
        public async Task<ActionResult> GetDoctorById(int id)
        {
            var deps = await _unitOfWork.DepartmentModels.GetAllAsync();
            var doctor = _unitOfWork.DanpheCareDoctorModels.GetAll().ToList();
            var query = (from doc in doctor
                       
                         where doc.DoctorId == id
                         select new
                         {
                             doc.Designation,
                             doc.Experience,
                             doc.FullName,
                             doc.ImagePath,
                             //dep.DepartmentName,
                             doc.CoverPhoto,
                             doc.Content,
                             doc.PermaLink
                         }).ToList();

            return Ok(query);

        }
        public ActionResult GetDoctorList()
        {
            try
            {
                var _getDoc = _unitOfWork.DanpheCareDoctorModels.GetAll();
                //var _getDepartment = _unitOfWork.DepartmentModels.GetAll();
                var _docList = (from doc in _getDoc
                             //join dep in _getDepartment on doc.DepartmentId equals dep.DepartmentId
                             select new
                             {
                                 doc.FullName,
                                 doc.Experience,
                                 doc.Designation,
                                 doc.DoctorId,
                                 doc.DepartmentId,
                                 doc.ImagePath,
                                 //dep.DepartmentName,
                                 doc.CoverPhoto,
                                 doc.CreatedDate,
                                 doc.Content,
                                 doc.DisableSorting,
                                 doc.Sorting,
                                 doc.PermaLink

                             }).OrderByDescending(x => x.CreatedDate).ToList();
                

                return Ok(_getDoc);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpGet]
        public ActionResult GetDoctorSortingList()
        {
            try
            {
                var data = _unitOfWork.DanpheCareDoctorModels.GetAll();
                var dataList = _mapper.Map<List<DanpheCareDoctorModel>, List<DanpheCareDoctorViewModel>>(data.OrderBy(x => x.Sorting).ToList());

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Produces(typeof(DanpheCareDoctorViewModel))]
        public IActionResult AddDanpheCareDoctor([FromBody] DanpheCareDoctorViewModel _danpheCareDoc)
        {
            try
            {
                var danpheDoc = _mapper.Map<DanpheCareDoctorModel>(_danpheCareDoc);
                if (!string.IsNullOrEmpty(_danpheCareDoc.ImagePath))
                {
                    danpheDoc.ImagePath = "../Upload/DanpheCareCMS/" + _danpheCareDoc.ImagePath.Substring(_danpheCareDoc.ImagePath.LastIndexOf('\\') + 1);
                }
                if (!string.IsNullOrEmpty(_danpheCareDoc.CoverPhoto))
                {
                    danpheDoc.CoverPhoto = "../Upload/DanpheCareCMS/" + _danpheCareDoc.CoverPhoto.Substring(_danpheCareDoc.CoverPhoto.LastIndexOf('\\') + 1);
                }
                danpheDoc.CreatedDate = DateTime.Now;
                danpheDoc.DepartmentId = null;
                _unitOfWork.DanpheCareDoctorModels.Add(danpheDoc);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(DanpheCareDoctorViewModel))]
        public ActionResult UpdateDanpheCareDoctor([FromBody] DanpheCareDoctorViewModel _danpheCareDocVm)
        {
            try
            {
                var danpheDoc = _mapper.Map<DanpheCareDoctorModel>(_danpheCareDocVm);

                if (!string.IsNullOrEmpty(_danpheCareDocVm.ImagePath) && _danpheCareDocVm.IsImagePathUploaded)
                {
                    danpheDoc.ImagePath = "../Upload/DanpheCareCMS/" + _danpheCareDocVm.ImagePath.Substring(_danpheCareDocVm.ImagePath.LastIndexOf('\\') + 1);

                }
                if (!string.IsNullOrEmpty(_danpheCareDocVm.CoverPhoto) && _danpheCareDocVm.IsCoverPhotoUploaded)
                {
                    danpheDoc.CoverPhoto = "../Upload/DanpheCareCMS/" + _danpheCareDocVm.CoverPhoto.Substring(_danpheCareDocVm.CoverPhoto.LastIndexOf('\\') + 1);

                }
                if(_danpheCareDocVm.DisableSorting == true)
                {
                    danpheDoc.Sorting = 0;
                }
                danpheDoc.UpdatedDate = DateTime.Now;
                danpheDoc.DepartmentId = null;
                _unitOfWork.DanpheCareDoctorModels.Update(danpheDoc);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(DanpheCareDoctorViewModel))]
        public void DeleteDanpheDoctor(int id)
        {

            var _docToDelete = _unitOfWork.DanpheCareDoctorModels.GetAll().Where(x => x.DoctorId == id).FirstOrDefault();
            try
            {
                _unitOfWork.DanpheCareDoctorModels.Remove(_docToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Danphe Care Doctor

        #region Department Consultation

       
        public ActionResult GetConsultationList()
        {
            try
            {
                var _getConsultation = _unitOfWork.DepartmentConsultationModels.GetAll().ToList();
                var _getDepartment = _unitOfWork.DepartmentModels.GetAll().ToList();
                var _consultationList = (from con in _getConsultation
                                         join dep in _getDepartment on con.DepartmentId equals dep.DepartmentId
                                select new
                                {
                                    con.DepartmentConsultationId,
                                    con.DepartmentId,
                                    con.Title,
                                    con.Content,
                                    con.IconPath,
                                    dep.DepartmentName,
                                    con.CreatedDate
                                   

                                }).OrderByDescending(x => x.CreatedDate).ToList();

                return Ok(_consultationList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        //[Produces(typeof(DepartmentConsultationViewModel))]
        public ActionResult AddConsultation([FromBody] DepartmentConsultationViewModel _depConsultationVm)
        {
            try
            {
                var depCons = _mapper.Map<DepartmentConsultationModel>(_depConsultationVm);
                if (!string.IsNullOrEmpty(_depConsultationVm.IconPath))
                {
                    depCons.IconPath = "../Upload/DanpheCareCMS/" + _depConsultationVm.IconPath.Substring(_depConsultationVm.IconPath.LastIndexOf('\\') + 1);
                }
                depCons.CreatedDate = DateTime.Now;
                _unitOfWork.DepartmentConsultationModels.Add(depCons);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(DepartmentConsultationViewModel))]
        public ActionResult UpdateDepartmentConsultation([FromBody] DepartmentConsultationViewModel _depConsultationVm)
        {
            try
            {
                var depCons = _mapper.Map<DepartmentConsultationModel>(_depConsultationVm);

                if (!string.IsNullOrEmpty(_depConsultationVm.IconPath) && _depConsultationVm.IsIconPathUploaded)
                {
                    depCons.IconPath = "../Upload/DanpheCareCMS/" + _depConsultationVm.IconPath.Substring(_depConsultationVm.IconPath.LastIndexOf('\\') + 1);

                }
                depCons.UpdatedDate = DateTime.Now;
                _unitOfWork.DepartmentConsultationModels.Update(depCons);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(DepartmentConsultationViewModel))]
        public void DeleteDepartmentConsultation(int id)
        {

            var _conToDelete = _unitOfWork.DepartmentConsultationModels.GetAll().Where(x => x.DepartmentConsultationId == id).FirstOrDefault();
            try
            {
                _unitOfWork.DepartmentConsultationModels.Remove(_conToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Department Consultation

        #region Resourceful Articles

        //[HttpGet("{id}")]
        //public async Task<ActionResult> GetArticlesById(int id)
        //{
        //    var articles = await _unitOfWork.ResourcefulArticlesModels.GetAllAsync();
        //    //var doctor = _unitOfWork.DanpheCareDoctorModels.GetAll().ToList();
        //    //var query = (from doc in doctor
        //    //             join art in articles on doc.DoctorId equals art.DoctorId
        //    //             where doc.DoctorId == id
        //    //             select new
        //    //             {
        //    //                 doc.Designation,
        //    //                 doc.Experience,
        //    //                 doc.FullName,
        //    //                 doc.ImagePath,
        //    //                 art.CreatedDate,
        //    //                 art.Content,
        //    //                 art.DoctorId,
        //    //                 art.ResourcefulArticlesId,
        //    //                 ArtImage = art.ImagePath,
        //    //                 art.Title
        //    //             }).ToList();

        //    return Ok(articles);

        //}
        [HttpGet]
        public ActionResult GetArtiles()
        {
            try
            {
                var _getArticles = _unitOfWork.ResourcefulArticlesModels.GetAll().ToList();


                //var _getDoctor = _unitOfWork.DanpheCareDoctorModels.GetAll().ToList();
                //var _articlesList = (from articles in _getArticles
                //                     join doc in _getDoctor on articles.DoctorId equals doc.DoctorId
                //                         select new
                //                         {
                //                             articles.DoctorId,
                //                             articles.ResourcefulArticlesId,
                //                             articles.Title,
                //                             articles.Content,
                //                             articles.ImagePath,
                //                             doc.FullName,
                //                             articles.CreatedDate
                //                         }).OrderByDescending(x => x.CreatedDate).ToList();

                return Ok(_getArticles);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Produces(typeof(ResourcefulArticlesViewModel))]
        public IActionResult AddArticles([FromBody] ResourcefulArticlesViewModel _articlesVm)
        {
            try
            {
                var articles = _mapper.Map<ResourcefulArticlesModel>(_articlesVm);
                if (!string.IsNullOrEmpty(_articlesVm.ImagePath) )
                {
                    articles.ImagePath = "../Upload/DanpheCareCMS/" + _articlesVm.ImagePath.Substring(_articlesVm.ImagePath.LastIndexOf('\\') + 1);
                }
                articles.CreatedDate = DateTime.Now;
                _unitOfWork.ResourcefulArticlesModels.Add(articles);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(ResourcefulArticlesViewModel))]
        public ActionResult UpdateArticles([FromBody] ResourcefulArticlesViewModel _articlesVm)
        {
            try
            {
                var articles = _mapper.Map<ResourcefulArticlesModel>(_articlesVm);

                if (!string.IsNullOrEmpty(_articlesVm.ImagePath) && _articlesVm.IsImagePathUploaded)
                {
                    articles.ImagePath = "../Upload/DanpheCareCMS/" + _articlesVm.ImagePath.Substring(_articlesVm.ImagePath.LastIndexOf('\\') + 1);

                }
                articles.UpdatedDate = DateTime.Now;
                _unitOfWork.ResourcefulArticlesModels.Update(articles);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(ResourcefulArticlesViewModel))]
        public void DeleteArticles(int id)
        {

            var _articlesToDelete = _unitOfWork.ResourcefulArticlesModels.GetAll().Where(x => x.ResourcefulArticlesId == id).FirstOrDefault();
            try
            {
                _unitOfWork.ResourcefulArticlesModels.Remove(_articlesToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Resourceful Articles

        #region News

        [HttpGet("{id}")]
        public ActionResult GetNewsById(int id)
        {
            var news =  _unitOfWork.NewsModels.GetAll().Where(x=>x.NewsId == id).FirstOrDefault();

            return Ok(news);

        }

        public ActionResult GetNews()
        {
            try
            {
                var _getNews = _unitOfWork.NewsModels.GetAll();
                var _newsList = _mapper.Map<List<NewsModel>, List<NewsViewModel>>(_getNews.OrderByDescending(x => x.CreatedDate).ToList());

                return Ok(_newsList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Produces(typeof(NewsViewModel))]
        public IActionResult AddNews([FromBody] NewsViewModel _newsVm)
        {
            try
            {
                var news = _mapper.Map<NewsModel>(_newsVm);
                if (!string.IsNullOrEmpty(_newsVm.ImagePath) )
                {
                    news.ImagePath = "../Upload/DanpheCareCMS/" + _newsVm.ImagePath.Substring(_newsVm.ImagePath.LastIndexOf('\\') + 1);
                }
                news.CreatedDate = DateTime.Now;
                _unitOfWork.NewsModels.Add(news);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(NewsViewModel))]
        public ActionResult UpdateNews([FromBody] NewsViewModel _newsVm)
        {
            try
            {
                var news = _mapper.Map<NewsModel>(_newsVm);

                if (!string.IsNullOrEmpty(_newsVm.ImagePath) && _newsVm.IsImagePathUploaded)
                {
                    news.ImagePath = "../Upload/DanpheCareCMS/" + _newsVm.ImagePath.Substring(_newsVm.ImagePath.LastIndexOf('\\') + 1);

                }
                news.UpdatedDate = DateTime.Now;
                _unitOfWork.NewsModels.Update(news);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(NewsViewModel))]
        public void DeleteNews(int id)
        {

            var _newsToDelete = _unitOfWork.NewsModels.GetAll().Where(x => x.NewsId == id).FirstOrDefault();
            try
            {
                _unitOfWork.NewsModels.Remove(_newsToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion News

        #region Blogs

        [HttpGet("{id}")]
        public ActionResult GetBlogsById(int id)
        {
            var blog = _unitOfWork.BlogsModels.GetAll().Where(x => x.BlogId == id).FirstOrDefault();

            return Ok(blog);

        }
        [HttpGet]
        public ActionResult GetBlogs()
        {
            try
            {
                var _getBlogs = _unitOfWork.BlogsModels.GetAll();
                var _blogsList = _mapper.Map<List<BlogsModel>, List<BlogsViewModel>>(_getBlogs.OrderByDescending(x => x.CreatedDate).ToList());

                return Ok(_blogsList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Produces(typeof(BlogsViewModel))]
        public IActionResult AddBlogs([FromBody] BlogsViewModel _blogsVm)
        {
            try
            {
                var blogs = _mapper.Map<BlogsModel>(_blogsVm);

                if (!string.IsNullOrEmpty(_blogsVm.ImagePath) )
                {
                    blogs.ImagePath = "../Upload/DanpheCareCMS/" + _blogsVm.ImagePath.Substring(_blogsVm.ImagePath.LastIndexOf('\\') + 1);
                }
                blogs.CreatedDate = DateTime.Now;
                _unitOfWork.BlogsModels.Add(blogs);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(BlogsViewModel))]
        public ActionResult UpdateBlogs([FromBody] BlogsViewModel _blogsVm)
        {
            try
            {
                var blogs = _mapper.Map<BlogsModel>(_blogsVm);

                if (!string.IsNullOrEmpty(_blogsVm.ImagePath) && _blogsVm.IsImagePathUploaded)
                {
                    blogs.ImagePath = "../Upload/DanpheCareCMS/" + _blogsVm.ImagePath.Substring(_blogsVm.ImagePath.LastIndexOf('\\') + 1);

                }
                blogs.UpdatedDate = DateTime.Now;
                _unitOfWork.BlogsModels.Update(blogs);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(BlogsViewModel))]
        public void DeleteBlogs(int id)
        {

            var _blogsToDelete = _unitOfWork.BlogsModels.GetAll().Where(x => x.BlogId == id).FirstOrDefault();
            try
            {
                _unitOfWork.BlogsModels.Remove(_blogsToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Blogs

        #region Meta Tag

        [HttpGet("{id}")]
        public ActionResult GetMetaTagById(int id)
        {
            var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.MetaTagId == id).FirstOrDefault();

            return Ok(metaTag);

        }

        [HttpGet]
        public ActionResult GetMetaTag()
        {
            try
            {
                var _getMetaTag = _unitOfWork.MetaTagModels.GetAll();
                var _metaTagList = _mapper.Map<List<MetaTagModel>, List<MetaTagViewModel>>(_getMetaTag.ToList());

                return Ok(_metaTagList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        [HttpGet]
        public ActionResult BlogMetaTag()
        {
            try
            {
                var _getMetaTag = _unitOfWork.MetaTagModels.GetAll().Where(x=>x.BlogId != 0).ToList();
                var blog = _unitOfWork.BlogsModels.GetAll().ToList();
                var blogMetaTag = _getMetaTag.GroupJoin(blog, a => a.BlogId, b => b.BlogId, (a, b) => new MetaTagViewModel
                {
                    BlogId = a.BlogId,
                    BlogName = b.Select(s => s.Title).FirstOrDefault(),
                    Title = a.Title,
                    Content = a.Content,
                    Keywords = a.Keywords,
                    MetaTagId = a.MetaTagId,
                    Page = a.Page
                }).ToList();
                //var _metaTagList = _mapper.Map<List<MetaTagModel>, List<MetaTagViewModel>>(_getMetaTag.ToList());

                return Ok(blogMetaTag);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        [HttpGet]
        public ActionResult DoctorMetaTag()
        {
            try
            {
                var _getMetaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.DoctorId != 0).ToList();
                var doctor = _unitOfWork.DanpheCareDoctorModels.GetAll().ToList();
                var doctorMetaTag = _getMetaTag.GroupJoin(doctor, a => a.DoctorId, b => b.DoctorId, (a, b) => new MetaTagViewModel
                {
                    DoctorId = a.DoctorId,
                    DoctorName = b.Select(s => s.FullName).FirstOrDefault(),
                    Title = a.Title,
                    Content = a.Content,
                    Keywords = a.Keywords,
                    MetaTagId = a.MetaTagId,
                    Page = a.Page
                }).ToList();
                //var _metaTagList = _mapper.Map<List<MetaTagModel>, List<MetaTagViewModel>>(_getMetaTag.ToList());

                return Ok(doctorMetaTag);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        [HttpGet]
        public ActionResult ServiceMetaTag()
        {
            try
            {
                var _getMetaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.ServiceId != 0).ToList();
                var services = _unitOfWork.OurServices.GetAll().ToList();
                var serviceMetaTag = _getMetaTag.GroupJoin(services, a => a.ServiceId, b => b.ServiceId, (a, b) => new MetaTagViewModel
                {
                    ServiceId = a.ServiceId,
                    ServiceName = b.Select(s => s.ServiceName).FirstOrDefault(),
                    Title = a.Title,
                    Content = a.Content,
                    Keywords = a.Keywords,
                    MetaTagId = a.MetaTagId,
                    Page = a.Page
                }).ToList();
                //var _metaTagList = _mapper.Map<List<MetaTagModel>, List<MetaTagViewModel>>(_getMetaTag.ToList());

                return Ok(serviceMetaTag);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        [HttpGet]
        public ActionResult DepartmentMetaTag()
        {
            try
            {
                var _getMetaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.DepartmentId != 0).ToList();
                var department = _unitOfWork.DepartmentModels.GetAll().ToList();
                var departmentMetaTag = _getMetaTag.GroupJoin(department, a => a.DepartmentId, b => b.DepartmentId, (a, b) => new MetaTagViewModel
                {
                    DepartmentId = a.DepartmentId,
                    DepartmentName = b.Select(s => s.DepartmentName).FirstOrDefault(),
                    Title = a.Title,
                    Content = a.Content,
                    Keywords = a.Keywords,
                    MetaTagId = a.MetaTagId,
                    Page = a.Page
                }).ToList();
                //var _metaTagList = _mapper.Map<List<MetaTagModel>, List<MetaTagViewModel>>(_getMetaTag.ToList());

                return Ok(departmentMetaTag);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [Produces(typeof(MetaTagViewModel))]
        public IActionResult AddMetaTag([FromBody] MetaTagViewModel _metaTagVm)
        {
            try
            {
                var metaTag = _mapper.Map<MetaTagModel>(_metaTagVm);
                metaTag.CreatedDate = DateTime.Now;
                _unitOfWork.MetaTagModels.Add(metaTag);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(MetaTagViewModel))]
        public ActionResult UpdateMetaTags([FromBody] MetaTagViewModel _metaTagVm)
        {
            try
            {
                var metaTags = _mapper.Map<MetaTagModel>(_metaTagVm);
                metaTags.UpdatedDate = DateTime.Now;
                _unitOfWork.MetaTagModels.Update(metaTags);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(MetaTagViewModel))]
        public void DeleteMetaTag(int id)
        {

            var _metaTagToDelete = _unitOfWork.MetaTagModels.GetAll().Where(x => x.MetaTagId == id).FirstOrDefault();
            try
            {
                _unitOfWork.MetaTagModels.Remove(_metaTagToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Meta Tag

        #region DanpheCare Contact

        [HttpGet("{id}")]
        public ActionResult GetContactById(int id)
        {
            var contact = _unitOfWork.DanpheCareContactModels.GetAll().Where(x => x.DanpheCareContactId == id).FirstOrDefault();

            return Ok(contact);

        }
        public ActionResult GetDanpheCareContact()
        {
            try
            {
                var _getContact = _unitOfWork.DanpheCareContactModels.GetAll();
                var _contactList = _mapper.Map<List<DanpheCareContactModel>, List<DanpheCareContactViewModel>>(_getContact.ToList());

                return Ok(_contactList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(DanpheCareContactViewModel))]
        public IActionResult AddDanpheContact([FromBody] DanpheCareContactViewModel _contactVm)
        {
            try
            {
                var contact = _mapper.Map<DanpheCareContactModel>(_contactVm);
                contact.CreatedDate = DateTime.Now;
                _unitOfWork.DanpheCareContactModels.Add(contact);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(DanpheCareContactViewModel))]
        public ActionResult UpdateContact([FromBody] DanpheCareContactViewModel _contactVm)
        {
            try
            {
                var contact = _mapper.Map<DanpheCareContactModel>(_contactVm);
                contact.UpdatedDate = DateTime.Now;
                _unitOfWork.DanpheCareContactModels.Update(contact);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(MetaTagViewModel))]
        public void DeleteContact(int id)
        {

            var _contactToDelete = _unitOfWork.DanpheCareContactModels.GetAll().Where(x => x.DanpheCareContactId == id).FirstOrDefault();
            try
            {
                _unitOfWork.DanpheCareContactModels.Remove(_contactToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Danphe Care Contact

        #region Webinar
        public ActionResult GetWebinar()
        {
            try
            {
                var _getWebinar = _unitOfWork.WebinarModels.GetAll();
                var _webinarList = _mapper.Map<List<WebinarModel>, List<WebinarViewModel>>(_getWebinar.OrderByDescending(x => x.CreatedDate).ToList());

                return Ok(_webinarList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(WebinarViewModel))]
        public IActionResult AddWebinar([FromBody] WebinarViewModel _webinarVm)
        {
            try
            {
                var webinar = _mapper.Map<WebinarModel>(_webinarVm);
                webinar.CreatedDate = DateTime.Now;
                _unitOfWork.WebinarModels.Add(webinar);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(WebinarViewModel))]
        public ActionResult UpdateWebinar([FromBody] WebinarViewModel _webinarVm)
        {
            try
            {
                var webinar = _mapper.Map<WebinarModel>(_webinarVm);
                webinar.UpdatedDate = DateTime.Now;
                _unitOfWork.WebinarModels.Update(webinar);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(WebinarViewModel))]
        public void DeleteWebinar(int id)
        {

            var _webinarToDelete = _unitOfWork.WebinarModels.GetAll().Where(x => x.WebinarId == id).FirstOrDefault();
            try
            {
                _unitOfWork.WebinarModels.Remove(_webinarToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Webinar

        #region Expat
        public ActionResult GetExpat()
        {
            try
            {
                var _getExpat = _unitOfWork.ExpatModels.GetAll();
                var _expatList = _mapper.Map<List<ExpatModel>, List<ExpatViewModel>>(_getExpat.OrderByDescending(x => x.CreatedDate).ToList());

                return Ok(_expatList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(ExpatViewModel))]
        public IActionResult AddExpat([FromBody] ExpatViewModel _expatVm)
        {
            try
            {
                var expat = _mapper.Map<ExpatModel>(_expatVm);
                if (!string.IsNullOrEmpty(_expatVm.ImagePath) )
                {
                    expat.ImagePath = "../Upload/DanpheCareCMS/" + _expatVm.ImagePath.Substring(_expatVm.ImagePath.LastIndexOf('\\') + 1);
                }
                expat.CreatedDate = DateTime.Now;
                _unitOfWork.ExpatModels.Add(expat);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(ExpatViewModel))]
        public ActionResult UpdateExpat([FromBody] ExpatViewModel _expatVm)
        {
            try
            {
                var expat = _mapper.Map<ExpatModel>(_expatVm);

                if (!string.IsNullOrEmpty(_expatVm.ImagePath) && _expatVm.IsImagePathUploaded)
                {
                    expat.ImagePath = "../Upload/DanpheCareCMS/" + _expatVm.ImagePath.Substring(_expatVm.ImagePath.LastIndexOf('\\') + 1);

                }
                expat.UpdatedDate = DateTime.Now;
                _unitOfWork.ExpatModels.Update(expat);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(ExpatViewModel))]
        public void DeleteExpat(int id)
        {

            var _expatToDelete = _unitOfWork.ExpatModels.GetAll().Where(x => x.ExpatId == id).FirstOrDefault();
            try
            {
                _unitOfWork.ExpatModels.Remove(_expatToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Expat

        #region Department Tab Content
        public ActionResult GetContent()
        {
            try
            {
                var _getContent = _unitOfWork.DepartmentTabContents.GetAll().ToList();
                var _getDepartment = _unitOfWork.DepartmentModels.GetAll().ToList();
                var _getTab = _unitOfWork.DepartmentTabs.GetAll().ToList();
                var _contentList = (from content in _getContent
                                         join dep in _getDepartment on content.DepartmentId equals dep.DepartmentId
                                         join tab in _getTab on content.DepartmentTabId equals tab.DepartmentTabId
                                         select new
                                         {
                                             content.DepartmentContentId,
                                             content.DepartmentId,
                                             content.DepartmentTabId,
                                             content.Title,
                                             content.Content,
                                             dep.DepartmentName,
                                             DepartmentTabName = tab.TabName,
                                             content.CreatedDate

                                         }).OrderByDescending(x => x.CreatedDate).ToList();

                return Ok(_contentList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
           
        }
        public ActionResult GetTab()
        {
            try
            {
                var _getDeptTabs = _unitOfWork.DepartmentTabs.GetAll().ToList();
              

                return Ok(_getDeptTabs);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(DepartmentTabContentViewModel))]
        public IActionResult AddDepartmentTabContent([FromBody] DepartmentTabContentViewModel _tabContent)
        {
            try
            {
                var depContent = _mapper.Map<DepartmentTabContent>(_tabContent);
                depContent.CreatedDate = DateTime.Now;
                _unitOfWork.DepartmentTabContents.Add(depContent);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(DepartmentTabContentViewModel))]
        public ActionResult UpdateDepartmentTabContent([FromBody] DepartmentTabContentViewModel _tabContentVm)
        {
            try
            {
                var tabContent = _mapper.Map<DepartmentTabContent>(_tabContentVm);
                tabContent.UpdatedDate = DateTime.Now;
                _unitOfWork.DepartmentTabContents.Update(tabContent);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(DepartmentTabContentViewModel))]
        public void DeleteTabContent(int id)
        {

            var _contentToDelete = _unitOfWork.DepartmentTabContents.GetAll().Where(x => x.DepartmentContentId == id).FirstOrDefault();
            try
            {
                _unitOfWork.DepartmentTabContents.Remove(_contentToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Department Tab Content

        #region Danphe Care Department Sub Heading

        [HttpGet("{id}")]
        public async Task<ActionResult> GetSubHeadingByDepartmentId(int id)
        {
            var deps = await _unitOfWork.DepartmentSubHeadings.GetAllAsync();
            var dep = _unitOfWork.DepartmentModels.GetAll().Where(x=>x.DepartmentId == id).ToList();
            var query = (from depSubHeading in deps
                         join department in dep on depSubHeading.DepartmentId equals department.DepartmentId
                         where depSubHeading.DepartmentId == id
                         select new
                         {
                             depSubHeading.Title,
                             depSubHeading.ShortDescription,
                             depSubHeading.IconPath,
                             depSubHeading.DepartmentSubHeadingId,
                             depSubHeading.DepartmentId
                            
                         }).ToList();

            return Ok(query);

        }

        public ActionResult GetDepartmentSubHeading()
        {
            try
            {
                var deps =  _unitOfWork.DepartmentSubHeadings.GetAll().ToList();
                var dep = _unitOfWork.DepartmentModels.GetAll().ToList();
                var query = (from depSubHeading in deps
                             join department in dep on depSubHeading.DepartmentId equals department.DepartmentId
                            
                             select new
                             {
                                 depSubHeading.Title,
                                 depSubHeading.ShortDescription,
                                 depSubHeading.IconPath,
                                 depSubHeading.DepartmentSubHeadingId,
                                 depSubHeading.DepartmentId,
                                 department.DepartmentName,
                                 depSubHeading.CreatedDate

                             }).OrderByDescending(x=>x.CreatedDate).ToList();

                return Ok(query);

            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(DepartmentSubHeadingViewModel))]
        public IActionResult AddDepartmentSubHeading([FromBody] DepartmentSubHeadingViewModel _danpheCareDept)
        {
            try
            {
                var danpheDept = _mapper.Map<DepartmentSubHeading>(_danpheCareDept);

                if (!string.IsNullOrEmpty(_danpheCareDept.IconPath))
                {
                    danpheDept.IconPath = "../Upload/DanpheCareCMS/" + _danpheCareDept.IconPath.Substring(_danpheCareDept.IconPath.LastIndexOf('\\') + 1);
                }
                danpheDept.CreatedDate = DateTime.Now;
                _unitOfWork.DepartmentSubHeadings.Add(danpheDept);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(DepartmentSubHeadingViewModel))]
        public ActionResult UpdateDepartmentSubHeading([FromBody] DepartmentSubHeadingViewModel _danpheCareDeptVm)
        {
            try
            {
                var danpheDept = _mapper.Map<DepartmentSubHeading>(_danpheCareDeptVm);

                if (!string.IsNullOrEmpty(_danpheCareDeptVm.IconPath) && _danpheCareDeptVm.IsIconPathUploaded)
                {
                    danpheDept.IconPath = "../Upload/DanpheCareCMS/" + _danpheCareDeptVm.IconPath.Substring(_danpheCareDeptVm.IconPath.LastIndexOf('\\') + 1);

                }
                danpheDept.UpdatedDate = DateTime.Now;
                _unitOfWork.DepartmentSubHeadings.Update(danpheDept);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(DepartmentSubHeadingViewModel))]
        public void DeleteDepartmentSubHeading(int id)
        {

            var _departmentToDelete = _unitOfWork.DepartmentSubHeadings.GetAll().Where(x => x.DepartmentSubHeadingId == id).FirstOrDefault();
            try
            {
                _unitOfWork.DepartmentSubHeadings.Remove(_departmentToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Danphe Care Department SubHeading

        #region Package Name

        [HttpGet("{id}")]
        public ActionResult GetPackageName(int id)
        {
            var packageName = _unitOfWork.PackageNameModels.GetAll().Where(s=>s.PackageNameModelId == id).FirstOrDefault(); ;
            return Ok(packageName);
        }

        public ActionResult GetPackageNameList()
        {
            try
            {
                var packageName = _unitOfWork.PackageNameModels.GetAll().ToList();
               

                return Ok(packageName);

            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(PackageNameViewModel))]
        public IActionResult AddPackageName([FromBody] PackageNameViewModel _packageNameVm)
        {
            try
            {
                var packageVm = _mapper.Map<PackageNameModel>(_packageNameVm);
                packageVm.CreatedDate = DateTime.Now;
                _unitOfWork.PackageNameModels.Add(packageVm);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(PackageNameViewModel))]
        public ActionResult UpdatePackageName([FromBody] PackageNameViewModel _packageNameVm)
        {
            try
            {
                var packageName = _mapper.Map<PackageNameModel>(_packageNameVm);
                packageName.UpdatedDate = DateTime.Now;
                _unitOfWork.PackageNameModels.Update(packageName);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(PackageNameViewModel))]
        public void DeletePackageName(int id)
        {

            var _packageToDelete = _unitOfWork.PackageNameModels.GetAll().Where(x => x.PackageNameModelId == id).FirstOrDefault();
            try
            {
                _unitOfWork.PackageNameModels.Remove(_packageToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Package Details

        #region Package Details

        [HttpGet("{id}")]
        public ActionResult GetPackageDetailsById(int id)
        {
            var packageName =  _unitOfWork.PackageNameModels.GetAll().ToList(); ;
            var packageDetail = _unitOfWork.PackageDetails.GetAll().Where(x => x.PackageNameModelId == id).ToList();
            var query = (from pkgDetails in packageDetail
                         join pkgName in packageName on pkgDetails.PackageNameModelId equals pkgName.PackageNameModelId
                         where pkgName.PackageNameModelId == id
                         select new
                         {
                             pkgDetails.Title,
                             pkgDetails.Content,
                             pkgName.PackageName,
                             pkgName.PackageNameModelId,
                             pkgDetails.PackageDetailId

                         }).ToList();

            return Ok(query);

        }

        public ActionResult GetPackageList()
        {
            try
            {
                var packageName = _unitOfWork.PackageNameModels.GetAll().ToList();
                var packageDetails = _unitOfWork.PackageDetails.GetAll().ToList();
                var query = (from pkgDetails in packageDetails
                             join pkgName in packageName on pkgDetails.PackageNameModelId equals pkgName.PackageNameModelId
                             select new
                             {
                                 pkgDetails.Title,
                                 pkgDetails.Content,
                                 pkgName.PackageName,
                                 pkgDetails.CreatedDate
                             }).OrderByDescending(x => x.CreatedDate).ToList();

                return Ok(query);

            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(PackageDetailsViewModel))]
        public IActionResult AddPackageDetails([FromBody] PackageDetailsViewModel _packageDetailsVm)
        {
            try
            {
                var packageVm = _mapper.Map<PackageDetails>(_packageDetailsVm);
                packageVm.CreatedDate = DateTime.Now;
                _unitOfWork.PackageDetails.Add(packageVm);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(PackageDetailsViewModel))]
        public ActionResult UpdatePackageDetails([FromBody] PackageDetailsViewModel _packageDetailsVm)
        {
            try
            {
                var packageDetails = _mapper.Map<PackageDetails>(_packageDetailsVm);
                packageDetails.UpdatedDate = DateTime.Now;
                _unitOfWork.PackageDetails.Update(packageDetails);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(PackageDetailsViewModel))]
        public void DeletePackageDetails(int id)
        {

            var _packageToDelete = _unitOfWork.PackageDetails.GetAll().Where(x => x.PackageDetailId == id).FirstOrDefault();
            try
            {
                _unitOfWork.PackageDetails.Remove(_packageToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Package Details

        #region Sub Services

        [HttpGet("{id}")]
        public ActionResult GetSubServicesById(int id)
        {
            var services = _unitOfWork.OurServices.GetAll().ToList(); ;
            var subservices = _unitOfWork.SubServices.GetAll().Where(x => x.OurServicesId == id).ToList();
            var query = (from subserv in subservices
                         join service in services on subserv.OurServicesId equals service.ServiceId
                         where service.ServiceId == id
                         select new
                         {
                             subserv.Title,
                             subserv.ImagePath,
                             subserv.OurServicesId,
                             subserv.SubServicesId,
                             service.ServiceName

                         }).ToList();

            return Ok(query);

        }

        public ActionResult GetSubServicesList()
        {
            try
            {
                var services = _unitOfWork.OurServices.GetAll().ToList(); ;
                var subservices = _unitOfWork.SubServices.GetAll().ToList();
                var query = (from subserv in subservices
                             join service in services on subserv.OurServicesId equals service.ServiceId
                        
                             select new
                             {
                                 subserv.Title,
                                 subserv.ImagePath,
                                 subserv.OurServicesId,
                                 subserv.SubServicesId,
                                 service.ServiceName,
                                 subserv.CreatedDate
                             }).OrderByDescending(x => x.CreatedDate).ToList();

                return Ok(query);
               
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(SubServicesViewModel))]
        public IActionResult AddSubServices([FromBody] SubServicesViewModel _subServices)
        {
            try
            {
                var subServices = _mapper.Map<SubServices>(_subServices);
                if (!string.IsNullOrEmpty(_subServices.ImagePath))
                {
                    subServices.ImagePath = "../Upload/DanpheCareCMS/" + _subServices.ImagePath.Substring(_subServices.ImagePath.LastIndexOf('\\') + 1);
                }

                subServices.CreatedDate = DateTime.Now;
                _unitOfWork.SubServices.Add(subServices);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(SubServicesViewModel))]
        public ActionResult UpdateSubServices([FromBody] SubServicesViewModel _subServicesVm)
        {
            try
            {
                var subservices = _mapper.Map<SubServices>(_subServicesVm);

                if (!string.IsNullOrEmpty(_subServicesVm.ImagePath) && _subServicesVm.IsImagePathUploaded)
                {
                    subservices.ImagePath = "../Upload/DanpheCareCMS/" + _subServicesVm.ImagePath.Substring(_subServicesVm.ImagePath.LastIndexOf('\\') + 1);

                }

                subservices.UpdatedDate = DateTime.Now;
                _unitOfWork.SubServices.Update(subservices);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(SubServicesViewModel))]
        public void DeleteSubServices(int id)
        {

            var _subServicesToDelete = _unitOfWork.SubServices.GetAll().Where(x => x.SubServicesId == id).FirstOrDefault();
            try
            {
                _unitOfWork.SubServices.Remove(_subServicesToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Sub Services

        #region Sub Services Details

        [HttpGet("{id}")]
        public ActionResult GetSubServicesDetailsById(int id)
        {
            var subservices = _unitOfWork.SubServices.GetAll().ToList(); ;
            var subservicesdetails = _unitOfWork.SubServicesDetails.GetAll().Where(x => x.SubServicesDetailsId == id).ToList();
            var query = (from subserdetails in subservicesdetails
                         join subservice in subservices on subserdetails.SubServicesId equals subservice.SubServicesId
                         where subservice.SubServicesId == id
                         select new
                         {
                             subserdetails.SubTitle,
                             subserdetails.ImagePath,
                             subserdetails.Introduction,
                             subserdetails.Content,
                             subserdetails.SubServicesDetailsId,
                             subserdetails.SubServicesId,
                             subservice.Title
                         }).ToList();

            return Ok(query);

        }

        public ActionResult GetSubServicesDetailsList()
        {
            try
            {
                var subservices = _unitOfWork.SubServices.GetAll().ToList(); ;
                var subservicesdetails = _unitOfWork.SubServicesDetails.GetAll().ToList();
                var query = (from subserdetails in subservicesdetails
                             join subservice in subservices on subserdetails.SubServicesId equals subservice.SubServicesId
                             select new
                             {
                                 subserdetails.SubTitle,
                                 subserdetails.ImagePath,
                                 subserdetails.Introduction,
                                 subserdetails.Content,
                                 subserdetails.SubServicesDetailsId,
                                 subserdetails.SubServicesId,
                                 subservice.Title,
                                 subserdetails.CreatedDate
                             }).OrderByDescending(x => x.CreatedDate).ToList();

                return Ok(query);

 

            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        [Produces(typeof(SubServicesDetailsViewModel))]
        public IActionResult AddSubServicesDetails([FromBody] SubServicesDetailsViewModel _subServicesDetails)
        {
            try
            {
                var subServicesDetails = _mapper.Map<SubServicesDetails>(_subServicesDetails);
                if (!string.IsNullOrEmpty(_subServicesDetails.ImagePath))
                {
                    subServicesDetails.ImagePath = "../Upload/DanpheCareCMS/" + _subServicesDetails.ImagePath.Substring(_subServicesDetails.ImagePath.LastIndexOf('\\') + 1);
                }

                subServicesDetails.CreatedDate = DateTime.Now;
                _unitOfWork.SubServicesDetails.Add(subServicesDetails);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(SubServicesDetailsViewModel))]
        public ActionResult UpdateSubServicesDetails([FromBody] SubServicesDetailsViewModel __subServicesDetailsVm)
        {
            try
            {
                var subservicesdetails = _mapper.Map<SubServicesDetails>(__subServicesDetailsVm);

                if (!string.IsNullOrEmpty(__subServicesDetailsVm.ImagePath) && __subServicesDetailsVm.IsImagePathUploaded)
                {
                    subservicesdetails.ImagePath = "../Upload/DanpheCareCMS/" + __subServicesDetailsVm.ImagePath.Substring(__subServicesDetailsVm.ImagePath.LastIndexOf('\\') + 1);

                }

                subservicesdetails.UpdatedDate = DateTime.Now;
                _unitOfWork.SubServicesDetails.Update(subservicesdetails);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(SubServicesDetailsViewModel))]
        public void DeleteSubServicesDetails(int id)
        {

            var _subServicesDetailsToDelete = _unitOfWork.SubServicesDetails.GetAll().Where(x => x.SubServicesDetailsId == id).FirstOrDefault();
            try
            {
                _unitOfWork.SubServicesDetails.Remove(_subServicesDetailsToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        #endregion Sub Services Details

    }
}
