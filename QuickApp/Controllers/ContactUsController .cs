﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TestTele.ViewModel;
using AutoMapper;
using Telemedicine.ServerModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using QuickApp.Controllers;
using DAL;

namespace TestTele.Controllers
{
    //[Produces("application/json")]
    [Route("api/[controller]/{action}")] 
    public class ContactUsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;     
        private readonly ILogger<AccountController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public ContactUsController( ILogger<AccountController> logger, IUnitOfWork unitOfWork, IMapper mapper,
                                  IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;   
            _logger = logger;
            _mapper = mapper;
            _configuration = configuration;
        }

        [HttpGet]
        [Produces(typeof(List<ContactUsViewModel>))]
        public IActionResult GetlistOfContactedUsr()
        {
            try
            {
                var Contactlist = _unitOfWork.ContacUs.GetAll();
                var contactusrlistVM = _mapper.Map<List<ContactUs>, List<ContactUsViewModel>>(Contactlist.ToList());

                return Ok(contactusrlistVM);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
          

        }

        [HttpPost]      
        public IActionResult PostContactformdata([FromBody] ContactUsViewModel contact)
        {
            try
            {              
                    if (ModelState.IsValid)
                    {
                    ContactUs contUs = new ContactUs
                    {                                
                                Name = contact.Name,                             
                                ContactNumber = contact.ContactNumber,
                                Email = contact.Email,
                                Company = contact.Company,
                                Massage = contact.Massage                               

                };
                            contUs.CreatedOn = DateTime.Now;
                            _unitOfWork.ContacUs.Add(contUs);
                            _unitOfWork.SaveChanges();
                             return Ok(StatusCode(StatusCodes.Status200OK));

                }
                    else
                    {
                    //model state not valid                   
                    return BadRequest(ModelState);
                }

            }
            catch (Exception ex)
            {
                // Transaction not commited if any errors occurs
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            //return Created("", "User Registered");
            //return Ok();
        }




    }
}
