﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TestTele.ViewModel;
using AutoMapper;
using Telemedicine.ServerModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mail;
using MailKit.Net.Smtp;
using MimeKit;
using DAL;
using QuickApp.Controllers;

namespace TestTele.Controllers
{
    //[Produces("application/json")]
    [Route("api/[controller]/{action}")] 
    public class DanphecareController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;     
        private readonly ILogger<AccountController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public DanphecareController( ILogger<AccountController> logger, IUnitOfWork unitOfWork, IMapper mapper,
                                  IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;   
            _logger = logger;
            _configuration = configuration;
            _mapper = mapper;
        }

        [HttpGet]
        [Produces(typeof(List<DanphecareViewModel>))]
        public IActionResult GetAppliedUsrlist()
        {
            try
            {
                var Userlist = _unitOfWork.Danphecare.GetAll();
                var usrlistVM = _mapper.Map<List<Danphecare>, List<DanphecareViewModel>>(Userlist.ToList());
                var result = (from usr in usrlistVM
                              select new
                              {
                                  usr.UserId,                                
                                  UserName = usr.FirstName + " " + (String.IsNullOrEmpty(usr.MiddleName) ? " " : usr.MiddleName) + " " + usr.LastName,
                                 usr.ContactNumber,
                                 usr.Email,
                                 usr.Service,
                                 usr.Price,
                                 usr.PaymentProvider,
                                 usr.CreatedOn
                              }).ToList();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }           

        }

        [HttpPost]      
        public IActionResult ApplyToIsolation([FromBody] DanphecareViewModel danphecare)
        {
            try
            {              
                    if (ModelState.IsValid)
                    {
                    Danphecare dhcare = new Danphecare
                    {
                                UserId = danphecare.UserId,
                                FirstName = danphecare.FirstName,
                                MiddleName = danphecare.MiddleName,
                                LastName = danphecare.LastName,                              
                                FullName = danphecare.FullName,                              
                                ContactNumber = danphecare.ContactNumber,
                                Message = danphecare.Message,
                                Email = danphecare.Email,
                                ServiceId = danphecare.ServiceId,
                                Service = danphecare.Service,
                                Price = danphecare.Price,
                                PaymentProvider = danphecare.PaymentProvider
                };
                            dhcare.CreatedOn = DateTime.Now;
                            _unitOfWork.Danphecare.Add(dhcare);
                            _unitOfWork.SaveChanges();
                    EmailMessage email = new EmailMessage();
                    email.Reciever = new MailboxAddress(danphecare.FullName, danphecare.Email);
                    var sender = _configuration["NotificationMetadata:Sender"];
                    email.Sender = new MailboxAddress("Danphe Care", sender);
                    email.Subject = "Home Isolation Care Booking";
                    email.Content = "Your application for Apply For Home Isolation Care is Accepted and Stay Connected. We will call you back. Thank You !";
                    //var mimeMessage = CreateMimeMessageFromEmailMessageAFHI(email,  dhcare);
                    using (MailKit.Net.Smtp.SmtpClient smtpClient = new MailKit.Net.Smtp.SmtpClient())
                    {
                        var smtpserver = _configuration["NotificationMetadata:SmtpServer"];
                        var port = _configuration["NotificationMetadata:Port"];
                        var username = _configuration["NotificationMetadata:Username"];
                        var pwd = _configuration["NotificationMetadata:Password"];
                        smtpClient.Connect(smtpserver,int.Parse(port),true);
                        smtpClient.Authenticate(username, pwd);
                        //smtpClient.Send(mimeMessage);
                        smtpClient.Disconnect(true);
                    }
                    return Ok(StatusCode(StatusCodes.Status200OK));

                }
                    else
                    {
                    //model state not valid                   
                    return BadRequest(ModelState);
                }

            }
            catch (Exception ex)
            {
               
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
           
           // return Ok();
        }

        [HttpPost]
        public IActionResult ReachUsQuickly([FromBody] ReachUsQuicklyViewModel danphecareruq)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DanphecareReachUsQuickly dhcareruq = new DanphecareReachUsQuickly
                    {
                        UserId = Guid.NewGuid(),
                        Name = danphecareruq.Name,
                        Email = danphecareruq.Email,
                        Phone = danphecareruq.Phone,
                        Company = danphecareruq.Company,
                        Message = danphecareruq.Message
                        
                    };
                    dhcareruq.CreatedOn = DateTime.Now;
                    _unitOfWork.DanphecareReachUsQuickly.Add(dhcareruq);
                    _unitOfWork.SaveChanges();
                    EmailMessage email = new EmailMessage();
                    var sender = _configuration["NotificationMetadata:Sender"];
                    email.Reciever = new MailboxAddress("DanpheCareVisitor", sender);
                    email.Sender = new MailboxAddress("Danphe Care", sender);
                    email.Subject = "Somebody Want To Connect";
                    if (danphecareruq.Message != null)
                    {
                        email.Content = "Hello Danphecare ! You receive msg from" + " " + danphecareruq.Name + " " + "Email" + " " + danphecareruq.Email + " " + "Phone:" + "  " + danphecareruq.Phone + " " + "with message" + " " + danphecareruq.Message;
                    }
                    if (danphecareruq.Message == null && danphecareruq.Company == null)
                    {
                        email.Content = "Hello Danphecare ! You receive msg from" + " " + danphecareruq.Name + " " + "Email" + " " + danphecareruq.Email + " " + "Phone:" + "  " + danphecareruq.Phone ;
                    }
                    if ( danphecareruq.Company != null)
                    {
                        email.Content = "Hello Danphecare ! You receive msg from" + " " + danphecareruq.Name + " " + "Email" + " " + danphecareruq.Email + " " + "Phone:" + "  " + danphecareruq.Phone + " " + "with Services needed" + " "+ danphecareruq.Company;
                    }
                    var mimeMessage = CreateMimeMessageFromEmailMessage(email);
                    using (MailKit.Net.Smtp.SmtpClient smtpClient = new MailKit.Net.Smtp.SmtpClient())
                    {
                        var smtpserver = _configuration["NotificationMetadata:SmtpServer"];
                        var port = _configuration["NotificationMetadata:Port"];
                        var username = _configuration["NotificationMetadata:Username"];
                        var pwd = _configuration["NotificationMetadata:Password"];
                        smtpClient.Connect(smtpserver, int.Parse(port), true);
                        smtpClient.Authenticate(username, pwd);
                        smtpClient.Send(mimeMessage);
                        smtpClient.Disconnect(true);
                    }
                    EmailMessage replymail = new EmailMessage();
                    var rep_sender = _configuration["NotificationMetadata:Sender"];
                    if (danphecareruq.Email != null)
                    {
                        email.Reciever = new MailboxAddress(danphecareruq.Name, danphecareruq.Email);
                        email.Sender = new MailboxAddress("Danphe Care Admin", sender);
                        email.Subject = "Response of Mail";
                        //email.Content = "Hello " + " " + danphecareruq.Name + " " + "!" + " " + "We received mail from you. We will response you back. Thank You !";
                        //var rep_mimeMessage = CreateMimeMessageFromEmailMessage(email);
                        var rep_mimeMessage = CreateMimeMessageFromEmailMessageAFHI(email, danphecareruq);
                        using (MailKit.Net.Smtp.SmtpClient smtpClient = new MailKit.Net.Smtp.SmtpClient())
                        {
                            var smtpserver = _configuration["NotificationMetadata:SmtpServer"];
                            var port = _configuration["NotificationMetadata:Port"];
                            var username = _configuration["NotificationMetadata:Username"];
                            var pwd = _configuration["NotificationMetadata:Password"];
                            smtpClient.Connect(smtpserver, int.Parse(port), true);
                            smtpClient.Authenticate(username, pwd);
                            smtpClient.Send(rep_mimeMessage);
                            smtpClient.Disconnect(true);
                        }
                    }

                    return Ok(StatusCode(StatusCodes.Status200OK));

                }
                else
                {
                    //model state not valid                   
                    return BadRequest(ModelState);
                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // return Ok();
        }
        private MimeMessage CreateMimeMessageFromEmailMessage(EmailMessage message)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(message.Sender);
            mimeMessage.To.Add(message.Reciever);
            mimeMessage.Subject = message.Subject;
            mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Text)
            { Text = message.Content };
            return mimeMessage;
        }
        private MimeMessage CreateMimeMessageFromEmailMessageAFHI(EmailMessage message, ReachUsQuicklyViewModel dhcare)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(message.Sender);
            mimeMessage.To.Add(message.Reciever);
            mimeMessage.Subject = message.Subject;
            var bodyBuilder = new BodyBuilder();
            //string Str = "<b> Dear " + " " + dhcare.FullName +" ! </b>";
            //Str += "<br/>";
            //Str += "<br/>";
            //Str += "<i>Welcome to Danphe Care! </i>";
            //Str += "<br/>";
            //Str += "Danphe Care would like to thank you for choosing our services to guide you during Home Isolation.";
            //Str += "<br/>";
            //Str += "<br/>";
            //Str += "During the 15 days,  we provide: ";
            //Str += "<br/>"; 
            //Str += " ◦  One time Orientation about Infection Control and Safety measures, signs and symptoms of COVID-19 and dietary consultation.";
            //Str += "<br/>";
            //Str += " ◦  Daily group meditation session";
            //Str += "<br/>";
            //Str += " ◦  Daily monitoring and reporting.";
            //Str += "<br/>";
            //Str += " ◦  Need based Doctors’ Intervention and Clinical Discussions.";
            //Str += "<br/>";
            //Str += " ◦ Need based Psychological Counselling";
            //Str += "<br/>";
            //Str += "<br/>";
            //Str += "The link for your self assessment form has been attached here.";
            //Str += "<br/>";
            //Str += "Kindly find the form, fill it up and submit your response.";
            //Str += "<br/>";
            //Str += "Self Assessment form is used for history verification of the patient before doctor consultation.";
            //Str += "<br/>";
            //Str += "This is the first tool which will be used as a first means of communication between doctor and patient which has the medical evaluation in details.";
            // Str += "<br/>";
            //Str += "The evaluation forms includes Demographic Profile and Symptom Tracing, Past Medical History, Physical Assessment and Mental Assessment.";
            //Str += "<br/>";
            //Str += "<br/>";
            //Str += "<b><u>LINK </u></b>: https://docs.google.com/forms/d/1vtU6vmpU1pWR6p_qjA71WDfIaxWX9sGMAUdUMGS5c_Q/edit?ts=5fb0cbb9&gxids=7757";
            //Str += "<br/>";
            //Str += "<br/>";
            //Str += "<br/>";
            //Str += "<b><u>PAYMENT DETAILS </u></b>";
            //Str += "<br/>";
            //Str += "➢ The charge for 15 days is <b>NRs. 3000</b>. Payment can be done through:";
            //Str += "<br/>";
            //Str += "<b>⊛  ESEWA</b>";
            //Str += "<br/>";
            //Str += "➢ Esewa Id : Sagar KC: 9846071309";
            //Str += "<br/>";
            //Str += "<b>⊛ NIC Asia</b>";
            //Str += "<br/>";
            //Str += "➢<i> Account No:</i> 6287618478524001";
            //Str += "<br/>";
            //Str += "➢ <i>Account Name:</i> Sagar KC";
            //Str += "<br/>";
            //Str += "➢<i> Branch:</i> Walling Branch";
            //Str += "<br/>"; 
            //Str += "<br/>";
            //Str += "<b><u>Additional Service Provided by Danphe Care </u><i>(Includes Additional Charges)</i></b>";
            //Str += "<br/>";
            //Str += " ◦  Medicine At home";
            //Str += "<br/>";
            //Str += " ◦  Consultant Tele-medicine service";
            //Str += "<br/>";
            //Str += " ◦  Dietician Consultation";
            //Str += "<br/>"; 
            //Str += " ◦  Laboratory Service at home";
            //Str += "<br/>";
            //Str += " ◦  Home Isolation Service";
            //Str += "<br/>";
            //Str += " ◦  Hotel Isolation";
            //Str += "<br/>";
            //Str += " ◦  Oxygen Cylinder Supply";
            //Str += "<br/>";
            //Str += " ◦  Hospital Referral";
            //Str += "<br/>"; 
            //Str += "<br/>";
            //Str += "<b>Danphe Care Website:<b> https://danphecare.com/ ";
            //Str += "<br/>"; 
            //Str += "<br/>";
            //Str += "<i>Note :</i> If you need quick response you can reach us at 9802314740, between 8 am to 8 pm.If you have general Question about Home Isolation, ";
            //Str += "<br/>";
            //Str += "check out our Home Isolation tutorials or walk through and answer to FAQs,you can visitour learning center at, https://danphecare.com/lms/ ";
            //Str += "<br/>";
            //Str += "<br/>";
            string Str = "<b> Dear " + " " + dhcare.Name + " ! </b>";
            Str += "<br/>";
            Str += "<br/>";
            Str += "Thank you for reaching us.";
            Str += "<br/>";
            Str += "On behalf of Danphe Care, we would like to thank you for choosing us and trusting our services.";
            Str += "<br/>";
            Str += "<br/>";
            Str += "We will soon reach you or, you can call us at 9802314740 for urgency.";
            Str += "<br/>";
            Str += "Danphe Care wishes you to stay healthy and safe. We will be happy to serve you with our following services:";
            Str += "<br/>";
            Str += "1. Telemedicine service";
            Str += "<br/>";
            Str += "2. Home Isolation Covid care";
            Str += "<br/>";
            Str += "3. Nurse at Home";
            Str += "<br/>";
            Str += "4. Physiotherapy at home";
            Str += "<br/>";
            Str += "5. Doctor at home";
            Str += "<br/>";
            Str += "6. Laboratory Service at home";
            Str += "<br/>";
            Str += "<br/>";
            Str += "With Regards";
            Str += "<br/>";
            Str += "Admin";
            Str += "<br/>";
            Str += "<b>Danphe Care</b>";
            Str += "<br/>";
            Str += "<br/>";
            bodyBuilder.HtmlBody = Str;
            mimeMessage.Body = bodyBuilder.ToMessageBody();
            //  mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Text)
            // { Text = message.Content };
            return mimeMessage;
        }
        //[HttpPost]
        //[Route("us")]
        //public HttpResponseMessage Add(HttpRequestMessage request, DanphecareViewModel danphecare)
        //{
        //    //_requiredRepositories = new List<Type>() { typeof(Job) };

        //    //return CreateHttpResponse(request, _requiredRepositories, () =>
        //   // {
        //        HttpResponseMessage response = null;

        //        try
        //        {
        //            MailMessage msz = new MailMessage();
        //            msz.From = new MailAddress(danphecare.Email);//Email which you are getting 
        //                                                      //from contact us page 
        //            msz.To.Add("connectnarayan11@gmail.com");//Where mail will be sent


        //            msz.Subject = "Home Isolation Care Booking";
        //            msz.Body = danphecare.Message + " " +
        //            "Phone Number:" + " " + danphecare.ContactNumber + " " +
        //            "Email:" + " " + danphecare.Email;

        //            SmtpClient smtp = new SmtpClient();

        //            smtp.Host = "smtp.gmail.com";

        //            smtp.Port = 587;

        //            smtp.Credentials = new System.Net.NetworkCredential
        //            ("connectnarayan11@gmail.com", "lenin5175");

        //            smtp.EnableSsl = true;

        //            smtp.Send(msz);

        //            ModelState.Clear();

        //        }
        //        catch (Exception ex)
        //        {
        //            ModelState.Clear();

        //        }

        //        //album = Mapper.Map<Album, AlbumViewModel>(newAlbum);
        //      //  response = request.CreateResponse<ContactViewModel>(HttpStatusCode.OK, contact);

        //        return response;
        //    });
        //}



    }
}
