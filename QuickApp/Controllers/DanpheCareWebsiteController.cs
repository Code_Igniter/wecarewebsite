﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTele.ViewModel;
using AutoMapper;
using Telemedicine.ServerModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using Microsoft.Extensions.Hosting.Internal;
using System.Net.Mail;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using DAL.Models;
using DAL;

namespace TestTele.Controllers
{
    [Route("api/[controller]/[action]")]
    public class DanpheCareWebsiteController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly ILogger<DanpheCareCMSController> _logger;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        //
        public DanpheCareWebsiteController(IUnitOfWork unitOfWork, IAuthorizationService authorizationService, IMapper mapper, IWebHostEnvironment hostingEnvironment,
                                  UserManager<ApplicationUser> userManager, ILogger<DanpheCareCMSController> logger)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _authorizationService = authorizationService;
            this._logger = logger;
            _userManager = userManager;
        }

        #region About Us  
        [HttpGet]
        public ActionResult GetAboutUs()
        {
            try
            {
                var abtus = _unitOfWork.AboutUs.GetAll();
                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "About Page").FirstOrDefault();
                var abtList = _mapper.Map<List<AboutUsModel>, List<AboutUsViewModel>>(abtus.ToList());
                if (metaTag != null)
                {
                    abtList[0].MetaTitle = metaTag.Title;
                    abtList[0].MetaContent = metaTag.Content;
                    abtList[0].MetaKeywords = metaTag.Keywords;
                }
                return Ok(abtList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
        #endregion

        #region GET Our Services 
        [HttpGet]
        public ActionResult GetOurServices()
        {
            try
            {
                var ourService = _unitOfWork.OurServices.GetAll();
                var ourServiceList = _mapper.Map<List<OurServicesModel>, List<OurServicesViewModel>>(ourService.ToList());
                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "Services List Page").FirstOrDefault();
                if (metaTag != null)
                {
                    ourServiceList[0].MetaTitle = metaTag.Title;
                    ourServiceList[0].MetaContent = metaTag.Content;
                    ourServiceList[0].MetaKeywords = metaTag.Keywords;
                }
                return Ok(ourServiceList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpGet("{permalink}")]
        public ActionResult GetServiceById(string permalink)
        {
            var service = _unitOfWork.OurServices.GetAll().Where(x => x.PermaLink == permalink).FirstOrDefault();
            var ourServiceDetails = _mapper.Map<OurServicesModel, OurServicesViewModel>(service);
            var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.ServiceId == service.ServiceId).FirstOrDefault();
            if (metaTag != null)
            {
                ourServiceDetails.MetaTitle = metaTag.Title;
                ourServiceDetails.MetaContent = metaTag.Content;
                ourServiceDetails.MetaKeywords = metaTag.Keywords;
            }

            return Ok(ourServiceDetails);

        }
        #endregion

        #region GET Our Team Members 
        [HttpGet]
        public ActionResult GetCoreTeamMembers()
        {
            try
            {
                var data = _unitOfWork.OurTeamMembers.GetAll();
                var dataList = _mapper.Map<List<OurTeamMemberModel>, List<OurTeamMemberViewModel>>(data.Where(x=>x.IsCoreTeam == true).OrderBy(x=>x.Sorting).ToList());
                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "Home Page").FirstOrDefault();
                if (metaTag != null)
                {
                    dataList[0].MetaTitle = metaTag.Title;
                    dataList[0].MetaContent = metaTag.Content;
                    dataList[0].MetaKeywords = metaTag.Keywords;
                }
                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpGet]
        public ActionResult GetOurTeamMembers()
        {
            try
            {
                var data = _unitOfWork.OurTeamMembers.GetAll();
                var dataList = _mapper.Map<List<OurTeamMemberModel>, List<OurTeamMemberViewModel>>(data.OrderBy(x => x.Sorting).ToList());
                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "Home Page").FirstOrDefault();
                if (metaTag != null)
                {
                    dataList[0].MetaTitle = metaTag.Title;
                    dataList[0].MetaContent = metaTag.Content;
                    dataList[0].MetaKeywords = metaTag.Keywords;
                }

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpGet]
        public ActionResult GetTeamSortingList()
        {
            try
            {
                var data = _unitOfWork.OurTeamMembers.GetAll();
                var dataList = _mapper.Map<List<OurTeamMemberModel>, List<OurTeamMemberViewModel>>(data.OrderBy(x => x.Sorting).ToList());

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        public ActionResult GetTeamMembersById(int id)
        {
            var tMem = _unitOfWork.OurTeamMembers.GetAll().Where(x => x.TeamMemberId == id).FirstOrDefault();

            return Ok(tMem);

        }
        #endregion

        #region GET Our Media Coverage 
        [HttpGet]
        public ActionResult GetOurMediaCoverage()
        {
            try
            {
                var data = _unitOfWork.OurMediaCoverage.GetAll();
                var dataList = _mapper.Map<List<OurMediaCoverageModel>, List<OurMediaCoverageViewModel>>(data.OrderByDescending(x=>x.CreatedDate).ToList());

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
        #endregion

        #region GET TestimonialMain
        [HttpGet]
        public ActionResult GetTestimonialMain()
        {
            try
            {
                var abtus = _unitOfWork.TestimonialMain.GetAll();
                var abtList = _mapper.Map<List<TestimonialMainModel>, List<TestimonialMainViewModel>>(abtus.ToList());

                return Ok(abtList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
        #endregion

        #region GET Our Testimonials 
        [HttpGet]
        public ActionResult GetTestimonials()
        {
            try
            {
                var data = _unitOfWork.Testimonials.GetAll();
                var dataList = _mapper.Map<List<TestimonialModel>, List<TestimonialViewModel>>(data.OrderByDescending(x=>x.CreatedDate).Take(2).ToList());

                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
        #endregion



        #region Danphe Care Department

        [HttpGet("{permalink}")]
        public async Task<ActionResult> GetDepartmentById(string permalink)
        {
            var deps = await _unitOfWork.DepartmentModels.GetAllAsync();
          
            var query = deps.Where(x => x.PermaLink == permalink).FirstOrDefault();
            var departmentDetails = _mapper.Map<DepartmentModel, DepartmentDanpheCareViewModel>(query);
            var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.DepartmentId == query.DepartmentId).FirstOrDefault();
            if (metaTag != null)
            {
                departmentDetails.MetaTitle = metaTag.Title;
                departmentDetails.MetaContent = metaTag.Content;
                departmentDetails.MetaKeywords = metaTag.Keywords;
            }

            return Ok(departmentDetails);

        }

        public ActionResult GetDepartmentList()
        {
            try
            {
                var _getDept = _unitOfWork.DepartmentModels.GetAll();
                var _depList = _mapper.Map<List<DepartmentModel>, List<DepartmentDanpheCareViewModel>>(_getDept.ToList());
             
                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "Department List Page").FirstOrDefault();
                if (metaTag != null)
                {
                    _depList[0].MetaTitle = metaTag.Title;
                    _depList[0].MetaContent = metaTag.Content;
                    _depList[0].MetaKeywords = metaTag.Keywords;
                }

                return Ok(_depList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        #endregion Danphe Care Department

        #region Danphe Care Doctor


        [HttpGet("{permalink}")]
        public async Task<ActionResult> GetDoctorById(string permalink)
        {
            var deps = await _unitOfWork.DepartmentModels.GetAllAsync();
            var doctor = _unitOfWork.DanpheCareDoctorModels.GetAll().ToList();
            var query = (from doc in doctor
                         //join dep in deps on doc.DepartmentId equals dep.DepartmentId
                         where doc.PermaLink == permalink
                         select new DanpheCareDoctorViewModel
                         {
                             Designation = doc.Designation,
                             Experience = doc.Experience,
                             FullName = doc.FullName,
                             ImagePath = doc.ImagePath,
                             //DepartmentName = dep.DepartmentName,
                             CoverPhoto = doc.CoverPhoto,
                             Content = doc.Content,
                             DoctorId = doc.DoctorId
                         }).FirstOrDefault();
            var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.DoctorId == query.DoctorId).FirstOrDefault();
            if (metaTag != null)
            {
                query.MetaTitle = metaTag.Title;
                query.MetaContent = metaTag.Content;
                query.MetaKeywords = metaTag.Keywords;
            }

            return Ok(query);

        }

        public ActionResult GetDoctorList()
        {
            try
            {
                var _getDoc = _unitOfWork.DanpheCareDoctorModels.GetAll();
                var _getDepartment = _unitOfWork.DepartmentModels.GetAll();
                var _docList = (from doc in _getDoc
                                join dep in _getDepartment on doc.DepartmentId equals dep.DepartmentId
                                select new DanpheCareDoctorViewModel
                                {
                                   FullName = doc.FullName,
                                   Experience =  doc.Experience,
                                   Designation = doc.Designation,
                                   DoctorId = doc.DoctorId,
                                   DepartmentId = doc.DepartmentId,
                                   ImagePath = doc.ImagePath,
                                   DepartmentName = dep.DepartmentName,
                                   CoverPhoto = doc.CoverPhoto,
                                   Content = doc.Content

                                }).ToList();

             

                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "Specialist Page").FirstOrDefault();
                if (metaTag != null)
                {
                    _docList[0].MetaTitle = metaTag.Title;
                    _docList[0].MetaContent = metaTag.Content;
                    _docList[0].MetaKeywords = metaTag.Keywords;
                }
                return Ok(_docList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
        [HttpGet]
        public ActionResult GetDoctorSortingList()
        {
            try
            {
                var data = _unitOfWork.DanpheCareDoctorModels.GetAll();
                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "Specialist Page").FirstOrDefault();
                var dataList = _mapper.Map<List<DanpheCareDoctorModel>, List<DanpheCareDoctorViewModel>>(data.OrderBy(x => x.Sorting).ToList());
                if (metaTag != null)
                {
                    dataList[0].MetaTitle = metaTag.Title;
                    dataList[0].MetaContent = metaTag.Content;
                    dataList[0].MetaKeywords = metaTag.Keywords;
                }
                return Ok(dataList);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion Danphe Care Doctor

        #region Department Consultation


        public ActionResult GetConsultationList()
        {
            try
            {
                var _getConsultation = _unitOfWork.DepartmentConsultationModels.GetAll().ToList();

                var _getDepartment = _unitOfWork.DepartmentModels.GetAll();
                var _consultationList = (from con in _getConsultation
                                         join dep in _getDepartment on con.DepartmentId equals dep.DepartmentId
                                         select new
                                         {
                                             con.DepartmentConsultationId,
                                             con.DepartmentId,
                                             con.Title,
                                             con.Content,
                                             con.IconPath,
                                             dep.DepartmentName


                                         }).ToList();

                return Ok(_consultationList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        #endregion Department Consultation

        #region Resourceful Articles

        //[HttpGet("{id}")]
        //public async Task<ActionResult> GetArticlesById(int id)
        //{
        //    var articles = await _unitOfWork.ResourcefulArticlesModels.GetAllAsync();
        //    var doctor = _unitOfWork.DanpheCareDoctorModels.GetAll().ToList();
        //    var query = (from doc in doctor
        //                 join art in articles on doc.DoctorId equals art.DoctorId
        //                 where doc.DoctorId == id
        //                 select new
        //                 {
        //                     doc.Designation,
        //                     doc.Experience,
        //                     doc.FullName,
        //                     doc.ImagePath,
        //                     art.CreatedDate,
        //                     art.Content,
        //                     art.DoctorId,
        //                     art.ResourcefulArticlesId,
        //                     ArtImage = art.ImagePath,
        //                     art.Title,
        //                     DoctorName = doc.FullName,
        //                 }).ToList();

        //    return Ok(query);

        //}
        public ActionResult GetArticles()
        {
            try
            {
                var _getArticles = _unitOfWork.ResourcefulArticlesModels.GetAll();


                //var _getDoctor = _unitOfWork.DanpheCareDoctorModels.GetAll();
                //var _articlesList = (from articles in _getArticles
                //                     join doc in _getDoctor on articles.DoctorId equals doc.DoctorId
                //                     select new
                //                     {
                //                         articles.DoctorId,
                //                         articles.ResourcefulArticlesId,
                //                         articles.Title,
                //                         articles.Content,
                //                         articles.ImagePath,
                //                         doc.FullName
                //                     }).ToList();

                return Ok(_getArticles);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetArticlesId(int id)
        {
            var articles = await _unitOfWork.ResourcefulArticlesModels.GetAllAsync();
            var query = articles.Where(x => x.ResourcefulArticlesId == id).FirstOrDefault();
            //var doctor = _unitOfWork.DanpheCareDoctorModels.GetAll().ToList();
            //var query = (from art in articles
            //             join doc in doctor on art.DoctorId equals doc.DoctorId
            //             where art.ResourcefulArticlesId == id
            //             select new
            //             {
            //                 doc.Designation,
            //                 doc.Experience,
            //                 doc.FullName,
            //                 doc.ImagePath,
            //                 art.CreatedDate,
            //                 art.Content,
            //                 art.DoctorId,
            //                 art.ResourcefulArticlesId,
            //                 ArtImage = art.ImagePath,
            //                 art.Title,
            //                 DoctorName = doc.FullName,
            //             }).FirstOrDefault();

            return Ok(query);

        }

        #endregion Resourceful Articles

        #region News

        [HttpGet("{id}")]
        public ActionResult GetNewsById(int id)
        {
            var news = _unitOfWork.NewsModels.GetAll().Where(x => x.NewsId == id).FirstOrDefault();

            return Ok(news);

        }

        public ActionResult GetNews()
        {
            try
            {
                var _getNews = _unitOfWork.NewsModels.GetAll();
                var _newsList = _mapper.Map<List<NewsModel>, List<NewsViewModel>>(_getNews.OrderByDescending(x => x.CreatedDate).Take(10).ToList());

                return Ok(_newsList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        #endregion News

        #region Blogs

        [HttpGet("{id}")]
        public ActionResult GetBlogsById(string id)
        {
            var blog = _unitOfWork.BlogsModels.GetAll().Where(x => x.PermaLink == id).FirstOrDefault();
            var blogDetails = _mapper.Map<BlogsModel, BlogsViewModel>(blog);
            var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.BlogId == blog.BlogId).FirstOrDefault();
            if (metaTag != null)
            {
                blogDetails.MetaTitle = metaTag.Title;
                blogDetails.MetaContent = metaTag.Content;
                blogDetails.MetaKeywords = metaTag.Keywords;
            }

            return Ok(blogDetails);

        }
        [HttpGet]
        public ActionResult GetBlogs()
        {
            try
            {
                var _getBlogs = _unitOfWork.BlogsModels.GetAll();
                var _blogsList = _mapper.Map<List<BlogsModel>, List<BlogsViewModel>>(_getBlogs.OrderByDescending(x=>x.CreatedDate).ToList());
                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "Blog List Page").FirstOrDefault();
                if (metaTag != null)
                {
                    _blogsList[0].MetaTitle = metaTag.Title;
                    _blogsList[0].MetaContent = metaTag.Content;
                    _blogsList[0].MetaKeywords = metaTag.Keywords;
                }

                return Ok(_blogsList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        #endregion Blogs

        #region Meta Tag

        [HttpGet("{id}")]
        public ActionResult GetMetaTagById(int id)
        {
            var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.MetaTagId == id).FirstOrDefault();

            return Ok(metaTag);

        }

        public ActionResult GetMetaTag()
        {
            try
            {
                var _getMetaTag = _unitOfWork.MetaTagModels.GetAll();
                var _metaTagList = _mapper.Map<List<MetaTagModel>, List<MetaTagViewModel>>(_getMetaTag.ToList());

                return Ok(_metaTagList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        #endregion Meta Tag

        #region DanpheCare Contact

        [HttpGet("{id}")]
        public ActionResult GetContactById(int id)
        {
            var contact = _unitOfWork.DanpheCareContactModels.GetAll().Where(x => x.DanpheCareContactId == id).FirstOrDefault();

            return Ok(contact);

        }
        public ActionResult GetDanpheCareContact()
        {
            try
            {
                var _getContact = _unitOfWork.DanpheCareContactModels.GetAll();
                var _contactList = _mapper.Map<List<DanpheCareContactModel>, List<DanpheCareContactViewModel>>(_getContact.ToList());

                return Ok(_contactList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        #endregion Danphe Care Contact

        #region Webinar
        public ActionResult GetWebinar()
        {
            try
            {
                var _getWebinar = _unitOfWork.WebinarModels.GetAll();
                var _webinarList = _mapper.Map<List<WebinarModel>, List<WebinarViewModel>>(_getWebinar.OrderByDescending(x=>x.CreatedDate).Take(10).ToList());

                return Ok(_webinarList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        #endregion Webinar

        #region Expat
        public ActionResult GetExpat()
        {
            try
            {
                var _getExpat = _unitOfWork.ExpatModels.GetAll();
                var _expatList = _mapper.Map<List<ExpatModel>, List<ExpatViewModel>>(_getExpat.ToList());
                var metaTag = _unitOfWork.MetaTagModels.GetAll().Where(x => x.Page == "Expat Page").FirstOrDefault();
                if (metaTag != null)
                {
                    _expatList[0].MetaTitle = metaTag.Title;
                    _expatList[0].MetaContent = metaTag.Content;
                    _expatList[0].MetaKeywords = metaTag.Keywords;
                }

                return Ok(_expatList);
            }
            catch (System.Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        #endregion Expat

        #region Department Details


        [HttpGet("{permalink}")]
        public async Task<ActionResult> GetDepartmentContentById(string permalink)
        {
            var depContent = await _unitOfWork.DepartmentTabContents.GetAllAsync();
            var depTab = _unitOfWork.DepartmentTabs.GetAll().ToList();
            var depId = _unitOfWork.DepartmentModels.GetAll().Where(x => x.PermaLink == permalink).Select(s => s.DepartmentId).FirstOrDefault();
            var query = (from content in depContent
                         join tab in depTab on content.DepartmentTabId equals tab.DepartmentTabId
                         where content.DepartmentId == depId
                         select new
                         {
                             content.DepartmentContentId,
                             content.DepartmentTabId,
                             content.DepartmentId,
                             content.Title,
                             content.Content,
                             tab.TabName
                            
                         }).ToList();

            return Ok(query);

        }

        [HttpGet("{permalink}")]
        public async Task<ActionResult> GetServiceByDepartmentId(string permalink)
        {
            var department = await _unitOfWork.DepartmentModels.GetAllAsync();
            var services = _unitOfWork.OurServices.GetAll().ToList();
            var query = (from dep in department
                         join ser in services on dep.DepartmentId equals ser.DepartmentId
                         where dep.PermaLink == permalink
                         select new
                         {
                             dep.DepartmentId,
                             dep.DepartmentName,
                             ser.ServiceId,
                             ser.ServiceName,
                             ser.Content,
                             ser.ImagePath

                         }).FirstOrDefault();

            return Ok(query);

        }

        [HttpGet("{permalink}")]
        public async Task<ActionResult> GetDoctorByDepartmentId(string permalink)
        {
            var doctor = await _unitOfWork.DanpheCareDoctorModels.GetAllAsync();
            var department = _unitOfWork.DepartmentModels.GetAll().Where(x=>x.PermaLink == permalink).ToList();
            var query = (from doc in doctor
                         join dep in department on doc.DepartmentId equals dep.DepartmentId
                         select new
                         {
                             dep.DepartmentId,
                             dep.DepartmentName,
                             doc.Designation,
                             doc.Experience,
                             doc.FullName,
                             doc.ImagePath,
                             doc.DoctorId,
                             doc.PermaLink

                         }).ToList();

            return Ok(query);

        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetArticlesByDepartmentId(int id)
        {
            var articles = await _unitOfWork.ResourcefulArticlesModels.GetAllAsync();
            var query = articles.ToList();
            //var department = _unitOfWork.DepartmentModels.GetAll().ToList();
            //var doctor = _unitOfWork.DanpheCareDoctorModels.GetAll().Where(x=>x.DepartmentId == id).ToList();
            //var query = (from art in articles
            //             join doc in doctor on art.DoctorId equals doc.DoctorId
            //             select new
            //             {
            //                 art.ResourcefulArticlesId,
            //                 art.DoctorId,
            //                 art.Content,
            //                 art.Title,
            //                 art.ImagePath,
            //                 DoctorName=doc.FullName

            //             }).ToList();

            return Ok(query);

        }

        [HttpGet("{permalink}")]
        public async Task<ActionResult> GetSubHeadingByDepartmentId(string permalink)
        {
            var depSubHeading = await _unitOfWork.DepartmentSubHeadings.GetAllAsync();
            var dep = _unitOfWork.DepartmentModels.GetAll().Where(x=>x.PermaLink == permalink).ToList();
            var depId = _unitOfWork.DepartmentModels.GetAll().Where(x => x.PermaLink == permalink).Select(s => s.DepartmentId).FirstOrDefault();
            var query = (from subheading in depSubHeading
                         join department in dep on subheading.DepartmentId equals department.DepartmentId
                         where subheading.DepartmentId == depId
                         select new
                         {
                             subheading.Title,
                             subheading.ShortDescription,
                             subheading.DepartmentSubHeadingId,
                             subheading.DepartmentId,
                             department.DepartmentName,
                             subheading.IconPath,

                         }).ToList();

            return Ok(query);

        }
        [HttpGet("{permalink}")]
        public async Task<ActionResult> GetSubHeadingByDoctorId(string permalink)
        {
            var depSubHeading = await _unitOfWork.DepartmentSubHeadings.GetAllAsync();
            var depId = _unitOfWork.DanpheCareDoctorModels.GetAll().Where(x => x.PermaLink == permalink).Select(s=>s.DepartmentId).FirstOrDefault();
            
            var query = (from subheading in depSubHeading
                        
                         where subheading.DepartmentId == depId
                         select new
                         {
                             subheading.Title,
                             subheading.ShortDescription,
                             subheading.DepartmentSubHeadingId,
                             subheading.DepartmentId,
                             subheading.IconPath,

                         }).ToList();

            return Ok(query);

        }

        #endregion Department Details

        #region Danphe Care Core Team Get by Id
        [HttpGet("{id}")]
        public ActionResult GetCoreTeamById(int id)
        {
            var teamMember =  _unitOfWork.OurTeamMembers.GetAll().Where(a=>a.TeamMemberId == id ).FirstOrDefault();

            return Ok(teamMember);

        }
        #endregion




        [HttpGet("{id}")]
        public ActionResult GetSubServicesById(int id)
        {
            var services = _unitOfWork.OurServices.GetAll().ToList(); ;
            var subservices = _unitOfWork.SubServices.GetAll().Where(x => x.OurServicesId == id).ToList();
            var query = (from subserv in subservices
                         join service in services on subserv.OurServicesId equals service.ServiceId
                         where service.ServiceId == id
                         select new
                         {
                             subserv.Title,
                             subserv.ImagePath,
                             subserv.OurServicesId,
                             subserv.SubServicesId,
                             service.ServiceName
                         }).ToList();

            return Ok(query);

        }


        [HttpGet("{id}")]
        public ActionResult GetSubServicesDetailsById(int id)
        {
            var subservicesdetails = _unitOfWork.SubServicesDetails.GetAll().ToList(); ;
            var subservices = _unitOfWork.SubServices.GetAll().Where(x => x.SubServicesId == id).ToList();
            var query = (from subservdetails in subservicesdetails
                         join subserv in subservices on subservdetails.SubServicesId equals subserv.SubServicesId
                         where subserv.SubServicesId == id
                         select new
                         {
                            subservdetails.Introduction,
                            subservdetails.ImagePath,
                            subservdetails.SubServicesDetailsId,
                            subservdetails.SubServicesId,
                            subservdetails.SubTitle,
                            subservdetails.Content,
                            subserv.Title


                         }).FirstOrDefault();

            return Ok(query);

        }

    }
}
